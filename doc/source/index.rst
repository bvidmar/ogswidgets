.. include:: include.rst

OGSWIDGETS Python Package Documentation
*******************************************************************************

Foreword
===============================================================================
|project|_ has been developed by two programmers working in the 
`Aerial Operations <http://www.inogs.it/en/content/aerial-operations>`_
group of the IRI Research Section at
`OGS - Istituto Nazionale di Oceanografia e di
Geofisica Sperimentale <http://www.inogs.it>`_.

Python is their favourite programming language since 2006.

The authors:

**Roberto Vidmar, Nicola Creati**

.. image:: images/vidmar.jpg
   :height: 134 px
   :width:  100 px 
.. image:: images/creati.jpg
   :height: 134 px
   :width:  100 px 

Requirements
===============================================================================
The current version of |project|_ relies on the following packages:             
                                                                                
.. command-output:: cat ../../requirements.txt

OGSWIDGETS in detail
###############################################################################

.. toctree::
  :maxdepth: 2

  coordinates
  other
  obsolete
  api/index

.. warning::
   This code has been tested *only* on Linux (Kubuntu 20.04 LTS)
   but should work also on Mac and Windows.

.. warning::
   This is work in progress!


Indices and Tables
###############################################################################
* :ref:`genindex`
* :ref:`modindex`
