Obsolete Coordinate conversion widgets
*******************************************************************************


:mod:`ogswidgets.obsolete.coordDialog`
-------------------------------------------------------------------------------
The CoordDialog class handles a dialog for converting coordinate pairs
or files from one Datum / Projection to another interactively.
The widget uses :mod:`ogswidgets.projDialog` to choose datum and projection
from those defined by pyproj.

Upper coordinate in degrees, lower in degrees, minutes and seconds:

.. image:: images/coordDialog0.png
    :align: center

Both coordinates in degrees, minutes and seconds:

.. image:: images/coordDialog1.png
    :align: center

Clicking the button with the down arrow brings up this dialog to choose
a different datum and projection:

.. image:: images/coordDialog2.png
    :align: center

Searching for EPSG 26591:

.. image:: images/coordDialog3.png
    :align: center

Convert WGS84 geographic coordinates (lower input) in degrees,
minutes.decimals to plane (projected) in EPSG 26591 (Gauss-Boaga):

.. image:: images/coordDialog4.png
    :align: center

:mod:`ogswidgets.obsolete.projDialog`
-------------------------------------------------------------------------------
A widget to choose Datum / Projection from those defined by pyproj.

Browse available coordinate reference systems:

.. image:: images/projDialog0.png
    :align: center

Serach for EPSG 26592:

.. image:: images/projDialog1.png
    :align: center
