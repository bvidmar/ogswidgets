.. include:: include.rst

===============================================================================
Coordinates and Coordinates Conversion widgets
===============================================================================
DegreesWidget
-------------------------------------------------------------------------------
The module :mod:`ogswidgets.degreesWidget` defines the
:class:`ogswidgets.degreesWidget.DegreesWidget` class that handles
latitute or longitude **input** and representation in

    - degrees with decimals **or**
    - degrees and minutes with decimals **or**
    - degrees minutes and seconds with decimals

When editing the value displayed by the widget input is validated at
every keystroke so only valid degrees are accepted.
The widget can convert from any of the three represetations to another with
the :meth:`ogswidgets.degreesWidget.DegreesWidget.setRep` method.

Here the widget in action showing -123 degrees 45 minutes and 54 seconds:

 +--------------------------------+--------------------------------+--------------------------------+
 | .. image:: images/degrees0.png + .. image:: images/degrees1.png + .. image:: images/degrees2.png |
 +--------------------------------+--------------------------------+--------------------------------+

ProjectedWidget
-------------------------------------------------------------------------------
For the representation of plane (projected) coordinates |project|_ defines the
:class:`ogswidgets.projectedWidget.ProjectedWidget` class:

.. image:: images/projectedWidget.png
    :align: center

HeightWidget
-------------------------------------------------------------------------------
For the representation of height, either ellipsoidal or orthometric there is
the :class:`ogswidgets.heightWidget.HeightWidget` class. This widget can
show both heights when they are available:

 +----------------------------+------------------------------+
 | .. image:: images/hell.png + .. image:: images/hortho.png |
 +----------------------------+------------------------------+

CoorWidget
-------------------------------------------------------------------------------
The former three widgets are the main ingredients of the
:class:`ogswidgets.coorWidget.CoorWidget` class:

.. image:: images/coorWidget0.png
    :align: center

This widget shows also the Coordinate Reference Sistem of the point
and has an (optional) combo boxe to select the representation of
geographic coordinates:

.. image:: images/coorWidget1.png
    :align: center

.. image:: images/coorWidget2.png
    :align: center

.. image:: images/coorWidget3.png
    :align: center

When orthometric height is available as diffrence from ellipsoidal height
an (optional) combo can be visualized to choose which height to show:

.. image:: images/coorWidget4.png
    :align: center

The same widget can show also projected coordinates:

.. image:: images/coorWidget5.png
    :align: center

FileCoorWidget
-------------------------------------------------------------------------------
The :class:`ogswidgets.fileCoorWidget.FileCoorWidget` class subclassing
:class:`ogswidgets.coorWidget.CoorWidget` inherits its behaviour and
adds a file picker widget that allows to select a file for input or output:

.. image:: images/fileCoorWidget0.png
    :align: center

CoorDialog
-------------------------------------------------------------------------------
The :class:`ogswidgets.coorDialog.CoorDialog` class handles a dialog
for converting coordinate pairs or files from one Coordinate
Reference System (CRS) to another interactively.
The widget uses :mod:`ogswidgets.crsDialog` to choose the CRS from its
EPSG_ number using pyproj_.

.. _coorDialog0:
.. figure:: images/coorDialog0.png
    :align: center

    Converting from CRS EPSG 4979 (WGS 84) to CRS EPSG 32633
    (WGS 84 / UTM zone 33N)

In figure :numref:`coorDialog0` we are converting from CRS EPSG 4979 (WGS 84)
to CRS EPSG 32633 (WGS 84 / UTM 33) using the pyproj_ pipline shown in the
lower part of the widget.

Changing the Easting value in the widget from 0403328.793 to 0403333.333
changes interactively the values in the ᵠ, λ fields and defines a new
pyproj_ conversion pipeline in the upper part of the widget:

.. _coorDialog1:
.. figure:: images/coorDialog1.png
    :align: center

    Converting back from CRS EPSG 32633 (WGS 84 / UTM zone 33N) to CRS EPSG
    4979 (WGS 84)

The *destination* CSR can be changed clicking the button with the
*Downwards Double Arrow* right of the pyproj_ pipeline description.

.. _crsDialog0:
.. figure:: images/crsDialog0.png
    :align: center

    Dialog to change current CRS (EPSG 32633) to a new one, either geodetic
    or projected.

:numref:`crsDialog0` shows the dialog to choose a new CRS for the
conversion. A new CRS can be selected searching for its EPSG number or
for its name (as it appears in the proj.db database) and clicking
the *New Search* button

.. _crsDialog1:
.. figure:: images/crsDialog1.png
    :align: center

    Dialog to select the next CRS that satisfies the search criterium.

If more than one CRS satisfy the search criterium the *Move to* button in
:numref:`crsDialog1` can be clicked to select the next one.

If more than one conversion pipeline is defined from converting from
one CRS to another (like converting from EPSG 4979 (WGS 84 3D) to
EPSG 4985 (WGS 72 3d) the following dialog is shown:


.. _selectTransform0:
.. figure:: images/selectTransform0.png
    :align: center

    Dialog to select the transformation among all available.

In :numref:`selectTransform0` are displayed all the available transformation
pipelines with the difference between them highlighted for better
understanding.

File conversion
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Once input (upper) and output (lower) CRS have been chosen,
selecting *File* in the upper combo box changes the widget appearance
(:numref:`convertFile0`):

.. _convertFile0:
.. figure:: images/convertFile0.png
    :align: center

    Dialog to select file transformation.

Once the input and output file have been selected the *Convert File*
elliptical button starts the file coordinate conversion.


.. _convertFile1:
.. figure:: images/convertFile1.png
    :align: center

    Start the file transformation.

Conferm the degrees format for the output file

.. _convertFile2:
.. figure:: images/convertFile2.png
    :align: center

    Confirm format for output file.

Next define the format of the *input* file with the aid of the 
:class:`ogswidgets.fieldsWizard.FieldsWizard` widget.  
If the file has a header check the checkbox *This file has a Header* and
select the header rows with a left click of the mouse holding down the
*Shift* key.

.. _fieldsWizard00:
.. figure:: images/fieldsWizard00.png
    :align: center

    Select the file header

Next click the right mouse button to confirm the header selection and
double click (or right click) on a file row to take it in the field selection
area where the columns can be selected with the mouse and assigned with
a right click:

.. _fieldsWizard01:
.. figure:: images/fieldsWizard01.png
    :align: center

    Select the northing column

.. _fieldsWizard02:
.. figure:: images/fieldsWizard02.png
    :align: center

    Assign the northing column

.. _fieldsWizard03:
.. figure:: images/fieldsWizard03.png
    :align: center

    select the height column

When all the columns have been assigned the input file format can be saved
for future reuse and the conversion starts.

The :class:`ogswidgets.fieldsWizard.FieldsWizard` widget is more useful
when dealing with geographic coordinates like in :numref:`fieldsWizard04`:

.. _fieldsWizard04:
.. figure:: images/fieldsWizard04.png
    :align: center

    Select latitude seconds column

.. _fieldsWizard05:
.. figure:: images/fieldsWizard05.png
    :align: center

    Select longitude minutes column

.. _fieldsWizard06:
.. figure:: images/fieldsWizard06.png
    :align: center

    Assign longitude minutes column

.. note:: Only valid columns can be selected.
