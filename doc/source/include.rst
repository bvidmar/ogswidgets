.. |project| replace:: OGSWIDGETS                                                    
.. |projectl| replace:: ogswidgets                                                   
.. |projecth| replace:: https://bitbucket.org/bvidmar/ogswidgets

.. _project: index.html
.. _PySide2: https://pypi.org/project/PySide2/
.. _PyQt5:  https://pypi.org/project/PyQt5
.. _pyproj: https://pypi.org/project/pyproj/
.. _EPSG: https://epsg.io/
