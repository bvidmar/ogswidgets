*********************
Generic usage widgets
*********************

:mod:`ogswidgets.appExceptionTrace`
-----------------------------------

Exception handler for graphic aplications. Includes traceback, dump variables
to file and IPython console for interactive debugging.

How the widget appears:

.. image:: images/appExceptionTrace0.png
    :align: center

The application environment:

.. image:: images/appExceptionTrace1.png
    :align: center

The detailed traceback with extra information:

.. image:: images/appExceptionTrace2.png
    :align: center

The variables:

.. image:: images/appExceptionTrace3.png
    :align: center

The IPython console:

.. image:: images/appExceptionTrace4.png
    :align: center

:mod:`ogswidgets.console`
-------------------------

A Qt IPython widget to inspect running code.

How the widget appears:

.. image:: images/console0.png
    :align: center

:mod:`ogswidgets.fileBrowser`
-----------------------------

A class to browse a file with syntax highlighting. If it’s *NOT* a text file
do nothing. It is used in the :mod:`ogswidgets.filePicker` widget to add
preview for text files.

.. image:: images/fileBrowser0.png
    :align: center

:mod:`ogswidgets.filePicker`
----------------------------

A class to choose files interactively with preview for text files.

.. image:: images/filePicker0.png
    :align: center

:mod:`ogswidgets.hsiIndicator`
------------------------------

A Horizontal Situation Indicator widget. The module includes also a compass.
The HSI shows at the same time:

* Heading
* Course to go or its opposite
* Horizontal off track
* Vertical off track

Extra gauges at left and bottom are auto ranging and can be disabled.

.. image:: images/hsiIndicator0.png
    :align: center


:mod:`ogswidgets.progressDialog`
--------------------------------

A simple progress dialog module.

.. image:: images/progressDialog.png
    :align: center


:mod:`ogswidgets.qMemoryWidget`
-------------------------------

A Memory Usage Widget based on QWidget: minimum usage is the vertical green
line, maximum is the vertical red line. Initial value has a midnightblue
line. Current usage is the lightsteelblue bar.

.. image:: images/qMemoryWidget.png
    :align: center

