.. include:: ../include.rst

:mod:`ogswidgets` Package reference
===============================================================================

.. automodule:: ogswidgets.__init__
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`abstractFeditor` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.abstractFeditor.FValidator

.. inheritance-diagram:: ogswidgets.abstractFeditor.AbstractFEditor

.. inheritance-diagram:: ogswidgets.abstractFeditor.FEditor

.. inheritance-diagram:: ogswidgets.abstractFeditor.UFEditor

.. automodule:: ogswidgets.abstractFeditor
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`appExceptionTrace` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.appExceptionTrace.TraceBackApp

.. inheritance-diagram:: ogswidgets.appExceptionTrace.ExceptionDialog

.. automodule:: ogswidgets.appExceptionTrace
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`console` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.console.ConsoleWidget

.. automodule:: ogswidgets.console
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`coorDialog` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.coorDialog.CRSFileCoorWidget

.. inheritance-diagram:: ogswidgets.coorDialog.CoorDialog

.. automodule:: ogswidgets.coorDialog
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`coorWidget` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.fileCoorWidget.FileCoorWidget

.. automodule:: ogswidgets.coorWidget
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`crsDialog` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.crsDialog.CRSDialog

.. inheritance-diagram:: ogswidgets.crsDialog.SearchWidget

.. automodule:: ogswidgets.crsDialog
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`crsMVC` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.crsMVC.Branch

.. inheritance-diagram:: ogswidgets.crsMVC.Leaf

.. inheritance-diagram:: ogswidgets.crsMVC.CRSModel

.. inheritance-diagram:: ogswidgets.crsMVC.CRSView

.. inheritance-diagram:: ogswidgets.crsMVC.ProjValidator

.. inheritance-diagram:: ogswidgets.crsMVC.UserDefinedDialog

.. inheritance-diagram:: ogswidgets.crsMVC.SaveEPSGDialog

.. automodule:: ogswidgets.crsMVC
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`degreesWidget` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.degreesWidget.DValidator

.. inheritance-diagram:: ogswidgets.degreesWidget.DEditor

.. inheritance-diagram:: ogswidgets.degreesWidget.DMEditor

.. inheritance-diagram:: ogswidgets.degreesWidget.DMSEditor

.. inheritance-diagram:: ogswidgets.degreesWidget.DegreesWidget

.. automodule:: ogswidgets.degreesWidget
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`delegates` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.delegates.AbstractDelegate

.. inheritance-diagram:: ogswidgets.delegates.DatumPickerDelegate

.. inheritance-diagram:: ogswidgets.delegates.IntegerDelegate

.. inheritance-diagram:: ogswidgets.delegates.TextDelegate

.. inheritance-diagram:: ogswidgets.delegates.DoubleDelegate

.. inheritance-diagram:: ogswidgets.delegates.ComboDelegate

.. inheritance-diagram:: ogswidgets.delegates.FilePickerDelegate

.. inheritance-diagram:: ogswidgets.delegates.DirPickerDelegate

.. inheritance-diagram:: ogswidgets.delegates.PropertyDelegate

.. automodule:: ogswidgets.delegates
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`fieldsWizard` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.fieldsWizard.FieldSelectionDialog

.. inheritance-diagram:: ogswidgets.fieldsWizard.FileBrowserWidget

.. inheritance-diagram:: ogswidgets.fieldsWizard.FieldsSelector

.. inheritance-diagram:: ogswidgets.fieldsWizard.FieldsWizard

.. automodule:: ogswidgets.fieldsWizard
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`fileBrowser` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.fileBrowser.Highlighter

.. inheritance-diagram:: ogswidgets.fileBrowser.FileBrowser

.. automodule:: ogswidgets.fileBrowser
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`fileCoorWidget` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.fileCoorWidget.FileCoorWidget

.. automodule:: ogswidgets.fileCoorWidget
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`filePicker` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.filePicker.TinyButton

.. inheritance-diagram:: ogswidgets.filePicker.FileValidator

.. inheritance-diagram:: ogswidgets.filePicker.DirectoryPicker

.. inheritance-diagram:: ogswidgets.filePicker.FilePicker

.. automodule:: ogswidgets.filePicker
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`heightWidget` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.heightWidget.HeightWidget

.. automodule:: ogswidgets.heightWidget
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`hsiIndicator` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.hsiIndicator.CompassNeedle

.. inheritance-diagram:: ogswidgets.hsiIndicator.GaugeWidget

.. inheritance-diagram:: ogswidgets.hsiIndicator.CompassWidget

.. inheritance-diagram:: ogswidgets.hsiIndicator.MainWindow

.. inheritance-diagram:: ogswidgets.hsiIndicator.HSIIndicatorNeedle

.. inheritance-diagram:: ogswidgets.hsiIndicator.HSIIndicator

.. inheritance-diagram:: ogswidgets.hsiIndicator.HSIWidget

.. automodule:: ogswidgets.hsiIndicator
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`image` Module
-------------------------------------------------------------------------------

.. automodule:: ogswidgets.image
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`levelsWidget` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.levelsWidget.LevelsWidget

.. automodule:: ogswidgets.levelsWidget
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`lineEditors` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.lineEditors.ValidLineEdit

.. inheritance-diagram:: ogswidgets.lineEditors.DoubleValidator

.. inheritance-diagram:: ogswidgets.lineEditors.AbstractEditor

.. inheritance-diagram:: ogswidgets.lineEditors.IntegerEditor

.. inheritance-diagram:: ogswidgets.lineEditors.FloatEditor

.. inheritance-diagram:: ogswidgets.lineEditors.ComboEditor

.. inheritance-diagram:: ogswidgets.lineEditors.FileEditor

.. inheritance-diagram:: ogswidgets.lineEditors.DirEditor

.. inheritance-diagram:: ogswidgets.lineEditors.DatumEditor

.. inheritance-diagram:: ogswidgets.lineEditors.LineEditor

.. automodule:: ogswidgets.lineEditors
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`progressDialog` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.progressDialog.ThreadedFunc

.. inheritance-diagram:: ogswidgets.progressDialog.ProgressDialog

.. automodule:: ogswidgets.progressDialog
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`projectedWidget` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.projectedWidget.ProjectedWidget

.. automodule:: ogswidgets.projectedWidget
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`qMemoryWidget` Module
-------------------------------------------------------------------------------

.. inheritance-diagram:: ogswidgets.qMemoryWidget.QMemoryWidget

.. inheritance-diagram:: ogswidgets.qMemoryWidget.QMemoryDialog

.. automodule:: ogswidgets.qMemoryWidget
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`sliders` Module
-------------------------------------------------------------------------------

.. automodule:: ogswidgets.sliders
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

Subpackages                                                                     
-------------------------------------------------------------------------------

.. toctree::                                                                    
    :maxdepth: 2  

    ogswidgets.obsolete
