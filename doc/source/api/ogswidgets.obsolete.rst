:mod:`ogswidgets` Obsolete modules
===============================================================================
*coordDialog* classes:

:class:`ogswidgets.obsolete.coordDialog.CoordDialog`

.. inheritance-diagram:: ogswidgets.obsolete.coordDialog.CoordDialog

.. automodule:: ogswidgets.obsolete.coordDialog
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`coordWidget` Module
-------------------------------------------------------------------------------
*coordWidget* classes:

:class:`ogswidgets.obsolete.coordWidget.TinyButton`

.. inheritance-diagram:: ogswidgets.obsolete.coordWidget.TinyButton

:class:`ogswidgets.obsolete.coordWidget.CoordWidget`

.. inheritance-diagram:: ogswidgets.obsolete.coordWidget.CoordWidget

*coordWidget:*

.. automodule:: ogswidgets.obsolete.coordWidget
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`projDialog` Module
-------------------------------------------------------------------------------
*projDialog* classes:

:class:`ogswidgets.obsolete.projDialog.ProjDialog`

.. inheritance-diagram:: ogswidgets.obsolete.projDialog.ProjDialog

:class:`ogswidgets.obsolete.projDialog.SearchWidget`

.. inheritance-diagram:: ogswidgets.obsolete.projDialog.SearchWidget

*projDialog:*

.. automodule:: ogswidgets.obsolete.projDialog
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:

:mod:`projMVC` Module
-------------------------------------------------------------------------------
*projMVC* classes:

:class:`ogswidgets.obsolete.projMVC.Odict`

.. inheritance-diagram:: ogswidgets.obsolete.projMVC.Odict

:class:`ogswidgets.obsolete.projMVC.BranchNode`

.. inheritance-diagram:: ogswidgets.obsolete.projMVC.BranchNode

:class:`ogswidgets.obsolete.projMVC.LeafNode`

.. inheritance-diagram:: ogswidgets.obsolete.projMVC.LeafNode

:class:`ogswidgets.obsolete.projMVC.ProjModel`

.. inheritance-diagram:: ogswidgets.obsolete.projMVC.ProjModel

:class:`ogswidgets.obsolete.projMVC.ProjView`

.. inheritance-diagram:: ogswidgets.obsolete.projMVC.ProjView

:class:`ogswidgets.obsolete.projMVC.ProjValidator`

.. inheritance-diagram:: ogswidgets.obsolete.projMVC.ProjValidator

:class:`ogswidgets.obsolete.projMVC.UserDefinedDialog`

.. inheritance-diagram:: ogswidgets.obsolete.projMVC.UserDefinedDialog

:class:`ogswidgets.obsolete.projMVC.SaveEPSGDialog`

.. inheritance-diagram:: ogswidgets.obsolete.projMVC.SaveEPSGDialog

*projMVC:*

.. automodule:: ogswidgets.obsolete.projMVC
    :members:
    :exclude-members: __dict__, __module__, __weakref__
    :special-members:
    :undoc-members:
    :show-inheritance:
