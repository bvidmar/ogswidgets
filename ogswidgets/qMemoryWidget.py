""" A Memory Usage Widget based on QWidget

    Author:
      - 20111202-20120214 Roberto Vidmar
      - 20180518 R. Vidmar Python 3/Qt5

"""

import os
import psutil
from . import QtCore, Qt, QtGui, QtWidgets

# Local imports

#------------------------------------------------------------------------------
def bytes2human(n):
    # http://code.activestate.com/recipes/578019
    # >>> bytes2human(10000)
    # '9.8K'
    # >>> bytes2human(100001221)
    # '95.4M'
    symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
    prefix = {}
    for i, s in enumerate(symbols):
        prefix[s] = 1 << (i + 1) * 10
    for s in reversed(symbols):
        if n >= prefix[s]:
            value = float(n) / prefix[s]
            return '%.1f%s' % (value, s)
    return "%sB" % n

#==============================================================================
class QMemoryWidget(QtWidgets.QWidget):
    """ A Memory Widget class
        R. Vidmar, 20180517

    .. note:: Color names are from
                http://www.december.com/html/spec/colorsvg.html
    """
    def __init__(self, pid=None, updateTime=None, parent=None):
        """ Create a new instance of the QMemoryWidget.

            Args:
                pid (int): process id
                updateTime (float): widget update time in ms
                parent (QtWidgets.QWidget): parent widget
        """
        super(QMemoryWidget, self).__init__(parent)
        self._pid = pid
        self.totalmem = {
                'pcentmin' : 100.,
                'pcentmax': 0.,
                'init' : psutil.virtual_memory().percent,
                }
        self._proc = psutil.Process(pid)
        self.appmem = {
                'pcentmin' : 100.,
                'pcentmax' : 0.,
                'init' : self._proc.memory_percent(),
                }
        self.setMinimumSize(500, 80)
        self.totalmem['current'] = self.totalmem['init']
        self.appmem['current'] = self.appmem['init']
        self._painter = QtGui.QPainter()
        self.demo = False
        self.color_initial = 'midnightblue'
        self.color_current = 'lightsteelblue'
        self.color_min = 'green'
        self.color_max = 'red'
        # Update the widget automatically?
        if updateTime is not None:
            self._timer = QtCore.QTimer(self)
            self._timer.timeout.connect(self.updateUsage)
            self._timer.start(updateTime)

    def drawMeter(self, i, meminfo):
        """ Draw the meter:

            Args:
                meminfo (dict): memory usage dictionary
                                all values are in [0..100]
        """
        def toWidth(fract):
            return int(fract * w / 100)

        # Get widget size
        size = self.size()
        w = size.width()
        h = size.height()

        vertBorder = 5
        # transform fraction in width
        v0 = toWidth(meminfo['init'])
        v = toWidth(meminfo['current'])
        vmin = toWidth(meminfo['pcentmin'])
        vmax = toWidth(meminfo['pcentmax'])
        y0 = int(i * h / 2)
        rech = int((i + 1) * h / 2 - vertBorder)
        qp = self._painter
        # Draw current rectangle
        qp.setBrush(QtGui.QColor(self.color_current))
        qp.drawRect(0, y0, v, rech)

        # Draw initial bar
        qp.setBrush(QtGui.QColor(self.color_initial))
        qp.drawRect(v0 - 2, y0, 4, rech)

        # Draw minimum
        qp.setBrush(QtGui.QColor(self.color_min))
        qp.drawRect(vmin - 1, y0, 2, rech)

        # Draw maximum
        qp.setBrush(QtGui.QColor(self.color_max))
        qp.drawRect(vmax - 1 , y0, 2, rech)

    def name(self):
        """ Return process name of current pid
        """
        return self._proc.name()

    def paintEvent(self, e):
        """ Draw the widget
        """
        qp = self._painter
        qp.begin(self)

        # Set fonts
        font = QtGui.QFont('Arial', 10, QtGui.QFont.Light)
        qp.setFont(font)

        # Get widget size
        size = self.size()
        w = size.width()
        h = size.height()

        # Get memory information
        self.totalmem['current'] = self.totalPercent()
        if self.totalmem['current'] < self.totalmem['pcentmin']:
            self.totalmem['pcentmin'] = self.totalmem['current']
        if self.totalmem['current'] > self.totalmem['pcentmax']:
            self.totalmem['pcentmax'] = self.totalmem['current']
        self.appmem['current'] = self.appPercent()
        if self.appmem['current'] < self.appmem['pcentmin']:
            self.appmem['pcentmin'] = max(self.appmem['current'], 0)
        if self.appmem['current'] > self.appmem['pcentmax']:
            self.appmem['pcentmax'] = min(self.appmem['current'], 99.9)
        totmem = bytes2human(psutil.virtual_memory().total)
        used = bytes2human(psutil.virtual_memory().used)
        appused = bytes2human(self._proc.memory_info().rss)

        totmsg = "Memory %s (%s %%) of %s" % (
               used, self.totalmem['current'], totmem)
        appmsg = "Used by app %s (%.1f %%) of %s" % (
               appused, self.appmem['current'], totmem)

        # Draw the meters
        qp.setPen(Qt.NoPen)
        # Upper meter:
        self.drawMeter(0, self.totalmem)
        # Lower meter:
        self.drawMeter(1, self.appmem)

        # Fill with text messages
        pen = QtGui.QPen(QtGui.QColor(20, 00, 00), 1, Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(Qt.NoBrush)
        # Measure message lenght for upper bar and draw
        a = QtGui.QFontMetrics(font)
        x = int((w / 2) - (a.horizontalAdvance(totmsg)) / 2)
        qp.drawText(x, int(h / 4 + 3), totmsg)
        # Measure message lenght for lower bar and draw
        x = int((w / 2) - (a.horizontalAdvance(appmsg)) / 2)
        qp.drawText(x, int(h * 3 / 4 + 3), appmsg)
        qp.end()

    def updateUsage(self):
        """ Called internally by a QTimer.
            If self.demo is True values must be set by user
        """
        if not self.demo:
            self.totalmem['current'] = psutil.virtual_memory().percent
            self.appmem['current'] = self._proc.memory_percent()
        self.update()

    def appPercent(self):
        return self.appmem['current']

    def totalPercent(self):
        return self.totalmem['current']

#==============================================================================
class QMemoryDialog(QtWidgets.QDialog):
    """ Use this class in implementation
    """

    def __init__(self, pid=None, parent=None):
        super(QMemoryDialog, self).__init__(parent)

        layout = QtWidgets.QVBoxLayout()
        self.mw = QMemoryWidget(pid, updateTime=300)
        layout.addWidget(self.mw)
        self.setLayout(layout)
        self.setWindowTitle('Memory used by pid %s (%s)' % (
                pid, self.mw.name()))

#==============================================================================
class TestMemoryDialog(QMemoryDialog):
    """ Only for test purpouses
    """

    def __init__(self, pid=None, parent=None):
        super().__init__(pid=pid, parent=parent)

    def keyPressEvent(self, event):
        self.mw.demo = True
        if event.key() == Qt.Key_Left:
            self.mw.appmem['current'] -= 2.0
        elif event.key() == Qt.Key_Right:
            self.mw.appmem['current'] += 2.0

#==============================================================================
if __name__ == "__main__":
  import signal, sys

  signal.signal(signal.SIGINT, signal.SIG_DFL)

  app = QtWidgets.QApplication(sys.argv)
  w = TestMemoryDialog(int(sys.argv[1])  if len(sys.argv) > 1 else None)
  w.show()
  app.exec_()

