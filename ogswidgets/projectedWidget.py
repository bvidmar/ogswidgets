""" A Projected Coordinates Widget

    Author:
      - 20111202-20120110 Roberto Vidmar <rvidmar@inogs.it>
        Nicola Creati <ncreati@inogs.it>
      - 20180523 Roberto Vidmar PyQt5 and Python3

    License:
        MIT/X11 License (see :download:`license.txt <../../../license.txt>`)
"""
from . import QtCore, Qt, QtWidgets, Signal

for cls in ("QCoreApplication".split()):
    globals()[cls] = getattr(QtCore, cls)

for cls in ("QApplication QVBoxLayout QWidget".split()):
    globals()[cls] = getattr(QtWidgets, cls)

QCA = QCoreApplication

from . abstractFeditor import UFEditor

#==============================================================================
class ProjectedWidget(QWidget):
    """ A Widget that handles projected coordinates
    """
    textEditedSignal = Signal(object)

    def __init__(self, *args, **kargs):
        """ Create a new instance of the widget.

            The first argument, if present, is the value to assign.

            The following keyword arguments are understood:

            - name      : name of the widget
                        Default is ''
            - coordLen  : number of figures in float number
                        Default is 7
            - decimals  : number of decmals for the representation
                        Default is 3
            - align     : alignment of the widget.
                        Default is Qt.AlignCenter
            - readonly  : read only attribute.
                        Default is False
        """
        if len(args) > 1:
            value = args[1]
        else:
            value = None
        if len(args) > 0:
            parent = args[0]
        else:
            parent = None
        super(ProjectedWidget, self).__init__(parent)

        self.coordLen = kargs.pop('coordLen', 7)
        self.decimals = kargs.pop('decimals', 3)
        self.align = kargs.pop('align', Qt.AlignCenter)
        self.readonly = kargs.pop('readonly', False)
        self.setObjectName(kargs.pop('name', ''))

        self.editor = UFEditor(self)
        name = str(self.objectName())
        if name:
            self.editor.setToolTip(
                    QCA.translate("ProjectedWidget",
                                  "Enter here %s in meters.decimals") %
                    name.lower())
        else:
            self.editor.setToolTip(
                    QCA.translate("ProjectedWidget",
                                  "Enter here meters.decimals"))
        self.editor.setReadOnly(self.readonly)
        self.editor.textEdited.connect(self.textEdited)

        # Layout
        self._layout = QVBoxLayout(self)
        self._layout.addWidget(self.editor)
        self._layout.setContentsMargins(0, 0, 0, 0)

        if value is not None:
            self.setValue(value)

    def textEdited(self, text):
        """ Update all widgets except the one that sent the signal

            :param text: the content of the editor widget
            :type text: string
            :raises:
        """
        self.textEditedSignal.emit(self.sender().fvalue())

    def fvalue(self, rep=None):
        """ Return widget float value(s) according to rep
            Default is current representation

            :param rep: representation **(this parameter is unused)**
            :type rep: any
            :returns: widget float value(s) according to rep
            :rtype: list
            :raises:
        """
        return [float(v) for v in self.tvalue(rep)]

    def tvalue(self, rep=None):
        """ Return widget tuple string value according to rep
            Default is current representation

            :param rep: representation **(this parameter is unused)**
            :type rep: any
            :returns: widget tuple string value according to rep
            :rtype: tuple
            :raises:
        """
        return (self.editor.value(), )

    def svalue(self, rep=None):
        """ Return widget string value according to rep

            Default is current representation

            :param rep: representation **(this parameter is unused)**
            :type rep: any
            :returns: widget string value according to rep
            :rtype: tuple
            :raises:
        """
        return self.editor.svalue()

    def setValue(self, value):
        """ Set widget value.

            :param value: new float value
            :type value: float
            :raises:
        """
        self.editor.setValue(value)
#==============================================================================
if __name__ == '__main__':
    import sys
    from signal import signal, SIGINT, SIG_DFL

    signal(SIGINT, SIG_DFL)
    app = QApplication(sys.argv)
    x = 4000000.
    w1 = ProjectedWidget(None,
            x,
            name='Ghiottoni'
    )
    w1.show()
    sys.exit(app.exec_())
