""" EPSG Coordinate Reference System Tree Model / View

    Author:
      - 20111221-20120110 Roberto Vidmar <rvidmar@inogs.it>
      - Nicola Creati <ncreati@inogs.it>
      - 20180524 R. Vidmar

    License:
        MIT/X11 License (see :download:`license.txt <../../../license.txt>`)
"""

import os
import sys
import sqlite3
from pyproj import (Proj, _datadir, get_authorities, get_codes,
        pj_list, get_authorities, get_codes, CRS)
from collections import UserDict, OrderedDict

from . import QtCore, Qt, QtGui, QtWidgets

for cls in ("QCoreApplication QAbstractItemModel QModelIndex"
        " QItemSelectionModel".split()):
    globals()[cls] = getattr(QtCore, cls)

for cls in ("QValidator QColor QFont QPixmap QPalette".split()):
    globals()[cls] = getattr(QtGui, cls)

for cls in ("QVBoxLayout QLabel QDialogButtonBox QLayout QTreeView"
        " QAbstractItemView QHeaderView QDialog QLineEdit".split()):
    globals()[cls] = getattr(QtWidgets, cls)

QCA = QCoreApplication

CUSTOMPROJECTION = QCA.translate("crsMVC", "User defined Coordinate System")
CUSTOMCOLOR = QCA.translate("crsMVC", "lightsteelblue")
EPSGBRANCHCOLOR = QCA.translate("crsMVC", "lightgray")
CUSTOMSEPCHR = "|"
DATUMSHIFT_KEYWORD = "+datumShift="
GEOIDGRIDS_KEYWORD = "+geoidgrids="
EXTRA_KEYWORDS = (DATUMSHIFT_KEYWORD, GEOIDGRIDS_KEYWORD)


#------------------------------------------------------------------------------
def loadEPSG(projdb="/usr/share/proj/proj.db"):
    """ Return an ordered dictionary with epsg data from proj.db

        Returns:
            OrderedDict: ordered dictionary with epsg data from proj.db
    """
    epsgDict = OrderedDict()
    # Load all epsg codes:
    db = sqlite3.connect(projdb)
    cursor = db.cursor()
    geodetic_epsgs = [epsg[0] for epsg in cursor.execute(
           ("SELECT code FROM geodetic_crs WHERE auth_name = 'EPSG'"))]
    projected_epsgs = [epsg[0] for epsg in cursor.execute(
           ("SELECT code FROM projected_crs WHERE auth_name = 'EPSG'"))]
    geodetic_names = [name[0] for name in cursor.execute(
           ("SELECT name FROM geodetic_crs  WHERE auth_name = 'EPSG'"))]
    projected_names = [name[0] for name in cursor.execute(
           ("SELECT name FROM projected_crs  WHERE auth_name = 'EPSG'"))]

    for name, epsg in zip(geodetic_names, geodetic_epsgs):
        # Split names at '/'
        left, _, right = name.partition('/')
        if left in epsgDict:
            # Append
            epsgDict[left].append((epsg, name, 'geo'))
        else:
            # Create
            epsgDict[left] = [(epsg, name, 'geo')]
    for name, epsg in zip(projected_names, projected_epsgs):
        # Split names at '/'
        left, _, right = name.partition('/')
        if left in epsgDict:
            # Append
            epsgDict[left].append((epsg, name, 'pro'))
        else:
            # Create
            epsgDict[left] = [(epsg, name, 'pro')]

    return epsgDict


#==============================================================================
class Branch:
    """ Branch Node implementation
    """
    def __init__(self, name, parent=None):
        """ Create a new Branch instance.

            Args:
                name (str): Name of the branch
                parent (Branch): parent node
        """
        self._name = name
        self._parent = parent
        self._children = []

    def __len__(self):
        return len(self._children)

    def __str__(self):
        return self._name

    def parent(self):
        """ Return parent node

            Returns:
                Branch: parent node
        """
        return self._parent

    def children(self):
        return self._children

    def name(self):
        """ Return node name

            Returns:
                str: node name
        """
        return self._name

    def __repr__(self):
        return self.__str__()

    def childAtRow(self, row):
        return self._children[row]

    def appendChild(self, child):
        child._parent = self
        self._children.append(child)

    def removeChildAtRow(self, row):
        """ Remove child at row `row`

            Args:
                row (int):  number of the row to remove (starting from 0)
        """
        assert 0 <= row < len(self._children)
        self._children.pop(row)

    def rowOfChild(self, child):
        for i, item in enumerate(self._children):
            if item == child:
                return i
        return -1

    def isBranch(self):
        """ Return True

            Returns:
                bool: True
        """
        return True

    def isLeaf(self):
        """ Return False

            Returns:
                bool: True
        """
        return False


#==============================================================================
class Leaf:
    """ Leaf Node implementation
    """

    def __init__(self, entry, parent=None):
        """ Create a new Leaf instance.

            Args:
                entry (tuple): (epsg, name)
                parent (Branch): parent node
        """
        self._parent = parent
        self._name = entry[1]
        self._epsg = entry[0].strip()
        self._type = entry[2].strip()

    def __len__(self):
        return 3

    def children(self):
        """ Return node children: the empty list

            Returns:
                list: []
        """
        return []

    def __str__(self):
        return "%s: %7s '%s'" % (self._name, self._epsg, self._type)

    def __repr__(self):
        return self.__str__()

    def parent(self):
        """ Return parent node

            Returns:
                Branch: parent node
        """
        return self._parent

    def childAtRow(self, row):
        return self._parent.childAtRow(row)

    def field(self, column):
        if column == 0:
            return self._name
        elif column == 1:
            return self._epsg
        elif column == 2:
            return self._type

    def name(self):
        """ Return node name

            Returns:
                str: node name
        """
        return self._name

    def setName(self, name):
        """ Set node name to `name`

            Args:
                name (str): node name
        """
        self._name = name

    def epsg(self):
        """ Return EPSG code

            Args:
                name (str): EPSG code
        """
        return self._epsg

    def projStr(self):
        """ Return CRS name

            Return:
                str: CRS name
        """
        return CRS.from_epsg(self._epsg).name

    def isGeodetic(self):
        """ Return True if CRS is geodetic

            Return:
                bool: True if CRS is geodetic
        """
        return self._type == 'geo'

    def isProjected(self):
        """ Return True if CRS is projected

            Return:
                bool: True if CRS is projected
        """
        return self._type == 'pro'

    def getMyRow(self):
        """ Return row of this node into model

            Returns:
                int: row of this node into model
        """
        return self._parent.rowOfChild(self)

    def isBranch(self):
        """ Return False

            Returns:
                bool: False
        """
        return False

    def isLeaf(self):
        """ Return True

            Returns:
                bool: True
        """
        return True


#==============================================================================
class CRSModel(QAbstractItemModel):
    """ Coordinate Reference System TreeView Model
    """
    CUSTOMFILENAME = os.path.join("~", ".custom_projections")

    def __init__(self, parent=None):
        """ Create a new CRSModel instance.

            Args:
                parent (QObject): parent object
        """
        super().__init__(parent)
        self.headers = [
                QCA.translate("CRSModel", "Coordinate Reference System"),
                QCA.translate("CRSModel", "EPSG"),
                QCA.translate("CRSModel", "Proj.4")
        ]
        self.columns = len(self.headers)
        self.root = Branch('')

    def columnCount(self, parent):
        """ Return number of columns in the model

            Args:
                parent (QModelIndex): parent node

            Returns:
                int: number of columns in the model
        """
        return self.columns

    def rightmostIndex(self, index):
        """ Return index of rightmost item in the row of index

            Args:
                index (QModelIndex): index of an intem in the tree

            Returns:
                QModelIndex: QModelIndex of rightmost item in the row of index
        """
        node = self.nodeFromIndex(index)
        return self.createIndex(index.row(), self.columns, node.parent())

    def makeNewName(self, sepchr='-'):
        """ Return a new name for a user defined CRS

            Args:
                sepchr (str): separation character

            Returns:
                str: new name for a user defined CRS
        """
        name = QCA.translate("CRSModel", "New Projection") + sepchr + '1'
        namelist = [
                child.name() for child in self.root.children()[0].children()
        ]
        if name not in namelist:
            return name
        else:
            if sepchr in name:
                base, dummy, ext = name.rpartition(sepchr)
            else:
                base, ext = name, '0'
            i = 1
            name = "%s%s%d" % (base, sepchr, i)
            while name in namelist:
                i += 1
                name = "%s%s%d" % (base, sepchr, i)
            return name

    def addCustom(self, value, epsgString=''):
        """ Return QModelIndex of new user defined (custom) CRS

            .. note:: NEW DATA WILL BE ADDED TO THE MODEL

            Args:
                value (str): custom projection
                epsgString (str): optional EPSG string

            Returns:
                QModelIndex: QModelIndex of new user defined (custom) CRS
        """
        new = (epsgString, self.makeNewName(), value)
        userBranch = self.root.children()[0]
        # Create a new (custom) projection
        leaf = Leaf(new, userBranch)

        self.layoutAboutToBeChanged.emit()
        row = len(userBranch)
        self.beginInsertRows(self.createIndex(row, 0, userBranch), row, row)
        userBranch.appendChild(leaf)
        leafIndex = self.createIndex(row, 0, leaf)
        self.endInsertRows()
        self.layoutChanged.emit()
        return leafIndex

    def loadCustom(self, branch):
        """ Load custom projections from file to branch node

            Args:
                branch (Branch): branch node
        """
        custom = self.root.children()[0]
        try:
            fid = open(os.path.expanduser(self.CUSTOMFILENAME), 'r')
            projections = fid.readlines()
            if projections:
                for line in projections:
                    if line[0] == '#':
                        # Comment
                        comment = line[2:-1]
                    else:
                        elements = line.split(CUSTOMSEPCHR)
                        name = elements[0]
                        projString = elements[1].strip()
                        epsgString = elements[2].strip()
                        # Append a projection
                        customTuple = (epsgString, name, projString)
                        custom.appendChild(Leaf(customTuple, branch))
        except IOError:
            pass

    def saveCustom(self):
        """ Save custom projections to well known :class:`CUSTOMFILENAME`
        """
        userBranch = self.root.children()[0]
        # Save the projection in our custom projections file
        fid = open(os.path.expanduser(self.CUSTOMFILENAME), 'w')
        for custom in userBranch.children():
            fid.write("%s%s  %s%s %s\n" % (custom.name(), CUSTOMSEPCHR,
                                           custom.projStr(), CUSTOMSEPCHR,
                                           custom.epsg()))
        fid.close()

    def setData(self, index, value, role=Qt.DisplayRole):
        """ Custom Reimplementation of the `setData` method.

            .. note:: DATA IN THE MODEL WILL CHANGE AFTER THIS CALL

            .. note:: After changing the node data, models must emit the
                dataChanged() signal to inform other components of the change.

            .. warning:: We go through setData twice if we press <Enter>
        """
        success = False
        if (
                index.internalPointer() == self.root.children()[0] or
                index.parent().internalPointer() == self.root.children()[0]):
            userBranch = self.root.children()[0]
            if role == Qt.EditRole:
                #svalue = str(value.toString())
                svalue = value
                if not svalue:
                    return False
                success = True
                # Avoid duplicates
                for child in index.parent().internalPointer().children():
                    if child.name() == svalue:
                        success = False
                        break
                if success:
                    #index.internalPointer().setName(str(value.toString()))
                    index.internalPointer().setName(value)

        if success:
            self.dataChanged.emit(index, self.rightmostIndex(index))
            # Update file with custom projections
            self.saveCustom()

        return success

    def data(self, index, role):
        """ Custom Reimplementation of the `data` method.

            Returns the data stored under the given role for the item
            referred to by the index
        """
        node = index.internalPointer()
        if role == Qt.DecorationRole:
            if index.column() == 0:
                if isinstance(node, Branch):
                    if node.name() == CUSTOMPROJECTION:
                        return QPixmap(':/icon_user.png')
                    isProjected = node.children()[0].isProjected()
                else:
                    isProjected = node.isProjected()

                if isProjected:
                    return QPixmap(':/icon_projection.png')
                else:
                    return QPixmap(':/icon_geographic.png')
        elif role == Qt.FontRole:
            if isinstance(node, Branch):
                myFont = QFont()
                myFont.setBold(True)
                return myFont
        elif role == Qt.BackgroundRole:
            if isinstance(node, Branch):
                if node.name() == CUSTOMPROJECTION:
                    return QColor(CUSTOMCOLOR)
                else:
                    return QColor(EPSGBRANCHCOLOR)
        elif role == Qt.TextAlignmentRole:
            return int(Qt.AlignTop | Qt.AlignLeft)
        elif role == Qt.ToolTipRole:
            toolTip = ""
            userToolTip = ""
            if (index.internalPointer() == self.root.children()[0]
                        or index.parent().internalPointer() ==
                        self.root.children()[0]):
                userBranch = self.root.children()[0]
                # User defined area
                userToolTip = QCA.translate(
                        "CRSModel", ".\nRight click on this item to add\n"
                        "your custom Coordinate System.\n"
                        "Double click to rename it\n"
                        "Press <DEL> key to delete it.")

            if isinstance(node, Branch):
                if index.column() == 0:
                    toolTip = QCA.translate("CRSModel",
                                            'The "%s" group%s') % (str(node),
                                                                   userToolTip)
            else:
                # Leaf Node
                if index.column() == 0:
                    if userToolTip:
                        toolTip = (QCA.translate(
                                "CRSModel", 'User defined projection "%s"%s')
                                   % (node.field(index.column()), userToolTip))
                    else:
                        toolTip = QCA.translate(
                                "CRSModel",
                                'EPSG projection "%s"') % node.field(
                                        index.column())
                elif index.column() == 1:
                    if node.field(1):
                        if userToolTip:
                            toolTip = (QCA.translate(
                                    "CRSModel", "Original EPSG code was %s%s")
                                       % (node.field(1), userToolTip))
                        else:
                            toolTip = QCA.translate(
                                    "CRSModel", "EPSG code is %s%s") % (
                                            node.field(1), userToolTip)
                    else:
                        toolTip = userToolTip[2:]
            return toolTip
        elif role == Qt.DisplayRole:
            if isinstance(node, Branch):
                return str(node) if index.column() == 0 else ""
            else:
                return node.field(index.column())
        return

    def flags(self, index):
        """ Custom Reimplementation of the `flags` method.

            Return the item flags for the given index.
            The base class implementation returns a combination of flags that
            enables the item (ItemIsEnabled) and allows it to be selected
            (ItemIsSelectable). See Qt Documentation.
        """
        if (index.parent().internalPointer() == self.root.children()[0]
                    and index.column() == 0):
            return (Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsEditable)
        else:
            return Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def headerData(self, section, orientation, role):
        """ Set header value model
        """
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.headers[section]
        return

    def index(self, row, column, parentIndex):
        """ Custom Reimplementation of the `index` method.

            Return the QModelIndex for the element at (row, column) given
            parentIndex

        .. note:: Must be implemented
        """
        assert self.root
        branch = self.nodeFromIndex(parentIndex)
        if row < len(branch):
            return self.createIndex(row, column, branch.childAtRow(row))
        else:
            return QModelIndex()

    def load(self):
        """ Populate the QTreeModel loading data with `loadEPSG()`
        """
        epsgDict = loadEPSG()
        # Custom Projections Branch
        custom = Branch(CUSTOMPROJECTION)
        self.root.appendChild(custom)

        # First the EPSG file
        for key in epsgDict.keys():
            branch = Branch(key)
            self.root.appendChild(branch)

            for entry in epsgDict[key]:
                branch.appendChild(Leaf(entry, branch))

        # Then user defined projection
        self.beginResetModel()
        self.loadCustom(branch)
        #self.reset()
        self.endResetModel()
        return

    def indexOfCol0(self, index):
        """ Return QModelIndex of item at column 0

            Args:
                index (QModelIndex): QModelIndex

            Returns:
                QModelIndex: QModelIndex of item at column 0
        """
        return self.createIndex(index.row(), 0, index.internalPointer())

    def nodeFromIndex(self, index):
        """ Return node (Branch or Leaf) instance from QModelIndex

            Args:
                index (QModelIndex): QModelIndex

            Returns:
                (Branch or Leaf): instance from QModelIndex
        """
        return index.internalPointer() if index.isValid() else self.root

    def parent(self, child):
        """ Return parent QModelIndex of `child`

            Args:
                child (QModelIndex): QModelIndex

            Returns:
                QModelIndex: QModelIndex of parent
        """
        node = self.nodeFromIndex(child)
        if node is None:
            return QModelIndex()
        parent = node.parent()
        if parent is None:
            return QModelIndex()
        grandparent = parent.parent()
        if grandparent is None:
            return QModelIndex()
        row = grandparent.rowOfChild(parent)
        return self.createIndex(row, 0, parent)

    def removeRecord(self, index):
        """ Remove only items in the first branch (user defined records).

            Args:
                index (QModelIndex): QModelIndex of the node to remove

            Returns:
                bool: True on success
        """
        userBranch = self.root.children()[0]
        node = self.nodeFromIndex(index)
        if node.parent() == userBranch:
            row = index.row()
            self.layoutAboutToBeChanged.emit()
            self.beginRemoveRows(self.createIndex(0, 0, userBranch), row, row)
            userBranch.removeChildAtRow(row)
            self.endRemoveRows()
            self.layoutChanged.emit()
            return True
        return False

    def rowCount(self, parent):
        """ Return number of rows in parent tree

            Args:
                parent (QModelIndex): parent QModelIndex

            Returns:
                int: number of rows in parent node
        """
        node = self.nodeFromIndex(parent)
        if node is None or isinstance(node, Leaf):
            return 0
        return len(node)

    def search(self, s, matchCase=False, exact=False):
        """ Recursively search the model for string s in (name, epsg)

            Args:
                s (str): string to search
                matchCase (bool): ignore case in search if True
                exact (bool): exact match if True, contained if False

            Returns:
                list: list of QModelIndex that match
        """
        s = s.strip()
        foundItems = []

        def match(s, ss):
            if matchCase:
                if exact:
                    return s == ss
                else:
                    return s in ss
            else:
                if exact:
                    return s.upper() == ss.upper()
                else:
                    return s.upper() in ss.upper()

        def loop(node):
            """ Iterate over the tree model
            """
            for row, child in enumerate(node.children()):
                if isinstance(child, Branch):
                    loop(child)
                else:
                    # Leaf
                    if match(s, child.name()):
                        foundItems.append(self.createIndex(row, 1,
                                child.childAtRow(row)))
                    elif match(s, child.epsg()):
                        foundItems.append(self.createIndex(row, 0,
                                child.childAtRow(row)))
        loop(self.root)
        return foundItems


#==============================================================================
class CRSView(QTreeView):
    """ Projection main TreeView based on MVC design
    """
    def __init__(self, parent=None):
        """ Create a new CRS View instance.

            Args:
                parent (QObject): parent object
        """
        super().__init__(parent)

        self.setUniformRowHeights(True)
        self.setAlternatingRowColors(True)
        self.setSelectionMode(QAbstractItemView.SingleSelection)

        # Context menu
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.contextMenuRequested)
        self.customProjStr = ''

    def resizeView(self):
        """ Resize columns so that everything fits into the view
        """
        self.header().resizeSections(QHeaderView.ResizeToContents)

    def customAdded(self, newIndex):
        """ Save file and update view after new User Defined projection has
            been added at newIndex

            Args:
                newIndex (QModelIndex): update view at this QModelIndex
        """
        # Update user file
        self.model().saveCustom()
        # Set current index
        self.setCurrentIndex(newIndex)
        # Clear previous selection
        self.clearSelection()
        # Select current index
        self.selectionModel().select(
                newIndex,
                QItemSelectionModel.Select | QItemSelectionModel.Rows)
        # Expand branch
        self.expand(newIndex.parent())
        self.resizeView()

    def contextMenuRequested(self, pos):
        """ Pop up a UserDefinedDialog on user request
        """
        index = self.currentIndex()
        if (index.parent() == self.rootIndex() and index.row() == 0
                    or index.parent().internalPointer() == self.model()
                    .root.children()[0]):
            # New User Defined Datum / Projection Dialog
            dlg = UserDefinedDialog(self.customProjStr)
        elif index.parent() != self.rootIndex():
            # Standard EPSG or Coordinate reference System
            node = index.internalPointer()
            dlg = SaveEPSGDialog(node.projStr(), node.epsg())
        else:
            # Coordinate reference System
            dlg = None

        if dlg:
            if dlg.exec_():
                self.customProjStr, epsgString = dlg.text()
                newIndex = self.model().addCustom(self.customProjStr,
                                                  epsgString)
                self.customAdded(newIndex)

    def keyPressEvent(self, event):
        """ Custom reimplementation of the `keyPressEvent` method to delete
            records using  `Qt.Key_Delete`
        """
        if event.key() == Qt.Key_Delete:
            # Delete key was pressed, remove current item
            result = self.model().removeRecord(self.currentIndex())
            if result:
                # Update view selecting current index
                self.selectionModel().select(
                        self.currentIndex(),
                        QItemSelectionModel.Select | QItemSelectionModel.Rows)

                # Update user file
                self.model().saveCustom()
                return
        else:
            super().keyPressEvent(event)


#==============================================================================
class ProjValidator(QValidator):
    """ Validate string against PROJ4 syntax.

        .. note:: This validator stores its status for later usage
    """

    def __init__(self, parent):
        """ Create a new ProjValidator instance.

            Args:
                parent (QObject): parent object
        """
        super().__init__(parent)
        self.extraKeys = EXTRA_KEYWORDS
        self._status = QValidator.Acceptable

    def validate(self, *args):
        """ Validate proj data string:

            .. warning:: *** THIS DOES NOT WORK AS EXPECTED ***
                Some strings seem valid but yeld unexpected results.
                This depends on the `Proj` class implementation.
        """
        status = QValidator.Intermediate
        palette = self.parent().palette()
        buttonBox = self.parent().parent().buttonBox

        # Search for extra keyword(s)
        text = str(args[0])

        for extraKey in self.extraKeys:
            b, key, e = text.partition(extraKey)
            if key == extraKey:
                # extra keyword found, parse its value
                pn = e.split()[0]
                # Remove extra from the string
                text = text.replace(extraKey + pn, '')

        try:
            p = Proj(text)
        except (RuntimeError, UnicodeEncodeError) as e:
            pass
        else:
            status = QValidator.Acceptable

        if status == QValidator.Acceptable:
            color = QColor('white')
            buttonBox.button(QDialogButtonBox.Ok).setEnabled(True)
        else:
            color = QColor('SALMON')
            buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        palette.setColor(QPalette.Base, color)
        self.parent().setPalette(palette)
        self._status = status
        return status, args[0]

    def status(self):
        """ Return True **ONLY IF** the status is `QValidator.Acceptable`

            Returns:
                bool: True if the value is `QValidator.Acceptable`
        """
        if self._status == QValidator.Acceptable:
            return True
        else:
            return False


#==============================================================================
class UserDefinedDialog(QDialog):
    """ A simple class to handle a User Defined Datum/Projection Proj.4 String
    """

    def __init__(self, text='', parent=None, title=CUSTOMPROJECTION):
        """ Create a new UserDefinedDialog instance.

            Args:
                text (str): text of the dialog
                parent (QObject): parent object
                title (str): dialog title
        """
        super().__init__(parent)
        self.setWindowTitle(title)

        self.customEditor = QLineEdit(text)
        self.customEditor.setValidator(ProjValidator(self.customEditor))
        self.customEditor.setToolTip(
                QCA.translate(
                        "UserDefinedDialog",
                        "Enter here a valid PROJ.4 string\n"
                        "The string is validated while you type.\n"
                        "If the string is NOT valid you cannot save it.\n"
                        "WARNING... a valid PROJ.4 string\n"
                        "does NOT mean that it is correct!"))
        layout = QVBoxLayout()
        label = QLabel(
                QCA.translate("UserDefinedDialog",
                              "Enter a valid PROJ.4 string below:"))
        label.setToolTip(
                QCA.translate(
                        "UserDefinedDialog",
                        "Below you can define your own Datum/Projection:\n"
                        "Use the standard PROJ.4 syntax plus the keywords\n"
                        "+datumShift= and +geoidgrids= to define custom\n"
                        "Datum Shift and Geoid (.gtx) files.\n"))
        layout.addWidget(label)
        layout.addWidget(self.customEditor)

        self.buttonBox = QDialogButtonBox(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel
                                          | QDialogButtonBox.Ok)
        self.buttonBox.rejected.connect(self.reject)
        self.buttonBox.accepted.connect(self.accept)

        l = QVBoxLayout(self)
        l.setSizeConstraint(QLayout.SetFixedSize)
        l.addLayout(layout)
        l.addWidget(self.buttonBox)
        # Validate current text string
        self.customEditor.validator().validate(text, len(text))

    def text(self):
        """ Return QLineEdit content as string

            Returns:
                str: QLineEdit text content
        """
        return str(self.customEditor.text()), ''

    def closeEvent(self, event):
        """ Close dialog **only if** the validator agrees on content
        """
        if self.customEditor.validator().status():
            super(UserDefinedDialog, self).close()
        else:
            event.ignore()


#==============================================================================
class SaveEPSGDialog(QDialog):
    """ A simple class to handle a User Defined Datum/Projection Proj.4 String
    """
    def __init__(self, text='', epsg='', parent=None, title=CUSTOMPROJECTION):
        """ Create a new SaveEPSGDialog instance.

            Args:
                text (str): text
                epsg (str): optional epsg string
                parent (QObject): parent object
        """
        super().__init__(parent)
        self.setWindowTitle(title)

        self._text = text
        self._epsg = epsg

        layout = QVBoxLayout()
        layout.addWidget(
                QLabel(QCA.translate(
                       "SaveEPSGDialog",
                       "Save current EPSG projection among user's defined?"
                        )))

        self.buttonBox = QDialogButtonBox(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel
                                          | QDialogButtonBox.Ok)
        self.buttonBox.rejected.connect(self.reject)
        self.buttonBox.accepted.connect(self.accept)

        l = QVBoxLayout(self)
        l.setSizeConstraint(QLayout.SetFixedSize)
        l.addLayout(layout)
        l.addWidget(self.buttonBox)

    def text(self):
        """ Return QLineEdit content and epsg as string

            Returns:
                tuple: QLineEdit content and epsg string
        """
        if self._epsg:
            epsg = str(self._epsg)
        else:
            # Guess it from proj4 string
            s = str(self._text).upper()
            splitted = s.split('EPSG:')
            if len(splitted) > 1:
                rest = splitted[1]
                epsg = rest.split()[0]
            else:
                epsg = ''

        return str(self._text), epsg
