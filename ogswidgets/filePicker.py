""" A class to choose input/output files interactively.

    Author:
      - 20091202-20100321 Nicola Creati <ncreati@inogs.it>
      - 20111202-20120214 Roberto Vidmar <rvidmar@inogs.it>
      - 20180522 Roberto Vidmar ported to Python3 Qt5

    License:
        MIT/X11 License (see :download:`license.txt <../../../license.txt>`)
"""

import os

from . import QtCore, Qt, QtGui, QtWidgets, Signal
for cls in ("QCoreApplication QFileInfo QDir".split()):
    globals()[cls] = getattr(QtCore, cls)

for cls in ("QValidator QColor QPalette".split()):
    globals()[cls] = getattr(QtGui, cls)

for cls in ("QHBoxLayout QWidget QLineEdit QPushButton"
        " QApplication QFileDialog".split()):
    globals()[cls] = getattr(QtWidgets, cls)


# Local imports
from . fileBrowser import FileBrowser

QCA = QCoreApplication


#==============================================================================
class TinyButton(QPushButton):
    """ A QPushButton with a size fitted to the button label
    """

    def __init__(self, label, parent):
        """ Create a new TinyButton instance.

            Args:
                label (str): label to put on the tiny button
                parent (QWidget): parent widget
        """
        super(TinyButton, self).__init__(label, parent)

        textWidth = self.fontMetrics().boundingRect(self.text()).width()
        self.setMaximumWidth(textWidth + 12)
        self.setFocusPolicy(Qt.NoFocus)


#==============================================================================
class FileValidator(QValidator):
    def __init__(self, parent=None):
        """ Create a new FileValidator instance.

            Args:
                parent (QWidget): parent widget
        """
        super(FileValidator, self).__init__(parent)

    def validate(self, path, pos):
        """ Reimplementation of the validate method.

            Args:
                path (str): file path to check
                pos (int): cursor position

            Returns:
                QValidator.Acceptable or QValidator.Intermediate: result of
                    the validation
        """
        if isinstance(path, tuple):
            path, selectedFilter = path
        palette = self.parent().palette()

        if QFileInfo(path).isFile():
            palette.setColor(QPalette.Base, QColor('white'))
            self.parent().setPalette(palette)
            result = QValidator.Acceptable
        else:
            palette.setColor(QPalette.Base, QColor('SALMON'))
            self.parent().setPalette(palette)
            result = QValidator.Intermediate
        return result


#==============================================================================
class DirectoryPicker(QWidget):
    """ Directory picker class
    """
    pathChanged = Signal(object)

    def __init__(self, parent=None, toolTip=''):
        super(DirectoryPicker, self).__init__(parent)

        # Fill with widgets
        self._filePath = QLineEdit(self)
        self._browse = TinyButton(u"\N{DOWNWARDS DOUBLE ARROW}", self)

        # Layout
        layout = QHBoxLayout(self)
        layout.addWidget(self._filePath)
        layout.addWidget(self._browse)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)

        # Signals
        self._browse.clicked.connect(self._chooseDir)

    def _chooseDir(self):
        """ Select directory path

            Emit :class:`pathChanged` signal
        """
        if self._filePath.text():
            path = self._filePath.text()
        else:
            path = QDir.homePath()
        directory = QFileDialog.getExistingDirectory(
                self, 'Choose directory', path, QFileDialog.ShowDirsOnly)
        self._filePath.setText(str(directory))
        self.pathChanged.emit(str(directory))


#==============================================================================
class FilePicker(QWidget):
    """ A File picker class with integrated file browser.
    """
    pathChanged = Signal(str)
    InputMode = 0
    OutputMode = 1

    def __init__(self,
                 parent=None,
                 pn=None,
                 filt=u'*',
                 extension='=',
                 mode=InputMode,
                 home='',
                 viewer=None,
                 toolTip=None):
        """ Create a new instance.

            Args:
                parent (QWidget): parent widget
                pn (str): optional pathname
                filt (str): file extension filter
                extension (str): extension to use for the viewer (default
                    is full pathname). *Valid only if viewer is defined.*
                mode (int): either :class:`InputMode` or :class`OutputMode`
                home (str): open picker in this directory
                viewer (class): an optional file viewer class whose init
                    accepts parent and path keyword arguments
                toolTip (str): An optional toolTip
        """
        # Attributes
        self._filter = filt
        self._directory = home
        self._viewer = None
        self._ext = extension
        self._path = None
        if viewer == 'default':
            self.viewerClass = FileBrowser
        else:
            self.viewerClass = viewer

        # Create the widget
        super(FilePicker, self).__init__(parent)
        # populate it
        if pn and os.path.isfile(pn):
            self._editor = QLineEdit(pn, self)
        else:
            self._editor = QLineEdit(self)

        self._browse = TinyButton(u"\N{DOWNWARDS DOUBLE ARROW}", self)

        # Layout
        layout = QHBoxLayout(self)
        layout.addWidget(self._editor)
        layout.addWidget(self._browse)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)

        # Signals
        self._editor.editingFinished.connect(self.fileSelected)
        self._browse.clicked.connect(self._selectFile)

        if self.viewerClass:
            self._viewBtn = TinyButton(
                    u"\N{LOWER RIGHT DROP-SHADOWED WHITE SQUARE}", self)
            layout.addWidget(self._viewBtn)
            self._viewBtn.clicked.connect(self._showFile)
        else:
            self._viewBtn = None

        self.setMode(mode, toolTip)

    def setMode(self, mode, toolTip='', caption=None):
        """ Set Widget mode: either :class:`InputMode` or :class:`OutputMode`
            A caption of None defaults to `mode`

            Args:
                mode (int): either :class:`InputMode` or :class:`OutputMode`
                toolTip (str): An optional toolTip
                caption (str): optional caption
        """
        if mode == self.InputMode:
            self._dialog = QFileDialog.getOpenFileName
            self._caption = QCA.translate("filePicker", "Open file")
            self._browse.setToolTip(QCA.translate("filePicker",
                    "Click to select the input file"))
            if toolTip:
                self._editor.setToolTip(toolTip)
            else:
                self._editor.setToolTip(QCA.translate("filePicker",
                        "Enter here the name of the file to convert"))
            self._editor.setValidator(FileValidator(self._editor))
        else:
            # self.OutputMode
            self._dialog = QFileDialog.getSaveFileName
            self._caption = QCA.translate("filePicker", "Save to")
            self._browse.setToolTip(QCA.translate("filePicker",
                    "Click to select the output file"))
            if toolTip:
                self._editor.setToolTip(toolTip)
            else:
                self._editor.setToolTip(QCA.translate("filePicker",
                        "Enter here the name of the output file"))
            self._editor.setValidator(None)
        self._mode = mode
        if caption is not None:
            self._caption = caption

        # Enable drops
        self.setAcceptDrops(True)

    def dragEnterEvent(self, evt):
        """ Reimplemented to tell the drag and drop system that we can only
            handle plain text

            Args:
                evt (QtCore.QEvent): event
        """
        if evt.mimeData().hasUrls():
            evt.accept()
        else:
            evt.ignore()

    def dropEvent(self, evt):
        """ Reimplemented to copy the first dropped url into the editor

            Args:
                evt (QtCore.QEvent): event
        """
        if evt.mimeData().hasUrls():
            evt.setDropAction(Qt.CopyAction)
            evt.accept()

            url = evt.mimeData().urls()[0]
            pn = str(url.toLocalFile())
            self._editor.setText(pn)
            self.fileSelected()
        else:
            evt.ignore()

    def text(self):
        """ Return text content

            Returns:
                str: text content
        """
        return self._editor.text()

    def _selectFile(self):
        """ Select file using an appropriate dialog
        """
        options = QFileDialog.DontUseNativeDialog
        if not self._editor.text():
            absPath = self._directory
        else:
            absPath = os.path.abspath(self._editor.text())
        fileName = self._dialog(
                self,
                self._caption,
                absPath,
                filter=self._filter,
                options=options)[0]

        if fileName:
            self._editor.setText(fileName)
            self.fileSelected()

    def fileSelected(self):
        """ File has been selected: emit the Signal
        """
        if self._path != self._editor.text():
            self._path = self._editor.text()
            self.pathChanged.emit(self._path)

    def _showFile(self):
        """ Show ascii file in a viewer
        """
        if self._editor.text():
            if self._viewer:
                self._viewer.close()
            if self._ext == '=':
                path = str(self._editor.text())
            else:
                path = (os.path.splitext(str(self._editor.text()))[0] +
                        self._ext)
            self._viewer = self.viewerClass(parent=self._viewBtn, path=path)
            self._viewer.resize(640, 480)
            self._viewer.setWindowTitle('%s' % path)
            self._viewer.show()


#==============================================================================
if __name__ == "__main__":
    import sys
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    app = QApplication(sys.argv)
    f = FilePicker(
            pn=__file__,
            home=QDir.homePath(),
            filt='Python modules (*.py)',
            viewer='default')
    #f = FilePicker(home=QDir.homePath())
    f.show()
    sys.exit(app.exec_())
