""" A Module to manage Exceptions in PySide2 / PyQt5 applications

    Author:
      - 2009-2012 Nicola Creati
      - 2009-2012 Roberto Vidmar
      - 20180608 Roberto Vidmar PyQt5 Python3

    License:
        MIT/X11 License (see :download:`license.txt <../../../license.txt>`)
"""
import os
import sys
import time
from tempfile import gettempdir
from ogsutils.exceptionTrace import formatTraceback, environmentInfo

# Local imports
from . import Qt, QtGui, QtWidgets
from .console import ConsoleWidget

#==============================================================================
def ExceptionHook(exctype, value, trace):
    """ The Handler function for all unhandled exceptions of the running
        QtWidgets.QApplication.

        Args:
            exctype (Exception): exception type
            value (error value): error
            trace (traceback): the traceback instance
    """
    traceBack, message, frame = formatTraceback(exctype, value, trace)

    QtWidgets.QApplication.instance().unhandledException(
            ("Environment", environmentInfo()),
            ("Trace", traceBack),
            ("Variables", message),
            ("IPython Console", frame),
    )


#==============================================================================
class TraceBackApp(QtWidgets.QApplication):
    def __init__(self, *args, **kargs):
        super(TraceBackApp, self).__init__(*args, **kargs)
        # Save sys.excepthook
        self._realExcepthook = sys.excepthook
        sys.excepthook = ExceptionHook

    def unhandledException(self, *args):
        dlg = ExceptionDialog(self._realExcepthook, *args)
        dlg.exec_()

#==============================================================================
class ExceptionDialog(QtWidgets.QDialog):
    def __init__(self, realExceptHook, *args):
        """ Create a new ExceptionDialog instance.

            Args:
                realException (sys.excepthook): the system sys.excepthook
                *args (unhandledException args):
        """
        # Restore sys.excepthook
        sys.excepthook = realExceptHook

        super(ExceptionDialog, self).__init__(None)
        self._t0 = time.localtime(time.time())
        self._args = args
        self.frame = args[-1][1]
        self._dumped = False
        self._console = None
        self._consoleIndex = None
        self._detailedIndexes = []
        name = QtWidgets.QApplication.instance().objectName()
        self.setWindowTitle("Unhandled Exception in '%s'....." % name)

        errMessage = (
                "%s has raised an exception and will be terminated") % name

        icon = QtWidgets.QLabel()
        pixmap = self.style().standardPixmap(
                QtWidgets.QStyle.SP_MessageBoxCritical)
        icon.setPixmap(pixmap)
        label = QtWidgets.QLabel(errMessage)
        showAdvanced = QtWidgets.QCheckBox("Show detailed traceback")
        dumpToFile = QtWidgets.QCheckBox("Dump to file")
        ipythonConsole = QtWidgets.QCheckBox("IPython console")
        self._buttonBox = QtWidgets.QDialogButtonBox(self)
        self._buttonBox.setOrientation(Qt.Horizontal)
        self._buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Ok
                                           | QtWidgets.QDialogButtonBox.Ignore)
        self._tabWidget = QtWidgets.QTabWidget()
        self._tabWidget.setHidden(True)

        self._mainLayout = QtWidgets.QVBoxLayout(self)

        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(label)
        v = QtWidgets.QVBoxLayout()
        v.addWidget(showAdvanced)
        v.addWidget(dumpToFile)
        v.addWidget(ipythonConsole)
        self._hLayout = QtWidgets.QHBoxLayout()
        self._hLayout.addWidget(icon)
        self._hLayout.addLayout(v, stretch=1)
        vLayout.addLayout(self._hLayout)
        self._mainLayout.addLayout(vLayout)
        self._mainLayout.addWidget(self._buttonBox)
        self._mainLayout.addWidget(self._tabWidget)

        # Signals
        self._buttonBox.clicked.connect(self._onClick)
        showAdvanced.stateChanged.connect(self._detailed)
        dumpToFile.stateChanged.connect(self._dumpToFile)
        ipythonConsole.stateChanged.connect(self._toggleConsole)

    def _toggleConsole(self, flag):
        if self._console is None:
            customBanner = (
                "\n  'locals' is the f_locals attribute of the frame that "
                "raised the exception."
                "\n  'globals' is the f_globals attribute of the same "
                "frame.\n\n")
            self._console = ConsoleWidget(customBanner=customBanner)
            self._console.set_default_style(colors='linux')
            self._console.setMinimumSize(640, 480)
            #font = QtGui.QFont("Monospace", 10)
            #self._console._control.setFont(font)
            #self._console._control.setCursorWidth(10)
            namespace = dict()
            namespace['locals'] = self.frame.f_locals
            namespace['globals'] = self.frame.f_globals
            self._console.pushVar(namespace)

        if flag:
            self._tabWidget.insertTab(0, self._console, self._args[-1][0])
            self._consoleIndex = 0
            self._tabWidget.setCurrentIndex(self._consoleIndex)
            self._tabWidget.setVisible(flag)
        else:
            self._tabWidget.removeTab(self._consoleIndex)
            self._consoleIndex = None
            if not self._tabWidget.count():
                self._tabWidget.setVisible(flag)
        self.adjustSize()

    def _detailed(self, flag):
        font = QtGui.QFont("Courier New", 11, QtGui.QFont.DemiBold)
        if self._consoleIndex is None:
            i = 0
        else:
            i = 1
        for arg in self._args[:-1]:
            if flag:
                self._tabWidget.insertTab(i, QtWidgets.QTextEdit(), arg[0])
                self._tabWidget.widget(i).setPlainText(arg[1])
                self._tabWidget.widget(i).setReadOnly(True)
                self._tabWidget.widget(i).setCurrentFont(font)
                i += 1
            else:
                self._tabWidget.removeTab(i)

        if flag:
            self._tabWidget.setVisible(flag)
        elif not self._tabWidget.count():
            self._tabWidget.setVisible(flag)
        self.adjustSize()

    def _dumpToFile(self):
        if not self._dumped:
            t0ASCII = time.strftime("%Y%m%d%H%M%S", self._t0)
            tmpfn = os.path.join(gettempdir(),
                                 "%s-Traceback.log" % (t0ASCII))
            fid = open(tmpfn, "w")
            f_locals = self._args[-1][1].f_locals
            for arg in f_locals:
                fid.write(arg[1])
            fid.close()
            label = QtWidgets.QLabel("Dumped to %s" % tmpfn)
            label.setTextInteractionFlags(Qt.TextBrowserInteraction)
            self._mainLayout.addWidget(label)
            self._dumped = True

    def _onClick(self, button):
        if button == self._buttonBox.button(QtWidgets.QDialogButtonBox.Ok):
            self.accept()
        else:
            self.reject()


#==============================================================================
if __name__ == '__main__':
    import signal

    class AWidget(QtWidgets.QWidget):
        def __init__(self):
            super(AWidget, self).__init__()
            print("Dividing by zero!")
            1 / 0


    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = TraceBackApp([])
    f = AWidget()
    f.show()
    app.exec_()
