""" A class to edit properties of an object.

    Author:
      - 20091202-20100321 Nicola Creati <ncreati@inogs.it>
      - 20111202-20120214 Roberto Vidmar <rvidmar@inogs.it>
      - 20180606 Roberto Vidmar PyQt5 Python3

    License:
        MIT/X11 License (see :download:`license.txt <../../../license.txt>`)
"""
from . import QtCore, Qt, QtGui, QtWidgets, Signal
for cls in ("QCoreApplication QLocale".split()):
    globals()[cls] = getattr(QtCore, cls)

for cls in ("QValidator QDoubleValidator QColor QPalette".split()):
    globals()[cls] = getattr(QtGui, cls)

for cls in ("QWidget QLineEdit QHBoxLayout QSizePolicy QSpinBox"
        " QComboBox".split()):
    globals()[cls] = getattr(QtWidgets, cls)

from ogswidgets.obsolete.projDialog import ProjDialog
from ogswidgets.filePicker import FilePicker, TinyButton, DirectoryPicker

QCA = QCoreApplication
#==============================================================================
class ValidLineEdit(QLineEdit):
    """ A Line Edit Class that avoid leaving it if the content is invalid
    """

    def __init__(self, *args, **kargs):
        super(ValidLineEdit, self).__init__(*args, **kargs)

    def focusOutEvent(self, e):
        """ Restore last value if value is less than acceptable

            If validator state is **not** :class:`QValidator.Acceptable`
            restore value to last acceptable one
        """
        v = self.validator()
        if v.state() != QValidator.Acceptable:
            v.parent().parent().restoreValue()


#==============================================================================
class DoubleValidator(QDoubleValidator):
    """ A double validator class that saves its state
    """
    INVALID = QColor('red')
    FIXABLE = QColor('pink')
    CORRECT = QColor('white')

    def __init__(self, bottom=-99, top=99, decimals=2, editor=None):
        """ Create a new DoubleValidator instance.

            Args:
                bottom (float): lower value
                top (float): upper value
                decimals (int): number of decimals
                editor (:class:`ValidLineEdit`): editor
        """
        self._state = None
        super(DoubleValidator, self).__init__(bottom, top, decimals, editor)
        # This sets decimalPoint to dot, the default is from system locale!!!
        # (2 hours of work)
        self.setLocale(QLocale(QLocale.C))

    def validate(self, *args):
        """ Reimplementation of the validate method

            Set widget base palette color according to status

            :returns: validator state
            :rtype: :class:`QValidator.State`
        """
        retval = super(DoubleValidator, self).validate(*args)
        self._state = retval[0]
        pal = self.parent().palette()
        if self._state == QValidator.Acceptable:
            pal.setColor(QPalette.Base, self.CORRECT)
        elif self._state == QValidator.Intermediate:
            pal.setColor(QPalette.Base, self.FIXABLE)
        else:
            pal.setColor(QPalette.Base, self.INVALID)
        self.parent().setPalette(pal)
        return retval

    def fixup(self, s):
        pass

    def state(self):
        """ Return internal state

            :returns: internal state (result of last validation)
            :rtype: :class:`QValidator.State`
        """
        return self._state


#==============================================================================
class AbstractEditor(QWidget):
    """ Abstract Editor Widget class with UNDO :class:`TinyButton`
    """

    def __init__(self, parent=None, toolTip=''):
        # Create the widget
        super(AbstractEditor, self).__init__(parent)

        # Set widget behaviour
        self.setSizePolicy(QSizePolicy.Expanding,
                           QSizePolicy.Expanding)

        self._toolTip = toolTip
        self._editor = None
        self._history = None
        self._undo = TinyButton(u"\N{ANTICLOCKWISE OPEN CIRCLE ARROW}", self)
        self._undo.setToolTip(
                QCA.translate("Abstract Editor", "Restore last value"))

        # Layout
        self._layout = QHBoxLayout(self)
        self._layout.addWidget(self._undo)
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._layout.setSpacing(0)

        # Signal
        self._undo.clicked.connect(self.restoreValue)

    def editor(self):
        """ Return the editor instance

            :returns: the editor instance
            :rtype: editor instance class
        """
        return self._editor

    def history(self):
        """ Return history value

            :returns: history value
            :rtype: depends on editor class
        """
        return self._history

    def restoreValue(self):
        """ Restore value from history setting instance value
        """
        if self._history is not None:
            self.setValue(self._history)

    def setEditor(self, editor):
        """ Set editor instance for this widget
        """
        self._editor = editor
        self._layout.insertWidget(0, editor)

    def setHistory(self, value):
        """ Set history value
        """
        self._history = value

    def setValue(self, value):
        """ Set editor value
        """
        self._editor.setValue(value)

    def value(self):
        """ Return editor value
        """
        return self._editor.value()


#==============================================================================
class IntegerEditor(AbstractEditor):
    """ Integer SpinBox editor
    """

    def __init__(self, parent=None, toolTip='', bottom=-99, top=99, step=1):
        super(IntegerEditor, self).__init__(parent, toolTip)

        self.setEditor(QSpinBox(parent))
        self._editor.setRange(bottom, top)
        self._editor.setSingleStep(step)
        self._editor.setAlignment(Qt.AlignLeft)


#==============================================================================
class FloatEditor(AbstractEditor):
    """ Floating point Line Editor
    """

    def __init__(self, parent=None, toolTip='', bottom=0., top=100.,
                decimals=2):
        super(FloatEditor, self).__init__(parent, toolTip)

        self.setEditor(ValidLineEdit(self))
        self._validator = DoubleValidator(
                bottom, top, decimals, self._editor)
        self._validator.setNotation(DoubleValidator.StandardNotation)
        self._editor.setValidator(self._validator)
        self._editor.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self._editor.setFocus(Qt.TabFocusReason)
        self._editor.setAlignment(Qt.AlignLeft)

    def setValue(self, value):
        """ Set editor value
        """
        self._editor.setText(str(value))

    def value(self):
        """ Return editor value
        """
        return self._editor.text()


#==============================================================================
class ComboEditor(AbstractEditor):
    """ ComboBox editor
    """

    def __init__(self, items=(), parent=None, toolTip=''):
        super(ComboEditor, self).__init__(parent, toolTip)

        self.setEditor(QComboBox(self))
        self._editor.addItems(list(items))
        self._editor.setFocus(Qt.TabFocusReason)

    def setValue(self, idx):
        """ Set editor value AND return it
        """
        return self._editor.setCurrentIndex(idx)

    def value(self):
        """ Return editor value
        """
        return self._editor.currentText()


#==============================================================================
class FileEditor(AbstractEditor):
    """ File Picker editor class
    """

    def __init__(self, parent=None, toolTip='', **kargs):
        super(FileEditor, self).__init__(parent, toolTip)

        # Create the FilePicker widget
        self._fp = FilePicker(parent=parent, toolTip=toolTip, **kargs)
        self.setEditor(self._fp)

    def setValue(self, value):
        """ Set file picker path
        """
        self._editor._editor.setText(value)

    def value(self):
        """ Return file picker path
        """
        return self._editor._editor.text()


#==============================================================================
class DirEditor(AbstractEditor):
    """ Directory Picker editor
    """

    def __init__(self, parent=None, toolTip=''):
        super(DirEditor, self).__init__(parent, toolTip)

        # Create the DirPicker widget
        self._fp = DirectoryPicker()
        self.setEditor(self._fp)

    def setValue(self, value):
        """ Set Directory picker path
        """
        self._editor._filePath.setText(value)

    def value(self):
        """ Return Directory picker path
        """
        return str(self._editor._filePath.text())


#==============================================================================
class DatumEditor(AbstractEditor):
    """ Datum/Projection editor widget
    """
    datumChanged = Signal(object)

    def __init__(self, parent=None, toolTip=''):
        super(DatumEditor, self).__init__(parent, toolTip)

        # Fill with widgets
        w = QWidget(self)
        self._select = TinyButton(u"\N{DOWNWARDS DOUBLE ARROW}", self)
        self._datum = QLineEdit(self)

        # Widget Layout
        layout = QHBoxLayout(w)
        layout.addWidget(self._datum)
        layout.addWidget(self._select)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        self.setEditor(w)

        # Signal
        self._select.clicked.connect(self._selectProj)

    def _selectProj(self):
        """ Open a dialog to select Datum/Projection
        """
        dlg = ProjDialog(self._datum.text(), self)
        dlg.resize(640, 480)
        if dlg.exec_():
            self._datum.setText(dlg.projection())
            self.datumChanged.emit(dlg.projection())

    def setValue(self, value):
        """ Set PROJ.4 string
        """
        self._datum.setText(value)

    def value(self):
        """ Return PROJ.4 string
        """
        return self._datum.text()


#==============================================================================
class LineEditor(AbstractEditor):
    """ Text editor
    """

    def __init__(self, parent=None, toolTip=''):
        super(LineEditor, self).__init__(parent, toolTip)

        self.setEditor(QLineEdit(self))
        self._editor.setFocus(Qt.TabFocusReason)

    def setValue(self, value):
        self._editor.setText(value)

    def value(self):
        return self._editor.text()
