# -*- coding: utf-8 -*-
""" Some frequently used functions

    Author:
      - 2009-2012 Roberto Vidmar <rvidmar@inogs.it>
      - 2009-2012 Nicola Creati <ncreati@inogs.it>
      - 20100607 Roberto Vidmar PyQt5 Python3

    License:
        MIT/X11 License (see :download:`license.txt <../../../license.txt>`)
"""

import os.path
import numpy as np
from osgeo import gdal, gdalconst

from . import QtGui
for cls in ("QImage QColor QPixmap".split()):
    globals()[cls] = getattr(QtGui, cls)

#------------------------------------------------------------------------------
def numpy2qimage(data):
    """ Return a numpy array converted into a QImage.

        :param data: array to convert
        :type data: numpy array
        :returns: array converted into a QImage
        :rtype: QImage
        :raises: ValueError

        .. note:: requires sip >= 4.7.5.

    """

    colortable = None

    if data.dtype in (np.uint8, np.ubyte, np.byte):
        if data.ndim == 2:
            h, w = data.shape
            #image = _aligned(data)
            image = data
            format_ = QImage.Format_Indexed8
            colortable = [QColor(i, i, i).rgb() for i in range(256)]

        elif data.ndim == 3 and data.shape[2] == 3:
            h, w = data.shape[:2]
            image = np.zeros((h, w, 4), data.dtype)
            image[:, :, 2::-1] = data
            image[..., -1] = 255
            format_ = QImage.Format_RGB32

        elif data.ndim == 3 and data.shape[2] == 4:
            h, w = data.shape[:2]
            image = np.require(data, np.uint8, 'CO')  # 'CAO'
            format_ = QImage.Format_ARGB32

        elif data.ndim == 3 and data.shape[0] == 3:
            # RGB array
            h, w = data.shape[1:]
            image = np.zeros((h, w, 4), data.dtype)
            image[:, :, 0] = data[2, :, :]
            image[:, :, 1] = data[1, :, :]
            image[:, :, 2] = data[0, :, :]
            image[..., 3] = 255
            format_ = QImage.Format_RGB32
            result = QImage(image.data, w, h, format_)
        else:
            raise ValueError('unable to convert data: shape=%s, '
                             'dtype="%s"' % (data.shape, np.dtype(data.dtype)))

    elif data.dtype == np.uint16 and data.ndim == 2:
        h, w = data.shape
        #image = _aligned(data)
        image = data
        format_ = QImage.Format_RGB16

    elif data.dtype == np.uint32 and data.ndim == 2:
        h, w = data.shape
        image = np.require(data, data.dtype, 'CO')  # 'CAO'
        format_ = QImage.Format_RGB32

    else:
        raise ValueError('unable to convert data: shape=%s, dtype="%s"' %
                         (data.shape, np.dtype(data.dtype)))

    result = QImage(image.data, w, h, format_)
    result.ndarray = image
    if colortable:
        result.setColorTable(colortable)

    return result


#------------------------------------------------------------------------------
def createThumbNail(pn, pixelSize=32):
    """ Return a small icon from raster file pn

        :param pn: pathname of raster file
        :type pn: string (unicode)
        :param pixelSize: pixel size of the icon (default 32)
        :type pixelSize: int
        :returns: array converted into a QImage
        :rtype: QPixmap
        :raises:
    """

    gdal2Numpy = {
            gdalconst.GDT_Byte: np.uint8,
            gdalconst.GDT_UInt16: np.uint16,
            gdalconst.GDT_Int16: np.int16,
            gdalconst.GDT_Int32: np.int32,
            gdalconst.GDT_UInt32: np.uint32,
            gdalconst.GDT_Float32: np.float32,
            gdalconst.GDT_Float64: np.float64,
            gdalconst.GDT_CFloat32: np.complex64,
            gdalconst.GDT_CFloat64: np.complex128
    }

    retval = None

    bands = (0, 1, 2)
    if pn and os.path.isfile(pn):
        reader = gdal.Open(pn, gdal.GA_ReadOnly)
        if reader:
            # Read raster and fill the numpy array
            xbuf = ybuf = pixelSize
            theArray = np.zeros((ybuf, xbuf, len(bands)),
                                gdal2Numpy[reader.GetRasterBand(1).DataType])
            for b in bands:
                theArray[:, :, b] = np.flipud(
                        reader.GetRasterBand(b + 1).ReadAsArray(
                                0, 0, None, None, xbuf, ybuf))
            del reader
            img = numpy2qimage(theArray)
            img = img.mirrored()
            pix = QPixmap.fromImage(img)
            pix.setMask(pix.createHeuristicMask())
            retval = pix

    return retval
