""" Coordinate Reference System Dialog: a widget to choose CRS from pyproj

    Author:
      - 20111221-20120313 Roberto Vidmar <rvidmar@inogs.it>
      - Nicola Creati <ncreati@inogs.it>
      - 20180524 R. Vidmar

    License:
        MIT/X11 License (see :download:`license.txt <../../../license.txt>`)
"""
from . import QtCore, Qt, QtGui, QtWidgets

for cls in ("QCoreApplication".split()):
    globals()[cls] = getattr(QtCore, cls)

for cls in ("QIcon".split()):
    globals()[cls] = getattr(QtGui, cls)

for cls in ("QVBoxLayout QWidget QPushButton QLineEdit"
        " QHBoxLayout QApplication QDialogButtonBox"
        " QGroupBox QLabel QDialog".split()):
    globals()[cls] = getattr(QtWidgets, cls)

# Local imports
from .crsMVC import (CRSView, CRSModel, SaveEPSGDialog, DATUMSHIFT_KEYWORD,
                     GEOIDGRIDS_KEYWORD, EXTRA_KEYWORDS)

QCA = QCoreApplication

#==============================================================================
class SearchWidget(QWidget):
    """ Search for a string in the projection tree.
        The widget has a button and a QlineEdit field.
    """

    def __init__(self, parent, text=''):
        """ Create a new SearchWidget instance.

            Args:
                parent (QWidget): parent widget
                text (str): the text to search for
        """
        super(SearchWidget, self).__init__(parent)

        self.searchEditor = QLineEdit(text, self)
        self.searchEditor.setToolTip(QCA.translate("SearchWidget",
                "Enter here the string to search for.\n"
                "This string will be searched through\n"
                "all known coordinate reference systems"))
        self.defaultText = QCA.translate("SearchWidget", 'New Search:')
        self.searchBtn = QPushButton(self.defaultText, self)
        self.searchBtn.setToolTip(QCA.translate("SearchWidget",
                "Click here to search for the string to the right\n"
                "among all known coordinate reference system"))
        self.defaultText = QCA.translate("SearchWidget", 'New Search:')
        self.searchBtn.setIcon(QIcon(":/icon_search.png"))

        o = QHBoxLayout()
        o.addWidget(self.searchBtn)
        o.addWidget(self.searchEditor)

        self.setLayout(o)

        # Connect button to search method
        self.searchBtn.clicked.connect(self.parent().search)
        self.searchEditor.textChanged.connect(self.onTextChanged)

    def onTextChanged(self):
        """ Update text contained in the search button
        """
        self.searchBtn.setText(self.defaultText)

    def text(self):
        """ Return text contained in the QLineEdit widget

            Returns:
                str: text contained in the QLineEdit widget
        """
        return self.searchEditor.text()

    def setText(self, text):
        """ Set the text contained in the QLineEdit widget

            Args:
                text (str): new text
        """
        self.searchEditor.setText(text)

    def resetBtnText(self):
        """ Reset the text contained in the Button widget to its default
        """
        self.searchBtn.setText(self.defaultText)

    def setBtnText(self, text):
        """ Set the text contained in the Button widget to text

            Args:
                text (str): new text
        """
        self.searchBtn.setText(text)


#==============================================================================
class CRSDialog(QDialog):
    """ Coordinate Reference System selection dialog
    """
    WGS84 = 4326
    def __init__(self, epsg=WGS84, parent=None):
        """ Create a new CRSDialog instance.

            Args:
                crs (str): default epsg
                parent (widget): parent widget
        """
        super().__init__(parent)

        self.setWindowTitle(
                QCA.translate("CRSDialog", "Coordinate Reference Systems"))
        self.foundArg = None
        # Populate the widget
        layout = QVBoxLayout()
        layout.addLayout(self._makeCurrentCRS())
        layout.addWidget(self._makeCRSTree())
        layout.addWidget(self._makeSearchGroup(str(epsg)))
        layout.addWidget(self._makeButtonBox())
        self.setLayout(layout)

        # Start search as if button was pressed
        if self.search(exact=True):
            # Found! Clear search field and button
            self.searchWidget.resetBtnText()
        else:
            dlg = SaveEPSGDialog(epsg, '')
            if dlg.exec_():
                customProjStr, epsgString = dlg.text()
                newIndex = model.addCustom(customProjStr, epsgString)
                self.tree.customAdded(newIndex)
        self.searchWidget.setText('')

    def _makeCRSTree(self):
        # CRS Tree
        self.tree = CRSView(self)
        # Set the tree model
        model = CRSModel()
        model.load()

        self.tree.setModel(model)
        self.tree.setColumnHidden(2, True)
        self.tree.resizeView()
        # Signals
        self.tree.clicked.connect(self.onCurrentChanged)
        self.tree.selectionModel().currentChanged.connect(
                self.onCurrentChanged)
        return self.tree

    def _makeCurrentCRS(self):
        currentLabel = QLabel(QCA.translate("CRSDialog",
                "Current Coordinate Reference System:  "))
        currentLabel.setToolTip(QCA.translate("CRSDialog",
                "The current Coordinate Reference System"))
        self.currentCRSLabel = QLabel()
        self.currentCRSLabel.setToolTip(QCA.translate("CRSDialog",
                "The selected Coordinate Reference System"))
        self.currentCRSLabel.setTextInteractionFlags(
                Qt.TextBrowserInteraction)

        f = self.currentCRSLabel.font()
        f.setBold(True)
        self.currentCRSLabel.setFont(f)
        self.currentCRSLabel.setWordWrap(True)
        self.currentCRSLabel.setAlignment(Qt.AlignLeft)
        currentCRS = QHBoxLayout()
        currentCRS.addWidget(currentLabel, alignment=Qt.AlignTop)
        currentCRS.addWidget(
                self.currentCRSLabel, stretch=1, alignment=Qt.AlignTop)
        return currentCRS


    def _makeButtonBox(self):
        # Dialog standard buttons
        self.buttonBox = QDialogButtonBox(self)
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel
                                          | QDialogButtonBox.Ok)
        # Signals
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        return self.buttonBox

    def _makeSearchGroup(self, crs):
        # Search widget
        self.searchWidget = SearchWidget(self, crs)
        searchGroup = QGroupBox(QCA.translate("CRSDialog",
                "Search in CRS description or EPSG code:"))
        searchGroup.setToolTip(QCA.translate("CRSDialog",
                "You can search for any text through all\n"
                "known Coordinate Reference Systems\nand EPSG codes"))
        searchGroup.setStyleSheet(" background-color: qlineargradient("
                                  "x1: 0, y1: 0, x2: 0, y2: 1, "
                                  "stop: 0 #E0E0E0, stop: 1 #FFFFFF); ")

        v = QVBoxLayout()
        v.addWidget(self.searchWidget)
        searchGroup.setLayout(v)
        return searchGroup

    def keyPressEvent(self, event):
        """ Reimplementation of keyPressEvent to disable default behaviour
            of Return key (i.e. trigger the Ok button).
        """
        if event.key() == Qt.Key_Escape:
            self.reject()
        elif event.key() == Qt.Key_Enter or event.key() == Qt.Key_Return:
            self.search()
        else:
            super(CRSDialog, self).keyPressEvent(event)

    def updateCurrent(self, text):
        """ Update current projection string

            Args:
                text (str): new text
        """
        print("updating with", text)
        self.currentCRSLabel.setText(text)

    def onCurrentChanged(self, curIdx=None, preIdx=None):
        item = self.tree.currentIndex().internalPointer()
        if item.isBranch():
            self.updateCurrent('')
        else:
            self.updateCurrent(item.projStr())
            self._epsg = item.epsg()

    def search(self, exact=False):
        """ Search for content of the searchWidget in tree

            Args:
                exact (bool): if False search also substrings
        """
        # Get the argument from the editor
        arg = self.searchWidget.text()

        if arg == '':
            # We don't search anything
            self.searchWidget.resetBtnText()
            self.tree.clearSelection()
            self.tree.collapseAll()
            return
        else:
            if arg != self.foundArg:
                # New Search
                self.foundList = []
                # Reset text
                self.searchWidget.resetBtnText()
                print("searching for <%s>" % arg)
                self.foundList = self.tree.model().search(
                        arg, matchCase=False, exact=exact)
                if len(self.foundList) > 0:
                    # found!
                    self.foundArg = arg
                    self.foundIndex = 0
            else:
                self.foundIndex += 1
                if self.foundIndex == len(self.foundList):
                    self.foundIndex = 0

            if self.foundList:
                howmany = len(self.foundList)
                if howmany == 1:
                    msg = QCA.translate("CRSDialog", "1 of 1")
                else:
                    msg = (QCA.translate("CRSDialog", "Move to %s of %s") % (
                            (self.foundIndex + 1) % howmany + 1, howmany))
                self.searchWidget.setBtnText(msg)
                idx = self.foundList[self.foundIndex]
                self.tree.setCurrentIndex(self.tree.model().indexOfCol0(idx))
                self.tree.resizeView()
                return True
            else:
                return False

    def epsg(self):
        """ Return current epsg string

            Returns:
                str: current epsg string
        """
        return self._epsg


#==============================================================================
if __name__ == "__main__":
    import sys
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    app = QApplication(sys.argv)
    form = CRSDialog()
    form.resize(640, 480)
    if form.exec_():
        print(form.projection())
