import sys
import time
import numpy as np
from . import QtCore, Qt, QtGui, QtWidgets, Signal

DIALCOLOR = "cornflowerblue"
CROSSCOLOR = QtGui.QColor("white")
MIN_UPDATE_TIME = 0.2

#------------------------------------------------------------------------------
colorGroupList = [QtGui.QPalette.ColorGroup(i)
        for i in range(QtGui.QPalette.NColorGroups)]
#colorGroupList is [QPalette.Active, QPalette.Disabled, QPalette.Inactive]

#------------------------------------------------------------------------------
def polar2Pos(p, distance, angle):
    """ Return point at (distance, angle) from p
    """
    if isinstance(p, QtCore.QPoint):
        x, y = p.x(), p.y()
    else:
        x, y = p
    return QtCore.QPoint(int(x + distance * np.cos(angle)),
            int(y - distance * np.sin(angle)))

#------------------------------------------------------------------------------
def colorTheme(dialColor="gray30", borderColor="gray60"):
    """ Return a color palette based on dial and border colors
    """
    dialcolor = QtGui.QColor(dialColor)
    border = QtGui.QColor(borderColor)

    background = QtGui.QColor("green")
    background = dialcolor
    foreground = dialcolor
    foreground = QtGui.QColor("red")

    mid = border.darker(200)
    dark = border.darker(900)
    light = border.lighter(400)

    text = dialcolor.lighter(900) # The dial numbers

    palette = QtGui.QPalette()
    for colorGroup in colorGroupList:
        palette.setColor(colorGroup, QtGui.QPalette.Base, dialcolor)
        palette.setColor(colorGroup, QtGui.QPalette.Window, background)
        palette.setColor(colorGroup, QtGui.QPalette.Mid, mid)
        palette.setColor(colorGroup, QtGui.QPalette.Light, light)
        palette.setColor(colorGroup, QtGui.QPalette.Dark, dark)
        palette.setColor(colorGroup, QtGui.QPalette.Text, text)
        palette.setColor(colorGroup, QtGui.QPalette.WindowText, foreground)
    return palette

#==============================================================================
class GaugeWidget(QtWidgets.QWidget):
    def __init__(self, parent=None, v=None, vrange='auto',
                zero='centered', colorPos='lightskyblue',
                colorNeg='sienna', vertical=True):
        """ Initialize widget
            vrange can be either 'auto' or (vmin, vmax)
            zero   can be 'centered' or 'vmin'

            Colors from http://www.december.com/html/spec/colorsvg.html
        """
        super(GaugeWidget, self).__init__(parent)
        self._painter = QtGui.QPainter()

        if vertical:
            self.setMinimumSize(60, 70)
        else:
            self.setMinimumSize(70, 30)
        self.vertical = vertical
        colorPos = list(QtGui.QColor(colorPos).getHsv())
        colorPos[2] = 100
        self.colorPos = QtGui.QColor()
        self.colorPos.setHsv(colorPos[0], colorPos[1], colorPos[2],
                colorPos[3])

        colorNeg = list(QtGui.QColor(colorNeg).getHsv())
        colorNeg[2] = 100
        self.colorNeg = QtGui.QColor()
        self.colorNeg.setHsv(colorNeg[0], colorNeg[1], colorNeg[2],
                colorNeg[3])
        self.shortTickColor = QtGui.QColor('black')
        self.longTickColor = QtGui.QColor('black')
        self.longTicks = 10
        self.longTickSize = 5
        self.shortTickSize = 3
        self.shortTicksFreq = 2
        self.fontSize = 20
        if vrange != 'auto':
            vmin, vmax = vrange
            self.vrange = vmax - vmin
            self.vminnorm = vmin / self.vrange
            self.vmaxnorm = vmax / self.vrange
            if zero == 'centered':
                self.v0norm = vmax - vmin / self.vrange
            else:
                self.v0norm = vmin / self.vrange
        else:
            self.v0norm = 0
            self.vrange = None
            self.zero = zero
        self._t0 = time.time()
        if v is None:
            self.v = 0.
        else:
            self.v = float(v)

    def value(self):
        """ Return gauge value
        """
        return self.v

    def setValue(self, offTrack):
        self.v = offTrack
        t1 = time.time()
        # Do not repaint if update is less than
        if t1 - self._t0 >= MIN_UPDATE_TIME:
            self._t0 = t1
            self.repaint()

    def paintEvent(self, e):
        self._painter.begin(self)
        # Set fonts
        font = QtGui.QFont('Arial', self.fontSize, QtGui.QFont.Bold)
        self._painter.setFont(font)

        # signum
        sig = -1 if self.v < 0 else 1
        absv = np.abs(self.v)
        # Count figures
        if absv > 1.:
            zeros = 1 + int(np.log10(np.abs(absv)))
            decimals = 0
        else:
            zeros = 0
            decimals = 1
        fmt = "%%%d.%df m" % (zeros + 1, decimals)
        msg = fmt % (sig * absv, )

        # Normalize value
        if self.vrange is None:
            fact = .5 if self.zero == 'centered' else 1
            self.vmaxnorm = fact
            self.vminnorm = -fact
            vnorm = sig * fact * absv / np.power(10, zeros)
        else:
            vnorm = self.v / self.vrange

        if self.vertical:
            self._drawVGauge(vnorm, zeros)
        else:
            self._drawHGauge(vnorm, zeros)

        # Fill with text messages
        pen = QtGui.QPen(QtGui.QColor(20, 20, 20), 1, QtCore.Qt.SolidLine)
        self._painter.setPen(pen)
        self._painter.setBrush(QtCore.Qt.NoBrush)

        # Measure message lenght for bar and draw
        size = self.size()
        w = size.width()
        h = size.height()
        a = QtGui.QFontMetrics(font)
        x = int((w / 2) - (a.horizontalAdvance(msg)) / 2)
        y = int((h / 2) + (a.height()) / 4)
        self._painter.drawText(x, y, msg)
        self._painter.end()

    def _drawVGauge(self, vnorm, zeros):
        """ Draw the meter:
            vnorm:     normalized value
        """
        def toHeight(fract):
            return int(fract * h)

        # Get widget size
        size = self.size()
        w = size.width()
        h = size.height()

        # Draw the colored bar first
        self._painter.setPen(QtCore.Qt.NoPen)
        if vnorm < self.v0norm:
            color = self.colorNeg
        else:
            color = self.colorPos
        hsv = list(color.getHsv())
        hsv[2] = 130 + 40 * zeros
        color = QtGui.QColor()
        color.setHsv(hsv[0], hsv[1], hsv[2], hsv[3])
        self._painter.setBrush(color)
        y0 = self.vmaxnorm - vnorm
        yh = vnorm - self.v0norm
        self._painter.drawRect(0, toHeight(y0), w, toHeight(yh))

        # Draw the scale
        pen = QtGui.QPen(self.shortTickColor, 1, QtCore.Qt.SolidLine)
        self._painter.setPen(pen)
        # Draw center line
        xc = w // 2
        self._painter.drawLine(xc, 0, xc, h)

        # Draw ticks
        # Short ticks
        x0s = xc - self.shortTickSize
        x1s = x0s + 2 * self.shortTickSize + 1
        lticks = self.longTicks * (zeros + 1)
        sticks = self.shortTicksFreq * lticks
        dhs = (h - 1) / float(sticks)
        for i in range(1, sticks + 1, 2):
            self._painter.drawLine(x0s, round(i * dhs), x1s, round(i * dhs))

        # Long ticks
        x0l = xc - self.longTickSize
        x1l = x0l + 2 * self.longTickSize + 1
        dhl = (h - 1) / float(lticks)
        pen = QtGui.QPen(self.longTickColor, 2, QtCore.Qt.SolidLine)
        self._painter.setPen(pen)
        for i in range(lticks + 1):
            self._painter.drawLine(x0l, round(i * dhl), x1l, round(i * dhl))

    def _drawHGauge(self, vnorm, zeros):
        """ Draw the gauge:
            vnorm:     normalized value
        """
        def toWidth(fract):
            return int(fract * w)

        # Get widget size
        size = self.size()
        w = size.width()
        h = size.height()

        # Draw the colored bar first
        self._painter.setPen(QtCore.Qt.NoPen)
        if vnorm < self.v0norm:
            color = self.colorNeg
        else:
            color = self.colorPos
        hsv = list(color.getHsv())
        hsv[2] = 130 + 40 * zeros
        color = QtGui.QColor()
        color.setHsv(hsv[0], hsv[1], hsv[2], hsv[3])
        self._painter.setBrush(color)
        x0 = self.v0norm - self.vminnorm
        xw = vnorm - self.v0norm
        self._painter.drawRect(toWidth(x0), 0, toWidth(xw), h)

        # Draw the scale
        pen = QtGui.QPen(self.shortTickColor, 1, QtCore.Qt.SolidLine)
        self._painter.setPen(pen)
        # Draw center line
        yc = h // 2
        self._painter.drawLine(0, yc, w, yc)

        # Draw ticks
        # Short ticks
        y0s = yc - self.shortTickSize
        y1s = y0s + 2 * self.shortTickSize + 1
        lticks = self.longTicks * (zeros + 1)
        sticks = self.shortTicksFreq * lticks
        dws = (w - 1) / float(sticks)
        for i in range(1, sticks + 1, 2):
            self._painter.drawLine(round(i * dws), y0s, round(i * dws), y1s)

        # Long ticks
        y0l = yc - self.longTickSize
        y1l = y0l + 2 * self.longTickSize + 1
        dwl = (w - 1) / float(lticks)
        pen = QtGui.QPen(self.longTickColor, 2, QtCore.Qt.SolidLine)
        self._painter.setPen(pen)
        for i in range(lticks + 1):
            self._painter.drawLine(round(i * dwl), y0l, round(i * dwl), y1l)

#==============================================================================
class CompassNeedle(QtCore.QObject):
    """ A simple Compass Needle Widget class
    """
    def __init__(self, parent=None):
        super(CompassNeedle, self).__init__(parent)
        self._palette = QtGui.QPalette()

    def setPalette(self, palette):
        """ Set palette for this object
        """
        self._palette = palette

    def palette(self):
        """ Return our palette
        """
        return self._palette

    def draw(self, painter, center, length, direction, cg):
        width = self.parent().width()
        height = self.parent().height()
        margins = self.parent()._margins
        painter.save()

        painter.translate(width / 2, height / 2)
        painter.rotate(direction)
        scale = min((width - margins) / 120.0, (height - margins) / 120.0)
        painter.scale(scale, scale)
        painter.setPen(QtGui.QPen())
        painter.setBrush(self._palette.brush(QtGui.QPalette.Shadow))
        painter.drawPolygon(
                QtGui.QPolygon([
                QtCore.QPoint(-10, 0), QtCore.QPoint(0, -45),
                QtCore.QPoint(10, 0), QtCore.QPoint(0, 45),
                QtCore.QPoint(-10, 0)]))
        painter.setBrush(self._palette.brush(QtGui.QPalette.Highlight))
        painter.drawPolygon(QtGui.QPolygon([
                QtCore.QPoint(-5, -25), QtCore.QPoint(0, -45),
                QtCore.QPoint(5, -25), QtCore.QPoint(0, -30),
                QtCore.QPoint(-5, -25)]))
        painter.restore()

#==============================================================================
class CompassWidget(QtWidgets.QWidget):
    """ A simple Compass Widget class
    """
    angleChanged = Signal(float)

    def __init__(self, parent=None, dial=True):
        super(CompassWidget, self).__init__(parent)
        self._painter = QtGui.QPainter()
        self._pen = QtGui.QPen()
        self._value = 0.0
        self._margins = 10
        self._pointText = {
                0.:    'N',
                30.0:  '30',
                60.0:  '60',
                90.0:   'E',
                120.0: '120',
                150.0: '150',
                180.0:   'S',
                210.0: '210',
                240.0: '240',
                270.0:   'W',
                300.0: '300',
                330.0: '330'}
        self._dial = dial
        self._value = 0.
        self._origin = 0.
        self._northDegrees = 0
        self.needle = CompassNeedle(self)

    def setValue(self, value):
        """ Set compass value
        """
        self._value = value

    def value(self):
        """ Return compass value
        """
        return self._value

    def setLineWidth(self, width):
        """ Set Border thickness
        """
        pass

    def setMode(self, mode):
        """ Set Mode: rotate scale or rotate needle
        """
        pass

    def setFrameShadow(self, shadow):
        """ Set Frame Shadow: sunken or..
        """
        pass

    def setDirection(self, direction):
        """ Set Compass direcion: Clockwise or Counterclockwise
        """
        pass

    def setOrigin(self, origin):
        """ Set Compass origin: default is 0
        """
        self._origin = origin

    def setLabelMap(self, labelMap):
        """ Set label map for the scale
        """
        self._pointText = labelMap

    def setNeedle(self, needle):
        """ Set the needle for this compass
        """
        self.needle = needle

    def setPenWidth(self, width):
        """ Set the pen with for the scale
        """
        self._pen.setWidth(width)

    def paintEvent(self, event):
        """ repaint() or update() was invoked.
        """
        self._painter.begin(self)
        self._painter.setRenderHint(QtGui.QPainter.Antialiasing)
        self._painter.fillRect(event.rect(),
                self.palette().brush(QtGui.QPalette.Window))
        center = QtCore.QPoint(self.width() // 2 , self.height() // 2)
        radius = min((self.width() - self._margins) / 2,
                (self.height() - self._margins) / 2)
        self.drawScale(self._painter, center, radius, self._origin, 0, 0)
        self.needle.draw(self._painter, center, radius * .8,
                self._northDegrees,
                self.palette().currentColorGroup())
        self._painter.end()

    def drawScale(self, painter, center, radius, origin, minArc, maxArc):
        painter.save()
        painter.translate(self.width() / 2, self.height() / 2)
        scale = min((self.width() - self._margins) / 120.0,
                (self.height() - self._margins) / 120.0)
        painter.scale(scale, scale)

        font = QtGui.QFont(self.font())
        font.setPixelSize(10)
        metrics = QtGui.QFontMetricsF(font)

        painter.setFont(font)
        painter.setPen(self.palette().color(QtGui.QPalette.Shadow))

        i = 0
        textTickLen = 10
        minorTickLen = 5
        offset = 40
        step = 10
        painter.rotate(self._value)
        while i < 360:
            if i % 30 == 0:
                painter.drawLine(0, -offset, 0, -offset - textTickLen)
                if self._dial:
                    painter.drawText(
                            -int(metrics.horizontalAdvance(self._pointText[i])
                                / 2.0),
                            -52, self._pointText[i])
            else:
                painter.drawLine(0, -offset, 0, -offset - minorTickLen)

            painter.rotate(step)
            i += step
        painter.restore()

    def sizeHint(self):
        return QtCore.QSize(400, 400)

    def hdg(self):
        return self.value()

    def setHdg(self, hdg):
        self.setValue(hdg)
        self.update()

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Left:
            self.setHdg(self.hdg() + 5.0)
        elif event.key() == QtCore.Qt.Key_Right:
            self.setHdg(self.hdg() - 5.0)
        else:
            super(CompassWidget, self).keyPressEvent(event)

#==============================================================================
class HSIIndicatorNeedle(CompassNeedle):
    def __init__(self, parent, color):
        super(HSIIndicatorNeedle, self).__init__()
        self._parent = parent
        self._adjust = 0
        self.offTrackBarPct = 60
        palette = parent.palette()
        for colourGroup in colorGroupList:
            palette.setColor(colourGroup, QtGui.QPalette.Text, color)
        self._palette = palette

    def keyPressEvent(self, event):
        print("I'm the needle! event.key() =", event.key())

    def parent(self):
        return self._parent

    def draw(self, painter, center, needleLength, direction, cg):
        """ Draw the needle, i.e. the NON rotating part of the dial
        """
        def drawCenterCross(length):
            l = int(length - 2 * thick)
            painter.drawLine(-l, 0, l, 0)
            painter.drawLine(0, -l, 0, l)

        def drawIndicator(length, direction):
            """ Draw the indicator triangle
            """
            pa = QtGui.QPolygon([
                    polar2Pos(p1, 2 * length, np.radians(direction)),
                    polar2Pos(p1, length, np.radians(direction + 90)),
                    polar2Pos(p1, length, np.radians(direction - 90)), ])
            painter.drawPolygon(pa)

        def drawFromToIndicator(needleLength, length, direction):
            """ The triagle pointing "to" or "from"
            """
            hOffset = int(needleLength / 2)
            vOffset = int(needleLength * .25)
            pbt = QtGui.QPolygon([
                    polar2Pos(p0, 2 * length, np.radians(direction)),
                    polar2Pos(p0, length, np.radians(direction + 90)),
            polar2Pos(p0, length, np.radians(direction - 90)), ])
            pbf = QtGui.QPolygon([
                    polar2Pos(p0, 2 * length, np.radians(direction + 180)),
                    polar2Pos(p0, length, np.radians(direction + 270)),
                    polar2Pos(p0, length, np.radians(direction + 90)),
                    ])
            if self.parent().to():
                # Draw the To triangle
                po = QtCore.QPoint(hOffset, -vOffset)
                painter.translate(po)
                painter.drawPolygon(pbt)
                painter.translate(-po)
            else:
                # Draw the From triangle
                po = QtCore.QPoint(hOffset, +vOffset)
                painter.translate(po)
                painter.drawPolygon(pbf)
                painter.translate(-po)

        def drawOffTrackScale(length):
            # Draw the horizontal scale
            markerSize = round(self.parent().markerSize * length)
            nMarks = self.parent().nMarks
            l = int(length * self.parent().offTrackPct * 0.01)
            # Scale offHeigt: +- offHeightMax means full scale
            offHeight = self.parent().offHeight()
            offHeightMax = self.parent()._offHeightMax
            #if offHeight > offHeightMax:
                #offHeight =  offHeightMax
            vpos = int(l * (-offHeight) / offHeightMax)

            vMarkSpc = l / nMarks
            pen = painter.pen()
            pen.setWidth(2)
            painter.setPen(pen)
            if self.parent().offTrackLine:
                painter.drawLine(-l, vpos, l, vpos)

            if self.parent().offTrackTicks:
                pen.setWidth(markerSize // 4)
                painter.setPen(pen)
                for i in range(1, nMarks + 1):
                    # Right
                    painter.drawLine(
                            int(vMarkSpc * i), int(vpos - markerSize),
                            int(vMarkSpc * i), int(vpos + markerSize))
                    # Left
                    painter.drawLine(
                            int(-vMarkSpc * i), int(vpos - markerSize),
                            int(-vMarkSpc * i), int(vpos + markerSize))

            pen.setWidth(markerSize)
            pen.setCapStyle(QtCore.Qt.RoundCap)
            painter.setPen(pen)
            for i in range(1, nMarks + 1):
                painter.drawPoint(int(vMarkSpc * i), int(vpos))
                painter.drawPoint(int(-vMarkSpc * i), int(vpos))
            return vMarkSpc * 0.1

        def drawVerticalBar(length):
            # Draw the vertical line
            pen = QtGui.QPen(textColor, thick)
            painter.setPen(pen)
            pen.setWidth(thick)
            #pen.setCapStyle(Qt.QtGui.RoundCap)
            painter.setPen(pen)
            cl = length - thick * 0.5
            upperLen = cl * (100 - self.offTrackBarPct) * .01
            painter.drawLine(
                    0, int(- cl + triangleSize * 1.5),
                    0, int(-(cl - upperLen)))
            painter.drawLine(0, int(cl - upperLen), 0, int(cl))
            gap = cl - upperLen
            return gap

        def drawCourseOffTrackBar(length, gap, offTrackToPixels):
            # Draw the Course offTrack Bar
            barLen = gap - thick - 2
            offTrack = self.parent().offTrack()
            # Convert offTrack to pixels
            devPixel = offTrack * offTrackToPixels
            if devPixel < -length:
                devPixel = -length
            elif devPixel > length:
                devPixel = length
            painter.drawLine(
                    int(devPixel), int(barLen), int(devPixel), int(-barLen))

        # Do the draw
        dirRad = direction * np.pi / 180.0
        triangleSize = int(round(needleLength * 0.1))

        p0 = QtCore.QPoint(1, 1)
        p1 = polar2Pos(p0, needleLength - 2 * triangleSize - 2,
                np.radians(direction + 90))

        textColor = self.palette().color(cg, QtGui.QPalette.Text)
        course = self.parent().course()
        course = course

        thick = 1
        pen = QtGui.QPen(QtGui.QColor(self.parent().crossColor), thick)
        painter.setPen(pen)
        painter.save()

        painter.translate(center)
        painter.rotate(course)
        if self.parent().drawCenterCross:
            drawCenterCross(needleLength)
        painter.setBrush(textColor)
        drawIndicator(triangleSize, direction + 90)
        if self.parent().fromto:
            drawFromToIndicator(needleLength, triangleSize, direction + 90)
        if self.parent().nMarks:
            offTrackToPixels = drawOffTrackScale(needleLength)
            thick = int(self.parent().vBarThick * needleLength)
            gap = drawVerticalBar(needleLength)
            drawCourseOffTrackBar(needleLength, gap, offTrackToPixels)
        painter.resetTransform()
        painter.restore()

#==============================================================================
class HSIWidget(QtWidgets.QWidget):
    def __init__(self):
        super(HSIWidget, self).__init__()
        hlayout = QtWidgets.QHBoxLayout()
        vlayout = QtWidgets.QVBoxLayout()

        # The main HSI indicator
        self.centralWidget = HSIIndicator()

        self.leftWidget = GaugeWidget(self.centralWidget)
        hlayout.addWidget(self.leftWidget)
        hlayout.addWidget(self.centralWidget)
        #hlayout.addWidget(CompassWidget())

        # Vertical layout
        vlayout.addLayout(hlayout)
        self.bottomWidget = GaugeWidget(self.centralWidget,
                colorPos='lime', colorNeg='red', vertical=False)
        vlayout.addWidget(self.bottomWidget)
        self.setLayout(vlayout)

    def keyPressEvent(self, event):
        self.centralWidget.keyPressEvent(event)

    def course(self):
        return self.centralWidget.course()

    def setCourse(self, val):
        self.centralWidget.setCourse(val)

    def hdg(self):
        return self.centralWidget.hdg()

    def setHdg(self, val):
        self.centralWidget.setHdg(val)

    def offHeight(self):
        # NOTE: Change to:
        #   return self.centralWidget.offTrack()
        return self.leftWidget.value()

    def offTrack(self):
        return self.centralWidget.offTrack()

    def setOffHeight(self, val):
        self.centralWidget.setOffHeight(val)
        self.leftWidget.setValue(val)

    def setOffTrack(self, val):
        self.centralWidget.setOffTrack(val)
        self.bottomWidget.setValue(val)

    def to(self):
        return self.centralWidget.to()

    def setTo(self, val):
        self.centralWidget.setTo(val)


#==============================================================================
class HSIIndicator(CompassWidget):
    """ The Horizontal Situation Indicator Widget

        Args:
            parent (QtWidgets.QWidget): The parent widget.
            course (float): The course we must follow.
            heading (float): Current heading.
            offtrack (float):
                Current offtrack in [-offTrackMax, offTrackMax].
            offheight (float):
                Current offtrack in [-offHeightMax, offHeightMax].
            to (boolean): False means that course is course + 180°.
            dial boolean): Draw the dial numbers.
            drawCenterCross (boolean): Draw a thin cross in the course
                direction.
            offTrackTicks (boolean): Draw off-track tick lines.
            offTrackLine (boolean): Draw off-track solid line.
            offTrackPct (float): The percentage of the wdget to use for the
                off-track line.
            offTrackMax (float): Clipping value for off-track.
            nMarks (int): The number of off-track marks.
            markerSize (float): The size of the off-track marks.
            vBarThick (float): Thickness of the track indicator bar.
            needleColor (string): Needle main color.
            crossColor (string): Cross and off-track marks color.
            simpleCompass (boolean): If True don't draw off-track widgets
    """
    def __init__(self, parent=None, course=0, heading=0, offtrack=0,
                offheight=0, to=True, dial=True,
                drawCenterCross=True, offTrackTicks=True,
                offTrackLine=True, offTrackPct=75, offTrackMax=60,
                offHeightMax=100,
                nMarks=5, markerSize=0.05, vBarThick=0.06,
                needleColor="yellow", crossColor="white",
                simpleCompass=False):
        self._course = course
        self._heading = heading
        self._offHeight = offheight
        self._offTrack = offtrack
        self._to = True
        self.offTrackTicks = offTrackTicks
        self.offTrackLine = offTrackLine
        self.offTrackPct = offTrackPct
        self._offHeightMax = offHeightMax
        self._offTrackMax = offTrackMax
        self.nMarks = nMarks
        self.markerSize = markerSize
        self.vBarThick = vBarThick
        self.drawCenterCross = drawCenterCross
        self.crossColor = crossColor
        self.fromto = True
        if simpleCompass:
            self.offTrackTicks = False
            self.offTrackLine = False
            self.nMarks = 0
            self.fromto = False

        # Create the compass
        super(HSIIndicator, self).__init__(parent, dial)
        self.setLineWidth(6) # The border thickness
        self.setPalette(colorTheme())

        self.setOrigin(270.0)
        self.setLabelMap({
                0.0:   'N',
                30.0:  '30',
                60.0:  '60',
                90.0:   'E',
                120.0: '120',
                150.0: '150',
                180.0:   'S',
                210.0: '210',
                240.0: '240',
                270.0:   'W',
                300.0: '300',
                330.0: '330'})

        self.setNeedle(HSIIndicatorNeedle(self, QtGui.QColor(needleColor)))
        self.setPenWidth(3)

    def course(self):
        return self._course

    def hdg(self):
        return self.value()

    def offHeight(self):
        """ Return current offheight value
        """
        return self._offHeight

    def offTrack(self):
        """ Return current offtrack value
        """
        return self._offTrack

    def to(self):
        """ Return True if we are heading "TO"
        """
        return self._to

    def setCourse(self, course):
        """ Set course we should follow
        """
        self._course = course
        self.update()

    def setHdg(self, hdg):
        """ Set current heading
        """
        self.setValue(hdg)
        self.update()

    def setOffHeight(self, offHeight):
        if offHeight > self._offHeightMax:
            offHeight = self._offHeightMax
        elif offHeight < -self._offHeightMax:
            offHeight = -self._offHeightMax
        self._offHeight = offHeight
        self.update()

    def setOffTrack(self, offTrack):
        if offTrack > self._offTrackMax:
            offTrack = self._offTrackMax
        elif offTrack < -self._offTrackMax:
            offTrack = -self._offTrackMax
        self._offTrack = offTrack
        self.update()

    def setTo(self, to=True):
        """ Set "we are heading TO" value to True
        """
        self._to = to
        self.update()

    def drawScale(self, painter, center, radius, origin, minArc, maxArc):
        """ Draw the scale with tickmarks
        """
        #radius = 125
        direction = (360.0 - origin) * np.pi / 180.0
        offset = 4
        p0 = polar2Pos(center, offset, direction + np.pi)

        w = self.contentsRect().width()

        painter.save()
        # Draw the scale
        super(HSIIndicator, self).drawScale(
        painter, center, radius, origin, minArc, maxArc)
        painter.restore()

#==============================================================================
class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, options):
        super(MainWindow, self).__init__()

        self.__headingOffset = 0.1
        self.__courseOffset = 0.2
        self.__offHeightOffset = 1
        self.__offTrackOffset = 1
        if options.widget == 'HSI':
            # Build the Attitude Indicator
            self.widget = HSIIndicator(self, offTrackTicks=True,
                    offTrackLine=True, dial=options.dial)
        elif options.widget == 'Compass':
            # Build the Compass
            self.widget = CompassWidget(dial=options.dial)
        else:
            # super!
            self.widget = HSIWidget()
        self.setCentralWidget(self.widget)

        if options.demo:
            # Timers for simulation
            courseTimer = QtCore.QTimer(self.widget)
            courseTimer.timeout.connect(self.changeHeading)
            courseTimer.start(100)
            if options.widget in 'HSI super'.split():
                headingTimer = QtCore.QTimer(self.widget)
                headingTimer.timeout.connect(self.changeCourse)
                headingTimer.start(100)

                offTrackTimer = QtCore.QTimer(self.widget)
                offTrackTimer.timeout.connect(self.changeOffTrack)
                offTrackTimer.start(100)

                offHeightTimer = QtCore.QTimer(self.widget)
                offHeightTimer.timeout.connect(self.changeOffHeight)
                offHeightTimer.start(100)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Up:
            self.widget.setCourse(self.widget.course() + 2)
        elif event.key() == QtCore.Qt.Key_Down:
            self.widget.setCourse(self.widget.course() - 2)
        elif event.key() == QtCore.Qt.Key_Left:
            self.widget.setHdg(self.widget.hdg() + 5.0)
        elif event.key() == QtCore.Qt.Key_Right:
            self.widget.setHdg(self.widget.hdg() - 5.0)
        elif event.key() == QtCore.Qt.Key_Plus:
            self.widget.setOffTrack(self.widget.offTrack() + 5)
        elif event.key() == QtCore.Qt.Key_Minus:
            self.widget.setOffTrack(self.widget.offTrack() - 5)
        elif event.key() == QtCore.Qt.Key_PageUp:
            self.widget.setOffHeight(self.widget.offHeight() + 7)
        elif event.key() == QtCore.Qt.Key_PageDown:
            self.widget.setOffHeight(self.widget.offHeight() - 7)
        elif event.key() == QtCore.Qt.Key_0:
            self.widget.setOffHeight(0)
            self.widget.setOffTrack(0)
        elif event.key() == QtCore.Qt.Key_T:
            self.widget.setTo(not self.widget.to())
        self.widget.keyPressEvent(event)

    def changeHeading(self):
        hdg = self.widget.hdg()
        RANGE = 10
        if hdg > 180.0:
            hdg -= 360.0

        if ((hdg < -RANGE and self.__headingOffset < 0.0 )
                or (hdg > RANGE and self.__headingOffset > 0.0)):
            self.__headingOffset = -self.__headingOffset

        self.widget.setHdg(hdg + self.__headingOffset)
        return

    def changeCourse(self):
        course = self.widget.course()
        RANGE = 10

        if ((course < -RANGE and self.__courseOffset < 0.0 )
                or (course > RANGE and self.__courseOffset > 0.0)):
            self.__courseOffset = -self.__courseOffset

        self.widget.setCourse(course + self.__courseOffset)
        #self.widget.setCourse(45.)

    def changeOffTrack(self):
        ot = self.widget.offTrack()
        RANGE = 50

        if ((ot < -RANGE and self.__offTrackOffset < 0.0 )
                or (ot > RANGE and self.__offTrackOffset > 0.0)):
            self.__offTrackOffset = -self.__offTrackOffset

        self.widget.setOffTrack(ot + self.__offTrackOffset)

    def changeOffHeight(self):
        ot = self.widget.offHeight()
        RANGE = 50

        if ((ot < -RANGE and self.__offHeightOffset < 0.0 )
                or (ot > RANGE and self.__offHeightOffset > 0.0)):
            self.__offHeightOffset = -self.__offHeightOffset

        self.widget.setOffHeight(ot + self.__offHeightOffset)

#==============================================================================
if __name__ == '__main__':
    import os
    import argparse
    from signal import signal, SIGINT, SIG_DFL

    signal(SIGINT, SIG_DFL)
    prog = os.path.basename(sys.argv[0])

    parser = argparse.ArgumentParser(
        description=(
            "\nNavigation widgets. R.Vidmar 20180515"),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        prog=prog)

    parser.add_argument('-d', '--demo', action='store_true',
        default=False,
        help=("Start demo mode: ")
        )
    parser.add_argument('-D', '--dial', action='store_true',
        default=False,
        help=("Show dial: ")
        )
    parser.add_argument('-w', '--widget', choices='Compass HSI super'.split(),
        default='super',
        help=("Select widget: ")
        )
    options = parser.parse_args(sys.argv[1:])

    app = QtWidgets.QApplication(sys.argv)

    main = MainWindow(options)
    #main.setFixedSize(350, 350)
    main.show()

    sys.exit(app.exec_())
