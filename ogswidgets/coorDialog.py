""" A Coordinate Dialog Module.

    The CoorDialog class handles a dialog for converting coordinate pairs or
    files from one Coordinate Reference System to another interactively.

    Author:
        - 20111227-20120111 Roberto Vidmar
        - 20180521 R. Vidmar

    Copyright: 2011-2012
        - Nicola Creati <ncreati@inogs.it>
        - Roberto Vidmar <rvidmar@inogs.it>

    License:
        MIT/X11 License (see :download:`license.txt <../../../license.txt>`)
"""
import sys
import codecs
import time
import numpy as np
from itertools import combinations
from pyproj import Proj, Transformer
from pyproj.transformer import TransformerGroup
from pyproj.crs import CRS
from ogsutils import inherit_docstrings
from ogsutils.degrees import d2dm, d2dms

# Local imports
from . import QtCore, Qt, QtGui, QtWidgets, QShortcut
from . fileCoorWidget import FileCoorWidget
from . filePicker import FilePicker
from . fieldsWizard import FieldsWizard, readFromFile
from . progressDialog import ProgressDialog, ThreadedFunc

__version__ = '0.1.0'
QCA = QtCore.QCoreApplication

moloSartorioWGS84 = (((13.763647000), (45.710551000), 285.800),
        '+proj=latlong +datum=WGS84',
        '+init=epsg:3004 +towgs84=-122.74,-34.27,-22.83,'
        '-1.884,-3.400,-3.030,-15.62', None, 'Molo Sartorio')
moloSartorioETRF2000 = (45.647206667, 13.759398083, 59.579)
moloSartorioETRF2000 = (13.759398083, 45.647206667, 59.579)

moloSartorioUTM33 = (((403768.542, ), (5062631.809, ), 285.8),
        '+proj=utm +zone=33 +ellps=WGS84 +datum=WGS84 +no_defs',
        '+proj=longlat +ellps=intl +pm=rome +no_defs', None,
        'Molo Sartorio')
moloSartorioUTM33_ETRF2000 = (403328.793, 5005599.370, 59.579)

#------------------------------------------------------------------------------
def diffWords(s1, s2):

    # count will contain all the word counts
    count = {}

    # insert words of string s1 to hash
    for word in s1.split():
        count[word] = count.get(word, 0) + 1

    # insert words of string s2 to hash
    for word in s2.split():
        count[word] = count.get(word, 0) + 1

    la = []
    lb = []
    for word in [word for word in count if count[word] == 1]:
        if word in s1:
            la.append(word)
        else:
            lb.append(word)
    return la, lb

#------------------------------------------------------------------------------
def unimplemented():
    """ Display an 'Unimplemented' MessageBox
    """
    mbox = QtWidgets.QMessageBox()
    mbox.setWindowTitle(QCA.translate("UnimplementedMessage", 'Unimplemented'))
    mbox.setIcon(QtWidgets.QMessageBox.Information)
    mbox.setInformativeText( QCA.translate("UnimplementedMessage",
            "This conversion has not been\n"
            "implemented yet."))
    mbox.setStandardButtons(QtWidgets.QMessageBox.Ok)
    mbox.setDefaultButton(QtWidgets.QMessageBox.Ok)
    mbox.button(QtWidgets.QMessageBox.Ok).setText(
            QCA.translate("UnimplementedMessage", 'Ok'))
    mbox.exec_()


#------------------------------------------------------------------------------
def noDatumShift(philamda):
    """ Display a 'noDatumShift' MessageBox
    """
    phi, lamda = philambda
    utfCodec = QtCore.QTextCodec.codecForName("UTF-8")
    QtCore.QTextCodec.setCodecForTr(utfCodec)
    mbox = QtWidgets.QMessageBox()
    mbox.setWindowTitle(QCA.translate("noDatumShift", 'No Datum Shift'))
    mbox.setIcon(QtWidgets.QMessageBox.Information)
    mbox.setInformativeText(QCA.translate( "noDatumShift",
            "Datum shift matrix is undefined in\n"
            "φ = %.9f λ = %.9f." % (phi, lamda)))
    mbox.setStandardButtons(QtWidgets.QMessageBox.Ok)
    mbox.setDefaultButton(QtWidgets.QMessageBox.Ok)
    mbox.button(QtWidgets.QMessageBox.Ok).setText(
            QCA.translate("noDatumShift", 'Ok'))
    mbox.exec_()


#------------------------------------------------------------------------------
def writeASCII(progress,
               quitFunc,
               pn,
               xyz,
               fmt,
               header='',
               recsPerChunk=1000000):
    """ Write xyz (n x npoints) to ASCII File pn in chunks with optional Header

        Args:
            progress (function): progress function. This funtion is called
                at every step.
            quitFunc (function): quit function. This funtion is called
                at every step and if it returns True it's time to quit.
            pn (str): pathname of the file to write
            xyz (numpy.ndarray 3xN): dataset to write
            fmt (str): format to use to output data to ASCII
            header (str): optional header
            recsPerChunk (int): number of records to write at every iteration
                (chunk)
    """
    # Write coordinates to output
    fid = open(pn, "w")

    # Write an optional header
    if header:
        fid.write(header)

    recsWritten = 0
    recsToWrite = xyz.shape[1]
    remainingRecords = recsToWrite
    while remainingRecords:
        # Adjust recsPerChunk
        recsPerChunk = min(recsPerChunk, remainingRecords)
        start = recsToWrite - remainingRecords
        end = start + recsPerChunk

        # Black Magic...
        values = np.ravel(np.vstack((c[start:end] for c in xyz)).T)
        chunk = fmt * recsPerChunk % tuple(values)
        fid.write(chunk)

        # Save number of records actually read
        recsWritten += recsPerChunk
        remainingRecords -= recsPerChunk
        # Memory mapped array?
        #if hasattr(points, 'flush'):
        #  points.flush()
        if quitFunc():
            break
        pcent = 100. * recsWritten / recsToWrite
        progress(pcent)
    fid.close()


#==============================================================================
class HighlightTextEdit(QtWidgets.QTextEdit):
    def __init__(self, text, words, *args, **kargs):
        """ Initialize instance.

            Args:
                words (list): list of words to highlight
        """
        super().__init__(text, *args, **kargs)
        extraselections = []
        for word in words:
            self.find(word)
            extra = self.ExtraSelection()
            color = QtGui.QColor(Qt.cyan).lighter()
            extra.format.setBackground(color)
            extra.cursor = self.textCursor()
            extraselections.append(extra)
        self.setExtraSelections(extraselections)

#==============================================================================
class TransformerDialog(QtWidgets.QDialog):
    """ Class to select a transformation among all available.
    """
    def __init__(self, choices, parent=None):
        """ Initialize instance.

            Args:
                choices (list): list of pyproj.transformer.Transformer
                    nstances
                parent (QWidget): parent widget
        """
        super().__init__(parent)
        self.selected = None
        # Create widgets
        label = QtWidgets.QLabel(
                ("More than one transformation is available.\n"
                "Please select one:"))
        f = label.font()
        f.setBold(True)
        f.setPointSize(12)
        label.setFont(f)

        # Make a button group
        bglayout = QtWidgets.QVBoxLayout()
        self.bg = QtWidgets.QButtonGroup()
        texts = []
        for c in choices:
            texts.append("%s <br/> Pipeline: <br/> %s" % (
                    repr(c).replace('\n', " <br/>"), str(c)))
        comb = combinations(texts, 2)
        diffwords = []
        for c in comb:
            diffwords.append(diffWords(*c))

        tmp = [j for i in diffwords for j in i]
        allwords = [j for i in tmp for j in i]

        for i, c in enumerate(choices):
            rb = QtWidgets.QRadioButton();
            te = HighlightTextEdit(texts[i], allwords);
            te.setReadOnly(True)
            #te.setWordWrap(True)
            hl = QtWidgets.QHBoxLayout()
            hl.addWidget(rb)
            hl.addWidget(te)
            bglayout.addLayout(hl)
            self.bg.addButton(rb, i)
        self.scroll = QtWidgets.QScrollArea()
        self.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll.setWidgetResizable(True)
        widget = QtWidgets.QWidget()
        widget.setLayout(bglayout)
        self.scroll.setWidget(widget)

        okBtn = QtWidgets.QPushButton("OK")
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(label)
        layout.addWidget(self.scroll)
        layout.addWidget(okBtn)
        self.setLayout(layout)
        self.setWindowTitle("Select transformation")
        # Signals
        self.bg.buttonClicked.connect(self.select)
        okBtn.clicked.connect(self.ok)

    def ok(self):
        if self.selected is not None:
            self.accept()

    def select(self, btn):
        self.selected = self.bg.checkedId()

#==============================================================================
class Transformers:
    """ A class to save transformer groups for converting from one widget
        to another
    """
    def __init__(self):
        self._groups = {}

    def setTransformerGroup(self, widget1, widget2):
        """ Add a transformer group for converting from
            `widget1` to `widget2`.

            Args:
                p0 (:class:`FileCoorWidget.CoorWidget`): `from` widget
                p1 (:class:`FileCoorWidget.CoorWidget`): `to` widget
        """
        #print("setTransformerGroup-------")
        crs_from = CRS.from_epsg(widget1.epsg)
        crs_to = CRS.from_epsg(widget2.epsg)
        ax1, ax2, *_ = crs_from.axis_info
        if ax1.abbrev.lower() == 'lat':
            invert_from = True
        else:
            invert_from = False
        ax1, ax2, *_ = crs_to.axis_info
        if ax1.abbrev.lower() == 'lat':
            invert_to = True
        else:
            invert_to = False
        group = TransformerGroup(crs_from, crs_to)
        ntransformers = len(group.transformers)
        if ntransformers > 1:
            dlg = TransformerDialog(group.transformers)
            dlg.resize(340, 400)
            if dlg.exec_():
                index = dlg.selected
        else:
            index = 0
        self._groups[(widget1, widget2)] = {
                'group': group,
                'selected': index,
                'invert_from': invert_from,
                'invert_to': invert_to,
                'fromEPSG': widget1.epsg,
                'toEPSG': widget2.epsg,
                }
        #print(("Transformers: Adding a TransformerGroup from"
                #" %s to %s index %d" %
                #(widget1.epsg, widget2.epsg, index)))

    def invert_from(self, widget1, widget2):
        return self._groups[(widget1, widget2)]['invert_from']

    def invert_to(self, widget1, widget2):
        return self._groups[(widget1, widget2)]['invert_to']

    def transformer(self, widget1, widget2):
        try:
            group = self._groups[(widget1, widget2)]
        except KeyError:
            self.setTransformerGroup(widget1, widget2)
        selected = self._groups[(widget1, widget2)]['selected']
        transformer = self._groups[
                (widget1, widget2)]['group'].transformers[selected]
        #print("transformer.definition", transformer.definition)
        # Update description with pipeline definition
        widget2.setDescription(transformer.definition)
        widget2.showCRS()
        return transformer

#==============================================================================
class TinyButton(QtWidgets.QPushButton):
    """ A QPushButton with a size fitted to the button label
    """

    def __init__(self, label, parent):
        """ Create a new TinyButton instance.

            Args:
                label (str): label to put on the tiny button
                parent (widget): parent widget
        """
        super().__init__(label, parent)

        textWidth = self.fontMetrics().boundingRect(self.text()).width()
        self.setMaximumWidth(textWidth + 12)
        self.setFocusPolicy(Qt.NoFocus)

#==============================================================================
class CRSFileCoorWidget(FileCoorWidget):
    """ A FileCoorWidget with a label to show the current CRS
    """
    @inherit_docstrings
    def __init__(self, *args, **kargs):
        super().__init__(*args, **kargs)
        self.showCRS(False)

    def showCRS(self, visible=True):
        self.crsLabel.setVisible(visible)
        self.crsBtn.setVisible(visible)

    def _makeCRS(self, inputLayout):
        # CRS button
        self.crsBtn = TinyButton(u"\N{Downwards Double Arrow}", self)
        self.crsBtn.clicked.connect(self._selectCRS)
        # Button is transparent
        self.crsBtn.setAutoFillBackground(False)
        #self.crsBtn.setStyleSheet(
                #"background-color: rgba(255,   0,   0, 1);")
        self.crsBtn.setSizePolicy(QtWidgets.QSizePolicy.Minimum,
                QtWidgets.QSizePolicy.Expanding)
        self.crsBtn.setToolTip(QCA.translate("datum tooltip",
                "Change Coordinate Reference System"))

        # a TextEdit to display the CRS
        #self.crsLabel = QTextEdit()
        #self.crsLabel.setReadOnly(True)
        self.crsLabel = QtWidgets.QLabel()
        self.crsLabel.setWordWrap(True)
        fm = self.crsLabel.fontMetrics()
        rowHeight = fm.lineSpacing()
        self.crsLabel.setMaximumHeight(3 * rowHeight)
        self.crsLabel.setMinimumHeight(3 * rowHeight)
        self.crsLabel.setStyleSheet("Text-align:left")
        #self.crsLabel.setStyleSheet(
                #"background-color: rgba(255, 255, 255, 160);")
        self.crsLabel.setTextInteractionFlags(Qt.TextBrowserInteraction)
        self.crsLabel.setToolTip(QCA.translate(
                "crsTooltip",
                "This is the PRØJ pipeline to transform TO this CRS")
                )

        # Make a group box frame with a title
        self.coorGroup = QtWidgets.QGroupBox(
                QCA.translate("datumGroup", "Coordinate Reference System:"))
        self.coorGroup.setFlat(True)
        f = self.coorGroup.font()
        f.setBold(True)
        self.coorGroup.setFont(f)

        crsLayout = QtWidgets.QHBoxLayout()
        #crsLayout.addWidget(self.crsLabel, alignment=Qt.AlignTop, stretch=1)
        crsLayout.addWidget(self.crsLabel, alignment=Qt.AlignTop)
        #crsLayout.addWidget(self.crsBtn, alignment=Qt.AlignLeft|Qt.AlignVCenter
        #crsLayout.addWidget(self.crsBtn, alignment=Qt.AlignLeft | Qt.AlignTop)
        crsLayout.addWidget(self.crsBtn, alignment=Qt.AlignTop)

        # Build widget layout:
        v = QtWidgets.QVBoxLayout()
        v.addLayout(crsLayout)
        v.addLayout(inputLayout)
        self.coorGroup.setLayout(v)
#==============================================================================
class CoorDialog(QtWidgets.QDialog):
    """ A coordinates dialog class to represent a 3D point in two different
        datums and projections.

        The dialog support also conversion of files.

        Author:
            R. Vidmar, 20111227
    """
    def __init__(self, xyz,
                 epsgFrom=FileCoorWidget.WGS84,
                 epsgTo=FileCoorWidget.WGS84,
                 ell2Ortho=None,
                 name='',
                 upperMode=FileCoorWidget.ModeFile,
                 lowerMode=FileCoorWidget.ModeFile,
                 dmsChars=[u"°", u"'", u'"'],
                 parent=None):
        """ Create a new CoorDialog instance.

            Args:
                xyz (tuple): Tuple (x, y, z) where: x = Easting or longitude,
                    y = Northing or latitude, z = Ellipsoidal height
                epsgFrom (int): input EPSG code
                epsgTo (int): output EPSG code
                ell2Ortho (float): offset to add to z to obtain orthometric
                    height
                name (str): optional name of this point
                upperMode (int): mode at init: (0, ... FileCoorWidget.ModeFile) for
                    the upper coorWidget
                lowerrMode (int): mode at init: (0, ... FileCoorWidget.ModeFile) for
                    the lower coorWidget
                dmsChars (tuple): Separators for degrees, minutes, seconds
                    ([u"°", u"'", u'"'])
                parent (QWidget): parent widget
        """
        super().__init__(parent)
        self._transformers = Transformers()
        self.setWindowTitle(QCA.translate("coordDialog",
                "PRØJ Coordinate Converter - Ver. %s") % __version__)
        if name:
            self.setObjectName(name)
            pointLabel = QtWidgets.QLabel(name)
            f = pointLabel.font()
            f.setBold(True)
            pointLabel.setFont(f)
            pointLabel.setToolTip(
                    QCA.translate("coordDialog", "The name of this point"))
        else:
            pointLabel = None
        self.inC = self.outC = None
        self._lastInputFilePath = None
        self._dmsChars = dmsChars
        self._logo1 = QtGui.QImage(":/BoboAndNick.png")
        self._logo2 = QtGui.QImage(":/logo.png")
        self._logo = self._logo2

        if (lowerMode == FileCoorWidget.ModeFile
                or upperMode == FileCoorWidget.ModeFile):
            self._fileMode = True
        else:
            self._fileMode = False
        cWidget1 = CRSFileCoorWidget(
                xyz,
                epsg=epsgFrom,
                ell2Ortho=ell2Ortho,
                name='Upper',
                comboU='File',
                initMode=upperMode)
        cWidget1.setDMSCombo(FileCoorWidget.COMBO_DMS_WITH_FILE)

        cWidget2 = CRSFileCoorWidget(
                epsg=epsgTo,
                name='Lower',
                comboU='File',
                initMode=lowerMode)
        cWidget1.setDMSCombo(FileCoorWidget.COMBO_DMS_WITH_FILE)

        # Connect Signals
        cWidget1.changedSignal.connect(self.changed)
        cWidget2.changedSignal.connect(self.changed)

        x, y, z = self.__transform(cWidget1, cWidget2, cWidget1.xyz())

        cWidget2.setValue(x, y, z)
        cWidget2.setEll2Ortho(cWidget1.ell2Ortho())
        self.cWidgets = [cWidget1, cWidget2]

        self.convertBtn = self._makeConvertButton()

        layout = QtWidgets.QVBoxLayout()
        if pointLabel:
            layout.addWidget(pointLabel, alignment=Qt.AlignJustify)
        layout.addWidget(cWidget1)
        layout.addWidget(self.convertBtn, alignment=Qt.AlignCenter)
        layout.addWidget(cWidget2)
        layout.addStretch(0)
        self.setLayout(layout)

        # Hidden key ;-)
        #QShortcut(QtGui.QKeySequence("Ctrl+H"), self, self._hiddenKey)
        QShortcut(QtGui.QKeySequence("Ctrl+H"), self).activated.connect(
                self._hiddenKey)

    def _makeConvertButton(self):
        # The "CONVERT FILE" button
        btn = QtWidgets.QPushButton(
                QCA.translate("Convert Button", "Convert File"))
        btn.setToolTip(
                QCA.translate("Convert Button",
                              "Click here to convert input File"))
        bsize = self._logo2.size()
        bsize.scale(100, 90, Qt.KeepAspectRatio)
        btn.setFixedWidth(bsize.width())
        btn.setFixedHeight(bsize.height())
        padx = 5
        pady = 10
        region = QtGui.QRegion(
                QtCore.QRect(btn.x() + padx,
                      btn.y() + pady,
                      btn.width() - 2 * padx,
                      btn.height() - 2 * pady), QtGui.QRegion.Ellipse)
        btn.setMask(region)
        btn.setStyleSheet("""
      QtWidgets.QPushButton {
        border: 2px solid #8f8f91;
        border-radius: 10px;
        color: white;
        font-weight: bold;
        background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 1,
                                       stop: 0 #D1D0E2, stop: 1 #383481);
        min-width: 100px;
      }
      QtWidgets.QPushButton:pressed {
        background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 1,
                                       stop: 0 #383481, stop: 1 #D1D0E2);
      }

      QtWidgets.QPushButton:flat {
        border: none; /* no border for a flat push button */
      }

      QtWidgets.QPushButton:default {
       border-color: navy;
      }""")
        btn.clicked.connect(self._onConvertBtn)
        btn.setHidden(True)
        return btn

    def resizeEvent(self, evt):
        self.backGround = QtGui.QImage(evt.size(),
                QtGui.QImage.Format_ARGB32_Premultiplied)
        p = QtGui.QPainter(self.backGround)
        p.fillRect(self.backGround.rect(), QtGui.QColor(222, 222, 222))
        scaled = self._logo.scaled(evt.size(), Qt.KeepAspectRatio)
        scaledRect = scaled.rect()
        scaledRect.moveCenter(self.backGround.rect().center())
        p.drawImage(scaledRect, scaled)

        palette = self.palette()
        brush = QtGui.QBrush(self.backGround)
        palette.setBrush(self.backgroundRole(), brush)
        self.setPalette(palette)
        super().resizeEvent(evt)

    def _hiddenKey(self):
        """ Some credit to the authors...
        """
        if self._logo == self._logo1:
            self._logo = self._logo2
        else:
            self._logo = self._logo1
        size = self.size()
        self.resizeEvent(QtGui.QResizeEvent(size, size))

    def _onConvertBtn(self):
        w1 = self.cWidgets[0]
        w2 = self.cWidgets[1]

        if self.cWidgets[0].filePath() and self.cWidgets[1].filePath():
            # Both input and output files defined and valid; convert?
            for c in self.cWidgets:
                if c.filePickerMode() == FilePicker.OutputMode:
                    # This is output
                    outC = c
                else:
                    inC = c
            msgBox = QtWidgets.QMessageBox(
                    QtWidgets.QMessageBox.Question,
                    QCA.translate("Ok to convert", 'Convert File'),
                    QCA.translate("Ok to convert",
                            "Ok to convert\n'%s' to\n'%s'\nwith format '%s' ?")
                    % (inC.filePath(), outC.filePath(), outC.coorFormat()),
                    QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
            if msgBox.exec_() == QtWidgets.QMessageBox.Ok:
                self.inC = inC
                self.outC = outC
                if inC.filePath() != self._lastInputFilePath:
                    self.selectFields(inC.filePath(), inC.isLatLon(),
                                      inC.ell2Ortho())
                    self._lastInputFilePath = inC.filePath()
                else:
                    self._convertFile(self._lastSelection)
            else:
                pass

    def selectFields(self, pn, longLat, ortho=None):
        """ Select fields for file pn

            Args:
                pn (str): pathname of the file from which select the fields
                longLat (bool): True (geographic coordinates) or
                    False (easting, northing)
                ortho (float or None): Orthometric height or None
        """
        self.dlg = FieldsWizard(pathName=pn, longLat=longLat, ortho=ortho)
        self.dlg.selectionDoneSignal.connect(self._convertFile)
        self.dlg.exec_()

    def _convertFile(self, d):
        """ Format has been selected, close dialog and convert file
        """
        self._lastSelection = d
        self.dlg.close()
        tic = time.time()

        # Read ASCII file
        names = d['names']
        data = readFromFile(
                d['pathName'],
                names,
                d['formats'],
                headerlines=d['headerLines'],
                removeEmptyLines=True,
                commentChars='#')

        try:
            x, y, z, isOrtho = self.dlg.data2Degrees(data, names)
        except IndexError:
            # Issue an error message
            mbox = QtWidgets.QMessageBox()
            mbox.setWindowTitle(QCA.translate("Error message",
                    'ERROR reading input file.....'))
            mbox.setIcon(QtWidgets.QMessageBox.Critical)
            mbox.setInformativeText(QCA.translate(
                    "Error message",
                    "ERROR!\nCannot understand input file:\n"
                    "Maybe format or projection is wrong?"))
            mbox.setStandardButtons(QtWidgets.QMessageBox.Ok)
            mbox.setDefaultButton(QtWidgets.QMessageBox.Ok)
            mbox.button(QtWidgets.QMessageBox.Ok).setText(
                    QCA.translate("Error message", 'Sorry'))
            ret = mbox.exec_()
            return

        x1, y1, z1, ell2Ortho = self._transform(self.inC, self.outC, (x, y, z))

        if self.outC.heightWidget.rep():
            # Orthometric selected
            z1 += ell2Ortho
            hType = QCA.translate("HeightWidget", "Heights are orthometric")
        else:
            hType = QCA.translate("HeightWidget", "Heights are ellipsoidal")

        xyzDegrees = (x1, y1, z1)

        fmt = ''
        xyz = []
        xdecimals, ydecimals, zdecimals = self.outC.decimals()
        for i, decimals in enumerate((xdecimals, ydecimals)):
            if isinstance(decimals, tuple):
                if i == 0:
                    # Number of digits for longitude
                    degdigits = 3
                else:
                    # Number of digits for latitude
                    degdigits = 2

                if len(decimals) == 1:
                    # Degrees
                    fmt += '%%.%df' % decimals[0]
                    xyz.append(xyzDegrees[i])
                elif len(decimals) == 2:
                    # Degrees, Minutes.decimals
                    fmt += '%%0%d.0f%s%%0%d.%df%s' % (
                            degdigits, self._dmsChars[0], 3 + xdecimals[1],
                            xdecimals[1], self._dmsChars[1])
                    dd, mm = d2dm(xyzDegrees[i], xdecimals[1])
                    xyz.append(dd)
                    xyz.append(mm)
                elif len(decimals) == 3:
                    # Degrees, Minutes, Seconds.decimals
                    fmt += '%%0%d.0f%s%%02.0f%s%%0%d.%df%s' % (
                            degdigits, self._dmsChars[0], self._dmsChars[1],
                            3 + xdecimals[2], xdecimals[2], self._dmsChars[2])
                    dd, mm, ss = d2dms(xyzDegrees[i], xdecimals[2])
                    xyz.append(dd)
                    xyz.append(mm)
                    xyz.append(ss)
            else:
                fmt += ' %%.%df' % xdecimals
                xyz.append(xyzDegrees[i])
            fmt += ' '

        fmt += ' %%.%df\n' % zdecimals
        xyz.append(xyzDegrees[2])

        xyz = np.vstack(xyz)

        # Create a header
        header = QCA.translate("outfile",
                "# Converted on %s:\n") % (time.asctime())
        header += QCA.translate("outfile", "# From EPSG: '%s' - %s\n") % (
                self.inC.epsg, hType)
        header += QCA.translate("outfile", "#   To EPSG: '%s' - %s\n") % (
                self.outC.epsg, hType)

        #writeASCII(self.outC.filePath(), xyz, fmt, header)
        tf = ThreadedFunc(writeASCII, self.outC.filePath(), xyz, fmt, header)
        pd = ProgressDialog(tf,
                title=QCA.translate("ProgressDialog",
                "Conversion in progress..."),
                msg=QCA.translate("ProgressDialog", "Converting..."))

        # Issue a "Done" message
        mbox = QtWidgets.QMessageBox()
        mbox.setWindowTitle(QCA.translate("Done message", 'File Conversion'))
        mbox.setIcon(QtWidgets.QMessageBox.Information)
        mbox.setInformativeText(QCA.translate(
                "Done message", "File has been convertd to %s\n"
                "Output format is '%s'.") % (self.outC.filePath(),
                self.outC.coorFormat()))
        mbox.setStandardButtons(QtWidgets.QMessageBox.Ok)
        mbox.setDefaultButton(QtWidgets.QMessageBox.Ok)
        mbox.button(QtWidgets.QMessageBox.Ok).setText(
                QCA.translate("Done message", 'Ok'))
        ret = mbox.exec_()

    def fileMode(self):
        """ Return True if widget is in `File` mode

            Returns:
                bool: widget file mode
        """
        return self._fileMode

    def changed(self, signal):
        """ Something changed: either coordinates or CRS:

            Transform coordinates of the "other" point accordingly

            Args:
                signal (QWidget): argument of the changedSignal emitted by
                        the FileCoorWidget
        """
        changed = self.sender()
        cWidgets = self.cWidgets[:]
        cWidgets.remove(changed)
        unchanged = cWidgets[0]

        #print("changed signal '%s'" % signal)
        if signal == FileCoorWidget.POINT_CHANGED:
            x1, y1, z1, ell2Ortho = self._transform(changed, unchanged,
                                                    changed)
            # Update widget's value
            unchanged.setValue(x1, y1, z1)
            # No need to change ell2Ortho if we don't change Datum
        elif signal == FileCoorWidget.CRS_CHANGED:
            #print("FileCoorWidget.CRS_CHANGED trasforming %s point" %
                    #unchanged.objectName())
            self._transformers.setTransformerGroup(unchanged, changed)
            x1, y1, z1, ell2Ortho = self._transform(unchanged, changed,
                                                    unchanged)
            # Update widget's value
            changed.setValue(x1, y1, z1)
            changed.setEll2Ortho(ell2Ortho)
            unchanged.setEll2Ortho(ell2Ortho)
            # Clear lastInputFilePath to force input fields selection
            self._lastInputFilePath = None
        elif signal == FileCoorWidget.FILE_ON:
            # Enable File Conversion Mode
            if changed.filePickerMode() == unchanged.filePickerMode():
                #########################
                # THIS HAPPENS ONLY ONCE!
                #########################
                # Both are input: Set to output the unchanged:
                unchanged.setFilePickerMode(FilePicker.OutputMode)
                # Disable "File" selection in unchanged
                unchanged.enableFileMode(False)

            # Show both file pickers
            changed.showFilePicker(True)
            unchanged.showFilePicker(True)
            self._fileMode = True
            self.convertBtn.setHidden(False)
            self.adjustSize()
        elif signal == FileCoorWidget.FILE_OFF:
            if changed.filePickerMode() == FilePicker.InputMode:
                # Hide both file pickers
                changed.showFilePicker(False)
                unchanged.showFilePicker(False)
                self._fileMode = False
                self.convertBtn.setHidden(True)
                self.adjustSize()
        else:
            raise NotImplementedError("Unimplemented signal '%s'." %
                    signal)

    def __transform(self, p0, p1, p):
        """
            Args:
                p0 (:class:`FileCoorWidget.FileCoorWidget`): input widget
                p1 (:class:`FileCoorWidget.FileCoorWidget`): output widget
                p (tuple or :class:`numpy.ndarray`): point(s) to transform

            Returns:
                tuple or :class:`numpy.ndarray`: transformed point(s)
        """
        transformer = self._transformers.transformer(p0, p1)
        xi, yi, zi = p
        if self._transformers.invert_from(p0, p1):
            xi, yi = yi, xi
        x, y, z = transformer.transform(xi, yi, zi)
        if self._transformers.invert_to(p0, p1):
            x, y = y, x
        return x, y, z

    def _transform(self, p0, p1, p):
        """ Transform point(s) in widget p from crs in widget p0 to
            crs in widget p1.

            Args:
                p0 (:class:`FileCoorWidget.FileCoorWidget`): input widget
                p1 (:class:`FileCoorWidget.FileCoorWidget`): output widget
                p (:class:`FileCoorWidget.FileCoorWidget`): widget to be
                    converted

            Returns:
                tuple or :class:`numpy.ndarray`: transformed point(s)
        """
        if isinstance(p, FileCoorWidget):
            x, y, z = p.xyz()
        else:
            x, y, z = p
        pointToConvert = x, y, z
        fromProjection = p0.crs()

        if p0.datumShift() or p1.datumShift() or p0.geoidGrid(
        ) or p1.geoidGrid():
            # Transform p to p_WGS84
            p84 = Proj(FileCoorWidget.WGS84)
            x84, y84, z84 = transform(p0.crs(), p84, x, y, z)

        if p1.datumShift() and not p0.datumShift():
            # ONLY p1
            # Get correction for p_WGS84
            lonc, latc = p1.datumShift().getLLCorrection((x84, y84))
            if lonc is None:
                noDatumShift((x84, y84))
                xyzT = (0, 0, 0)
            else:
                if p1.isLatLon():
                    unimplemented()
                    xyzT = transform(p84, p1.crs(), x - lonc,
                                     y - latc) + (z, )
                else:
                    xyzT = p1.crs()(x84 + lonc, y84 + latc) + (z, )
        elif p0.datumShift() and not p1.datumShift():
            # ONLY p0
            # Get correction for p_WGS84
            lonc, latc = p0.datumShift().getLLCorrection((x84, y84))
            if lonc is None:
                noDatumShift((x84, y84))
                xyzT = (0, 0, 0)
            else:
                if p0.isLatLon():
                    unimplemented()
                    xyzT = transform(p84, p1.crs(), x - lonc,
                                     y - latc) + (z, )
                else:
                    tx, ty = p0.crs()(x, y, inverse=True)
                    xyzT = transform(p84, p1.crs(), tx - lonc,
                                     ty - latc) + (z, )
        elif p0.datumShift() and p1.datumShift():
            # BOTH!
            # Get correction for p_WGS84
            lon0c, lat0c = p0.datumShift().getLLCorrection((x84, y84))
            lon1c, lat1c = p1.datumShift().getLLCorrection((x84, y84))
            if lon0c is None or lon1c is None:
                noDatumShift((x84, y84))
                xyzT = (0, 0, 0)
            else:
                if p0.isLatLon():
                    unimplemented()
                    xyzT = (0, 0, 0)
                else:
                    tx, ty = p0.crs()(x, y, inverse=True)
                    xyzT = p1.crs()(tx - lon0c + lon1c,
                                     ty - lat0c + lat1c) + (z, )
        else:
            # No datumShift
            xyzT = self.__transform(p0, p1, (x, y, z))

        # Geoid Correction
        ell2Ortho = None
        if p0.geoidGrid():
            # Get Geoid Correction
            correction = p0.geoidGrid().getCorrection((x84, y84))
            if correction is not None:
                ell2Ortho = -correction

        if p1.geoidGrid():
            # Get Geoid Correction
            correction = p1.geoidGrid().getCorrection((x84, y84))
            if correction is not None:
                if ell2Ortho is None:
                    ell2Ortho = correction
                else:
                    ell2Ortho += correction

        return xyzT + (ell2Ortho, )


#==============================================================================
if __name__ == '__main__':
    import os
    import argparse
    from signal import signal, SIGINT, SIG_DFL

    signal(SIGINT, SIG_DFL)
    app = QtWidgets.QApplication(sys.argv)

    # i18n for standard widgets
    qTranslator = QtCore.QTranslator()
    ok = qTranslator.load("qt_" + QtCore.QLocale.system().name(),
            QtCore.QLibraryInfo.location(QtCore.QLibraryInfo.TranslationsPath))
    if not ok:
        # Workaround for italian
        ok = qTranslator.load("qt_" + QtCore.QLocale.system().name())
    app.installTranslator(qTranslator)

    # i18n for our application
    translator = QtCore.QTranslator()
    ok = translator.load("i18n_" + QtCore.QLocale.system().name())
    app.installTranslator(translator)

    parser = argparse.ArgumentParser(prog=os.path.basename(sys.argv[0]))
    parser.add_argument(
            '-p',
            '--point',
            #default="(403328.7930, 5055599.370, 59.579)",
            #default="(400000.0000, 5000000.000, 59.579)",
            default=str(moloSartorioETRF2000),
            help=QCA.translate('main', 'point coordinates '
                               "(default: '%(default)s')"))
    parser.add_argument(
            '-u',
            '--upperEPSG',
            #default=32633,
            default=FileCoorWidget.WGS84,
            #default=4326,
            #default=4979,
            help=QCA.translate(
                    'main', 'upper EPSG '
                    "(default: '%(default)s')"))
    parser.add_argument(
            '-l',
            '--lowerEPSG',
            #default=FileCoorWidget.WGS84,
            #default=4322, # WGS 72
            default=32633, # WGS 84 / UTM 33
            #default=3004,
            #default=4985, # WGS 72
            help=QCA.translate(
                    'main', 'lower EPSG '
                    "(default: '%(default)s')"))
    parser.add_argument(
            '-e',
            '--ell2Ortho',
            default='None',
            help=QCA.translate(
                    'main', 'optional height to add to get orthometric height '
                    "(default: '%(default)s')"))
    parser.add_argument(
            '-n',
            '--name',
            default='',
            help=QCA.translate(
                    'main', 'optional name for the dialog '
                    "(default: '%(default)s')"))
    parser.add_argument(
            '-U',
            '--upperMode',
            default=str(FileCoorWidget.ModeD),
            help=QCA.translate(
                    'main', 'optional upper widget representation [0..2] '
                    "(default: '%(default)s')"))
    parser.add_argument(
            '-L',
            '--lowerMode',
            default=str(FileCoorWidget.ModeD),
            help=QCA.translate(
                    'main', 'optional lower widget representation [0..2] '
                    "(default: '%(default)s')"))
    parser.add_argument(
            '-d',
            '--dmsChars',
            default=u"""["°", "'", '"']""",
            help=QCA.translate(
                    'main', 'degrees, minutes, seconds characters '
                    "(default: '%(default)s')"))
    o = parser.parse_args(sys.argv[1:])

    #dialog = CoorDialog(*moloSartorioUTM33)
    #dialog = CoorDialog(*moloSartorioWGS84)
    if o.point == "(403328.7930, 5055599.370, 59.579)":
        o.name = "Molo Sartorio"
    dialog = CoorDialog(
            eval(o.point), o.upperEPSG, o.lowerEPSG, eval(o.ell2Ortho), o.name,
            eval(o.upperMode), eval(o.lowerMode), eval(o.dmsChars))
    dialog.show()
    app.exec_()
