""" A widget for (phi, lamda, h) or (easting, northing, h) coordinates

    The FileCoorWidget class implements a widget to display coordinates:
    (lat, lon, height) or (easting, northing height).
    The widget has optional combo boxes both for degrees representation
    (D.DD or D, M.MM or D, M, S.SS) and height (ellipsoidal or orthometric).
    The appearance of the widget (phi, lamda, h) or (easting, northing, h)
    depends on the CRS used.

    Author:
        20111221-20120214 Roberto Vidmar

    Updated:
        20201028 - R. Vidmar

    License:
        MIT/X11 License (see :download:`license.txt <../../../license.txt>`)
"""
from . import QtCore, Qt, QtWidgets, Signal

globals()['QCoreApplication'] = getattr(QtCore, 'QCoreApplication')
for cls in ("QWidget QHBoxLayout QApplication QLabel QStackedWidget".split()):
    globals()[cls] = getattr(QtWidgets, cls)

from ogsutils.geodesy import DatumShift, Geoid

# Local imports
from .degreesWidget import DegreesWidget
from .coorWidget import CoorWidget
from .filePicker import FilePicker
from .crsDialog import CRSDialog

QCA = QCoreApplication

#==============================================================================
class FileCoorWidget(CoorWidget):
    """ A CoorWidget with file handling capabilities.
    """
    fileChangedSignal = Signal(object)

    # Initial visualization Mode
    ModeFile = 3
    COMBO_DMS_WITH_FILE = 2

    POINTINDEX = 0
    FILEINDEX = 1
    INPUT_LABEL = "Input File"
    OUTPUT_LABEL = "Output File"

    # Signals
    CRS_CHANGED = 'crs'
    FILE_ON = 'File on'
    FILE_OFF = 'File off'
    def __init__(self,
            xyz=(0, 0, 0),
            initMode=CoorWidget.ModeD,
            repH=CoorWidget.RepEll,
            ell2Ortho=None,
            name='',
            epsg=CoorWidget.WGS84,
            comboU=CoorWidget.COMBO_DMS_OFF,
            comboH=CoorWidget.COMBO_HEIGHT_OFF):
        """ Create a new instance of the widget.

            Args:
                xyz (tuple):
                    - x = Easting or longitude,
                    - y = Northing or latitude,
                    - z = Ellipsoidal height
                initMode (int): Initial representation:
                    - ModeD = D.DD or M.MM
                    - ModeDM = D M.MM
                    - ModeDMS = D M S.SS
                    - ModeFile = File
                repH (int): representation for height:
                    - RepEll = ellipsoidal
                    - RepOrtho = orthometric
                ell2Ortho (float): offset to add to z to obtain
                    orthometric height
                name (str): optional name of this object
                epsg (int or str): EPSG number for Coordinate Reference
                    System
                comboU (int): Add a combo box to select degrees
                    representation and File selection Default: COMBO_DMS_OFF
                    Values: (COMBO_DMS_OFF, COMBO_DMS_ON, COMBO_DMS_WITH_FILE)
                comboH (int): Add a combo box to select height representation
        """
        if initMode == self.ModeFile:
            self._rep = DegreesWidget.DDD
        else:
            self._rep = initMode

        super().__init__(xyz=xyz, initMode=initMode, repH=repH,
            ell2Ortho=ell2Ortho, name=name, epsg=epsg,
            comboU=comboU, comboH=comboH)

        if initMode == self.ModeFile:
            # Show the File Picker
            self.showFilePicker(True)
        else:
            self.enableFileMode(False)

    def _makePointWidget(self, layout, repH):
        # The pointOrFileWidget
        self.pointOrFileWidget = QStackedWidget()
        self.pointOrFileWidget.addWidget(self._makeXYZWidget(repH))
        self._makeFilePicker(self.pointOrFileWidget)
        layout.addWidget(self.pointOrFileWidget)

    def _makeFilePicker(self, stackedWidget):
        # File Picker
        self._filePicker = FilePicker(self,
                filt=QCA.translate("FilePicker",
                "XYZ files (*.xyz);;All Files (*)"))

        self._filePickerLabels = [self.INPUT_LABEL, self.OUTPUT_LABEL]

        # Connect signals
        self._filePicker.pathChanged.connect(self.fpChanged)

        # The file picker: Build a Label + FilePicker widget
        hlayout = QHBoxLayout()
        hlayout.setContentsMargins(0, 0, 0, 0)
        # The label
        hlayout.addWidget(QLabel(), alignment=Qt.AlignLeft | Qt.AlignVCenter)
        hlayout.addWidget(self._filePicker)
        self.pickerWithLabel = QWidget()
        self.pickerWithLabel.setLayout(hlayout)
        stackedWidget.addWidget(self.pickerWithLabel)
        # Default is InputMode
        self.setFilePickerMode(FilePicker.InputMode)

    def setDMSCombo(self, value):
        if not value:
            self.formatStackedWidget.hide()
            return

        self.formatStackedWidget.show()
        if value == FileCoorWidget.COMBO_DMS_WITH_FILE:
            self.enableFileMode(True)
        elif value == FileCoorWidget.COMBO_DMS_ON:
            self.enableFileMode(False)

    def filePath(self):
        """ Return the File Picker text content

            Returns:
                (str): File Picker text content
        """
        layout, label, filePicker = self.pickerWithLabel.children()
        return filePicker.text()

    def filePickerMode(self):
        """ Return the File Picker mode

            Returns:
                int: File Picker mode
        """
        layout, label, filePicker = self.pickerWithLabel.children()
        return self._filePickerLabels.index(label.text())

    def enableFileMode(self, enable):
        """ Enable / disable `self.ModeFile` in combo box

            Args:
                enable (bool): enable / disable file mode
        """
        # Disconnect signals
        self.planeCombo.currentIndexChanged.disconnect(self._indexChanged)
        self.geographicCombo.currentIndexChanged.disconnect(self._indexChanged)

        # Clear combos
        self.planeCombo.clear()
        self.geographicCombo.clear()
        planeItems = (QCA.translate("plane", "M.MM"), )
        planeToolTip = QCA.translate("plane", "Easting, Northing in meters")
        geographicItems = (
                QCA.translate("degs", "D.DD"),
                QCA.translate("degs", "D M.MM"),
                QCA.translate("degs", "D M S.SS"))
        geographicToolTip = (QCA.translate(
                "degs", "Change angular representation among\n"
                "degrees.decimals,\ndegrees, minutes.decimals\nand degrees, "
                "minutes, seconds.decimals"))

        if enable:
            extraItem = (QCA.translate("file", "File"), )
            extraToolTip = QCA.translate(
                    "file", "\nOr select File for file conversion")
            planeItems += extraItem
            planeToolTip += extraToolTip
            geographicItems += extraItem
            geographicToolTip += extraToolTip

        self.planeCombo.addItems(planeItems)
        self.planeCombo.setToolTip(planeToolTip)
        self.geographicCombo.addItems(geographicItems)
        self.geographicCombo.setToolTip(geographicToolTip)

        # Reset current index
        self.planeCombo.setCurrentIndex(self.ModeD)
        self.geographicCombo.setCurrentIndex(self._rep)

        # Reconnect signals
        self.planeCombo.currentIndexChanged.connect(self._indexChanged)
        self.geographicCombo.currentIndexChanged.connect(self._indexChanged)

    def setFilePickerMode(self, mode):
        """ Set the File Picker mode

            Args:
                mode (int): set File Picker mode to `mode`
        """
        layout, label, filePicker = self.pickerWithLabel.children()
        label.setText(self._filePickerLabels[mode])
        label.setToolTip(self._filePickerLabels[mode])
        filePicker.setMode(mode)

    def fpChanged(self, newPath):
        """ Emit a `fileChangedSignal` Signal with `newPath` argument.

            Args:
                newPath (str): new file path
        """
        if newPath:
            self.fileChangedSignal.emit(newPath)

    def isFilePickerShown(self):
        """ Return True if the File Picker is shown

            Returns:
                bool: True if the File Picker is shown
        """
        if self.pointOrFileWidget.currentIndex == self.FILEINDEX:
            return True
        else:
            return False

    def showFilePicker(self, showIt=True):
        """ If `showIt` evaluates True set widget mode to show the File
            Picker, else show coordinates widget

            Args:
                showIt (bool): set widget mode to show the File Picker if True
        """
        if showIt:
            self.pointOrFileWidget.setCurrentIndex(self.FILEINDEX)
        else:
            self.pointOrFileWidget.setCurrentIndex(self.POINTINDEX)

    def _indexChanged(self, index):
        """ Index changed in the combo boxes: set appropriate representation

            Args:
                index (int): new index in the ComboBox
        """
        if index < 0:
            return

        if (index == self.ModeFile
                or index == 1 and self.sender() == self.planeCombo):
            self.changedSignal.emit(self.FILE_ON)
            self.showFilePicker(True)
            return

        self.changedSignal.emit(self.FILE_OFF)
        self.showFilePicker(False)
        self.setDRep(index)
        return


        #rep = None
        #if self.sender() == self.planeCombo:
            #if index == 1:
                #self.changedSignal.emit(self.FILE_ON)
                #self.showFilePicker(True)
            #else:
                #rep = index
                #self.changedSignal.emit(sef.FILE_OFF)
                #self.showFilePicker(False)
        #elif self.sender() == self.geographicCombo:
            #if index == self.ModeFile:
                #self.changedSignal.emit(self.FILE_ON)
                #self.showFilePicker(True)
            #else:
                #rep = index
                #self.changedSignal.emit(self.FILE_OFF)
                #self.showFilePicker(False)
        #else:
            #raise SystemExit(
                    #"FATAL ERROR..... _indexChanged: Invalid sender %s!" %
                    #self.sender().objectName())

        #if rep is not None:
            #self.setDRep(rep)

    def _selectCRS(self):
        """ Open a dialog to select Coordinate Reference System
        """
        dlg = CRSDialog(self.epsg, self)
        dlg.resize(640, 480)
        if dlg.exec_():
            epsg = dlg.epsg()
            if self.setCRS(epsg):
                self.changedSignal.emit(self.CRS_CHANGED)
                self.adjustSize()

    def coorFormat(self):
        """ Return format of coordinates as shown in the comboBox

            Returns:
                str: format of coordinates as shown in the comboBox

        """
        if self.isLatLon():
            combo = self.geographicCombo
        else:
            combo = self.planeCombo
        index = combo.currentIndex()
        allItems = [combo.itemText(i) for i in range(combo.count())]
        return allItems[index]

#==============================================================================
if __name__ == '__main__':
    import sys
    from signal import signal, SIGINT, SIG_DFL
    import argparse

    signal(SIGINT, SIG_DFL)
    parser = argparse.ArgumentParser("FileCoorWidget",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-f", "--format", type=int, default=0,
            choices=(0, 1, 2),
            help="Set representation 0=D, 1=DM, 2=DMS: ")
    parser.add_argument("--combo-dms", type=int, default=2,
            choices=(0, 1, 2),
            help="Disable/enable DMS combo 0=off, 1=on, 2=file ")
    parser.add_argument("--combo-h", type=int, default=1,
            choices=(0, 1),
            help="Disable/enable HEIGHT combo 0=off, 1=on ")
    parser.add_argument("--epsg", default=4979,
            help="Set Coordinate Reference System from EPSG: ")
    parser.add_argument("-e", "--ellH", default=123,
            help="Set height above ellipsoid: ")
    parser.add_argument("-d", "--diff", type=float, default=None,
            help="Set ell-ortho difference: ")
    opts = parser.parse_args()

    app = QApplication(sys.argv)
    # LatLong
    w = FileCoorWidget(epsg=opts.epsg, initMode=FileCoorWidget.ModeFile)
    w.setDMSCombo(opts.combo_dms)
    w.setHCombo(opts.combo_h)
    #w.setValue((13., 45.81882), (45.710551, ), 285.8)
    w.setEll2Ortho(opts.diff)
    w.show()
    sys.exit(app.exec_())
