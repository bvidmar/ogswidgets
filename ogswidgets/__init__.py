from colorsys import hsv_to_rgb
from .__version__ import __version__
from qtcompat import (Signal, QtCore, Qt, QtGui, QtWidgets, LIB,
        QAction, QShortcut)

#==============================================================================
class NiceColors(object):
    """ A NiceColors class
    """
    _colors = None
    golden_ratio_conjugate = 0.618033988749895

    def __init__(self):
        self.hue = self.golden_ratio_conjugate

    def __len__(self):
        """ Return number of colors alredy defined
        """
        return len(self._colors)

    def __contains__(self, color):
        """ Return True if color is already present
        """
        return color in self._colors

    def addRGBColor(self, rgb):
        """ Add color from rgb tuple
        """
        if self._colors is None:
            self._colors = [rgb]
        elif not rgb in self._colors:
            self._colors.append(rgb)

    def makeNewColor(self):
        """ Return a new nice random (r, g, b) color
        """

        def hue2rgb(h):
            r, g, b = hsv_to_rgb(h, 0.5, 0.95)
            return int(r * 256), int(g * 256), int(b * 256)

        while True:
            self.hue += self.golden_ratio_conjugate
            self.hue %= 1
            rgb = hue2rgb(self.hue)
            if self._colors is None:
                self._colors = [rgb]
                break
            elif not rgb in self._colors:
                self._colors.append(rgb)
                break
            else:
                msg = (_("Cannot create more than %d random colors!") %
                        len(self))
                raise NoMoreColorsException(msg)

        color = str(QtGui.QColor(*rgb).name())
        return color.strip('#')

    def colors(self):
        """ Return list of defined colors
        """
        return self._colors

