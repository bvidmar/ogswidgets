""" Widget delegates

    Author:
        - 20091202-20100321 Nicola Creati
        - 20111202-20120214 Roberto Vidmar
        - 20180606 Roberto Vidmar PyQt5 Python3

    License:
        MIT/X11 License (see :download:`license.txt <../../../license.txt>`)
"""
from . import QtCore, Qt, QtGui, QtWidgets
for cls in ("QAbstractProxyModel".split()):
    globals()[cls] = getattr(QtCore, cls)

for cls in ("QStyledItemDelegate ".split()):
    globals()[cls] = getattr(QtWidgets, cls)

from ogswidgets.filePicker import FilePicker, TinyButton, DirectoryPicker
from ogswidgets.lineEditors import(
        ValidLineEdit, DoubleValidator, IntegerEditor, FloatEditor,
        ComboEditor, FileEditor, DirEditor, DatumEditor, LineEditor,
        )

#==============================================================================
class AbstractDelegate(QStyledItemDelegate):
    def __init__(self, view=None, toolTip='', **kargs):
        self._editor = None
        self._view = view
        self.toolTip = toolTip
        self._kargs = kargs
        parent = kargs.pop('parent', None)
        super(AbstractDelegate, self).__init__(parent)

    def _onValueChange(self, dummy=None):
        self.setModelData(self._editor, self._view.model(),
                          self._view.currentIndex())

    def setEditorData(self, editor, index):
        value = index.model().data(index, Qt.DisplayRole)
        editor.setValue(value)

    def setModelData(self, editor, model, index):
        model.setData(index, editor.value())

    def createEditor(self, parent, option, index):
        if self._editor is None:
            raise NotImplementedError(
                    "createEditor method MUST be reimplemented")
        else:
            self._editor.setToolTip(self.toolTip)
            data = index.model().data(index, Qt.DisplayRole)
            self._editor.setHistory(data)
        return self._editor


#==============================================================================
class DatumPickerDelegate(AbstractDelegate):
    """ The DatumPicker Delegate Class
    """

    def __init__(self, *args, **kargs):
        super(DatumPickerDelegate, self).__init__(*args, **kargs)

    def createEditor(self, parent, option, index):
        self._kargs['parent'] = parent
        self._editor = DatumEditor(**self._kargs)
        self._editor.datumChanged.connect(self._onValueChange)
        return super(DatumPickerDelegate, self).createEditor(
                parent, option, index)


#==============================================================================
class IntegerDelegate(AbstractDelegate):
    """ The Integer Delegate Class
    """

    def __init__(self, *args, **kargs):
        super(IntegerDelegate, self).__init__(*args, **kargs)

    def createEditor(self, parent, option, index):
        self._kargs['parent'] = parent
        self._editor = IntegerEditor(**self._kargs)
        self._editor.editor().valueChanged.connect(self._onValueChange)
        return super(IntegerDelegate, self).createEditor(parent, option, index)


#==============================================================================
class TextDelegate(AbstractDelegate):
    """ The Text Delegate Class
    """

    def __init__(self, *args, **kargs):
        super(TextDelegate, self).__init__(*args, **kargs)

    def createEditor(self, parent, option, index):
        self._kargs['parent'] = parent
        self._editor = LineEditor(**self._kargs)
        self._editor.editor().editingFinished.connect(self._onValueChange)
        return super(TextDelegate, self).createEditor(parent, option, index)


#==============================================================================
class DoubleDelegate(AbstractDelegate):
    """ The Double (Floating Point) Delegate Class
    """

    def __init__(self, *args, **kargs):
        super(DoubleDelegate, self).__init__(*args, **kargs)

    def createEditor(self, parent, option, index):
        self._kargs['parent'] = parent
        self._editor = FloatEditor(**self._kargs)
        self._editor.editor().textChanged.connect(self._onValueChange)
        return super(DoubleDelegate, self).createEditor(parent, option, index)


#==============================================================================
class ComboDelegate(AbstractDelegate):
    """ The Combo Box Delegate Class
    """

    def __init__(self, *args, **kargs):
        self._items = kargs.get('items', ())
        super(ComboDelegate, self).__init__(*args, **kargs)

    def _valueToIndex(self, value):
        if value == '':
            idx = self._items.index(None)
        else:
            idx = self._items.index(value)
        return idx

    def createEditor(self, parent, option, index):
        self._kargs['parent'] = parent
        self._editor = ComboEditor(**self._kargs)
        self._editor.editor().currentIndexChanged.connect(self._onValueChange)
        super(ComboDelegate, self).createEditor(parent, option, index)

        self._editor.setHistory(
                self._valueToIndex(index.model().data(index, Qt.DisplayRole)))
        return self._editor

    def setEditorData(self, editor, index):
        value = index.model().data(index, Qt.DisplayRole)
        idx = self._valueToIndex(value)
        editor.setValue(idx)


#==============================================================================
class FilePickerDelegate(AbstractDelegate):
    """ The File Picker Delegate Class
    """

    def __init__(self, *args, **kargs):
        super(FilePickerDelegate, self).__init__(*args, **kargs)

    def createEditor(self, parent, option, index):
        self._kargs['parent'] = parent
        self._editor = FileEditor(**self._kargs)
        self._editor.editor().pathChanged.connect(self._onValueChange)
        return super(FilePickerDelegate, self).createEditor(
                parent, option, index)


#==============================================================================
class DirPickerDelegate(AbstractDelegate):
    """ The Directory Picker Delegate Class
    """

    def __init__(self, *args, **kargs):
        super(DirPickerDelegate, self).__init__(*args, **kargs)

    def createEditor(self, parent, option, index):
        self._kargs['parent'] = parent
        self._editor = DirEditor(**self._kargs)
        self._editor.editor().pathChanged.connect(self._onValueChange)
        return super(DirPickerDelegate, self).createEditor(
                parent, option, index)


#==============================================================================
class PropertyDelegate(QStyledItemDelegate):
    def __init__(self):
        super(PropertyDelegate, self).__init__()
        self.delegates = {}

    def getDelegate(self, index):
        """ Return delegate at index
        """
        model = index.model()
        if isinstance(model, QAbstractProxyModel):
            index = model.mapToSource(index)
        i = (index.row(), index.parent().internalPointer())
        return self.delegates.get((index.row(),
                                   index.parent().internalPointer()))

    def insertCellDelegate(self, row, parent, delegate):
        self.delegates[(row, parent)] = delegate

    def createEditor(self, parent, option, index):
        delegate = self.getDelegate(index)
        if delegate is not None:
            return delegate.createEditor(parent, option, index)
        else:
            super(PropertyDelegate, self).createEditor(parent, option, index)

    def setEditorData(self, editor, index):
        delegate = self.getDelegate(index)
        if delegate is not None:
            delegate.setEditorData(editor, index)
        else:
            super(PropertyDelegate, self).setEditorData(editor, index)

    def setModelData(self, editor, model, index):
        delegate = self.getDelegate(index)
        if delegate is not None:
            delegate.setModelData(editor, model, index)
        else:
            super(PropertyDelegate, self).setModelData(editor, model, index)

    def updateEditorGeometry(self, editor, option, index):
        editor.setGeometry(option.rect)
