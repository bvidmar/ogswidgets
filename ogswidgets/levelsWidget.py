""" A Levels editor Widget

    Author:
        - 20190222 Roberto Vidmar <rvidmar@inogs.it>
        - Nicola Creati <ncreati@inogs.it>

    License:
        MIT/X11 License (see :download:`license.txt <../../../license.txt>`)
"""
from . import Signal, QtCore, Qt, QtGui, QtWidgets, QAction

#==============================================================================
class LevelPoint(object):
    """ Color scale level point base class
    """
    def __init__(self, value, selected=False):
        # Scalar value
        self._value = value
        # Triangle object {QtGui.QPolygonF}
        self._triangle = None
        # Selection state
        self._selected = selected

    def isSelected(self):
        """ Return true if triangle is selected
        """
        return self._selected

    def triangle(self):
        """ Return triangle instance
        """
        return self._triangle

    def value(self):
        """ Return scalar value
        """
        return self._value

    def select(self):
        """ Select triangle level
        """
        self._selected = True

    def setValue(self, value):
        """ Set scalar value
        """
        self._value = value

    def setTriangle(self, triangle):
        """ Set triangle instance
        """
        self._triangle = triangle

    def unselect(self):
        """ Unselect triangle
        """
        self._selected = False

#==============================================================================
class LevelsWidget(QtWidgets.QFrame):

    levelsChanged = Signal(object)
    currentLevel = Signal(object)

    def __init__(self, parent=None, datarange=(0, 1), intervals=2,
            orientation=Qt.Vertical, tbase=10, theight=14, labelSize=6):
        super(LevelsWidget, self).__init__(parent)
        # Set default size
        self._size = QtCore.QSize(400, 130)
        self._labelBackgroundColor = QtGui.QColor(128, 128, 255, 128)

        assert intervals >= 1

        # Triangle dimensions
        self._triangleWidth = int(tbase)
        self._triangleHeight = theight
        self.labelSize = labelSize
        self._orientation = orientation

        # Status attributes
        self._currentItem = None
        self._dragging = False

        # Set attributes
        self.setFocusPolicy(Qt.StrongFocus)
        self.setAcceptDrops(True)
        self.setMouseTracking(True)

        # Context menu actions
        moveAction  = QAction('Move to', self)
        deleteAction  = QAction('Delete', self)
        spaceAction  = QAction('Space equally', self)
        self._contextMenu = QtWidgets.QMenu(self)
        self._contextMenu.addAction(moveAction )
        self._contextMenu.addAction(deleteAction )
        self._contextMenu.addAction(spaceAction )
        self._contextMenu.triggered.connect(self.onContextMenu)
        self.setLevels(datarange, intervals)

    def levels(self):
        return [level.value() for level in self._levels]

    def onContextMenu(self, action):
        """ Manage ContextMenu actions
        """
        if action.text().startswith('Delete'):
            self._levels.pop(self._currentItem)
            self.update()
            self.levelsChanged.emit(self.levels())
        elif action.text().startswith('Move'):
            # Move level
            value = self._levels[self._currentItem].value()
            # Save current locale
            currentLocale = QtCore.QLocale()
            # Set
            QtCore.QLocale.setDefault(QtCore.QLocale.C)
            newValue, ok = QtWidgets.QInputDialog.getDouble(self,
                    "Move to", "New value",
                value, self.min(), self.max(), 4)
            # Restore current locale
            QtCore.QLocale.setDefault(currentLocale)
            if newValue:
                self._levels[self._currentItem].setValue(newValue)
                self._levels[self._currentItem].unselect()
                # Sort levels!
                self._levels.sort(key=lambda level: level.value())
                self.update()
                self.levelsChanged.emit(self.levels())
        elif action.text().startswith('Space'):
            step = self.scalarRange() / (len(self._levels) - 1)
            for i, level in enumerate(self._levels):
                level.setValue(self.min() + i * step)
                self.update()
        self._currentItem = None

    def paintEvent(self, e):
        """ Paint event: draw the linear gradient and the triangle
            level points.
        """
        # Initialize the painter
        p = QtGui.QPainter(self)
        # Draw gray background
        contentsRect = QtCore.QRect(self.contentsRect())
        x = contentsRect.x()
        y = contentsRect.y()
        if self._orientation == Qt.Horizontal:
            lmargin = self._triangleWidth
            rmargin = (self.labelSize - 2) * self._triangleWidth
            bmargin = self._triangleHeight
            tmargin = self._triangleHeight
            contentsRect.adjust(x + lmargin,
                    y + bmargin,
                    - lmargin - rmargin,
                    - bmargin - tmargin
                    )
        else:
            lmargin = 0
            rmargin = int((self.labelSize + 2) * self._triangleWidth)
            bmargin = self._triangleWidth
            tmargin = int(0.5 * self._triangleWidth)
            contentsRect.adjust(x + lmargin,
                    y + bmargin,
                    - lmargin - rmargin,
                    - bmargin - tmargin
                    )
        for x in range(contentsRect.left(), contentsRect.right()):
            p.setPen(Qt.gray)
            p.drawLine(x, contentsRect.top(), x, contentsRect.bottom())
        self.drawFrame(p)

        self.bar = contentsRect

        if not self._levels:
            return

        p.setRenderHint(QtGui.QPainter.Antialiasing)

        # Set pen
        pen = QtGui.QPen(Qt.black, 0.8)
        pen.setJoinStyle(Qt.RoundJoin)
        p.setPen(pen)

        # Triangle
        marker = QtGui.QPolygonF((
                QtCore.QPointF(0, 0),
                QtCore.QPointF(-self._triangleWidth / 2,
                        self._triangleHeight),
                QtCore.QPointF(self._triangleWidth / 2,
                        self._triangleHeight)))
        rot90 = QtGui.QTransform().rotate(-90)
        textSize = QtCore.QSize(self.labelSize * self._triangleWidth,
                self._triangleHeight)
        # Iterate over defined levels
        bar = self.bar
        for lev in self._levels:
            triangle = QtGui.QPolygonF(marker)
            if self._orientation == Qt.Horizontal:
                # Marker
                triangle.translate(bar.left(), bar.bottom())
                delta = self.val2pixels(lev.value())
                triangle.translate(delta, 0)
                # Line
                line = QtCore.QLine(
                        int(triangle[0].x()), int(triangle[0].y()),
                        int(triangle[0].x()), int(triangle[0].y()
                        + (bar.top() - bar.bottom())))
                # Labels
                flags = Qt.AlignLeft
                textrect = QtCore.QRect(QtCore.QPoint(
                    int(triangle[0].x() - self._triangleWidth),
                    int(triangle[0].y() + self._triangleHeight)), textSize)
            else:
                # Marker
                triangle = rot90.map(triangle)
                triangle.translate(bar.right(), bar.top())
                delta = self.val2pixels(lev.value())
                triangle.translate(0, delta)
                # Lines
                line = QtCore.QLine(
                        int(triangle[0].x()), int(triangle[0].y()),
                        int(triangle[0].x() + (bar.left() - bar.right())),
                        int(triangle[0].y()))
                # Labels
                flags = Qt.AlignVCenter | Qt.AlignLeft
                textrect = QtCore.QRect(QtCore.QPoint(
                    int(triangle[0].x() + self._triangleHeight),
                    int(triangle[0].y() - 0.5 * self._triangleHeight)),
                    textSize)
            lev.setTriangle(triangle)

            # Manage selection level
            if lev.isSelected():
                pen.setWidthF(2)
                pen.setColor(Qt.red)
                p.setBrush(QtGui.QColor(Qt.red))
                p.setPen(pen)
            else:
                pen.setWidth(1)
                pen.setColor(Qt.black)
                p.setBrush(QtGui.QColor(Qt.black))
                p.setPen(pen)

            # Draw the triangle
            p.drawPolygon(triangle)
            # Draw line
            p.drawLine(line)
            # Draw text
            p.drawText(textrect, int(flags), '%.2f' % lev.value())
            p.fillRect(textrect, QtGui.QBrush(self._labelBackgroundColor))

    def contextMenuEvent(self, e):
        """ Manage contextMenu (i.e. right click) event
        """
        idx = self.indexAtPos(e.pos())
        if idx not in (None, 0, len(self._levels) - 1):
            self._currentItem = idx
            self._levels[idx].select()
            self.update()
        else:
            # Unselect any level
            self.unselect()
            return
        self._contextMenu.popup(QtGui.QCursor.pos())

    def mousePressEvent(self, e):
        """ Start dragging and switch triangle selection
        """
        if e.button() != Qt.LeftButton:
            return
        idx = self.indexAtPos(e.pos())
        if idx in (None, 0, len(self._levels) - 1):
            return

        # Mouse is on marker
        self._currentItem = idx
        self.dragStart = e.pos()
        self.dragPrev = self.dragStart
        self._levels[idx].select()
        self.currentLevel.emit(idx)
        self._dragging = True
        self.update()

    def mouseReleaseEvent(self, e):
        """ Manage mouse release event. Stop dragging
        """
        if e.button() != Qt.LeftButton or self._currentItem is None:
            return

        if self._orientation == Qt.Horizontal:
            dragDist = e.pos().x() - self.dragStart.x()
        else:
            dragDist = e.pos().y() - self.dragStart.y()
        if dragDist:
            self.levelsChanged.emit(self.levels())

        self._dragging = False
        self.unselect()

    def mouseDoubleClickEvent(self, e):
        """ Insert new level
        """
        if self.contentsRect().contains(e.pos()):
            level = LevelPoint(self.pixels2value(e.pos()))
            for i, l in enumerate(self._levels):
                if l.value() > level.value():
                    self._levels.insert(i, level)
                    break
            self.update()
            self.levelsChanged.emit(self.levels())

    def mouseMoveEvent(self, e):
        """ Drag triangle
        """
        tipRect = QtCore.QRect()
        pos = e.pos()
        idx = self.indexAtPos(pos)
        if idx is not None:
            value = self._levels[idx].value()
            tipText = ("Isoline %d of %d\nValue: %.3f" %
                    (idx + 1, len(self._levels), value))
        else:
            value = self.pixels2value(pos)
            if value < self.min():
                value = self.min()
            elif value > self.max():
                value = self.max()
            tipText = '%.3f' % value
        QtWidgets.QToolTip.showText(
                e.globalPos(), tipText, self, tipRect)

        if not self._dragging or self._currentItem is None:
            return

        if self._levels[self._currentItem].isSelected():
            triangle = self._levels[self._currentItem].triangle()

            if self._orientation == Qt.Horizontal:
                value = self.pixels2value(pos)
                triangle.translate(pos.x() - triangle[1].x(), 0)
                self._levels[self._currentItem].setTriangle(triangle)
            else:
                value = self.pixels2value(pos)
                triangle.translate(pos.y() - triangle[0].y(), 0)
                self._levels[self._currentItem].setTriangle(triangle)
            # Avoid collisions
            previousValue = self._levels[self._currentItem - 1].value()
            nextValue = self._levels[self._currentItem + 1].value()
            if previousValue < value < nextValue:
                self._levels[self._currentItem].setValue(value)

            QtWidgets.QToolTip.showText(e.globalPos(),
                    '%.2f' % value, self, QtCore.QRect())
            self.update()

    def indexAtPos(self, pos):
        """ Return index of triangle at pos or None
        """
        for idx, item in enumerate(self._levels):
            if item.triangle().containsPoint(
                    QtCore.QPointF(pos), Qt.OddEvenFill):
                return idx

    def unselect(self):
        """ Unselect all levels
        """
        [level.unselect() for level in self._levels]
        self.update()

    def setLevels(self, datarange, nlevels):
        """ Set data range and number of levels
        """
        dmin, dmax = datarange
        dv = (dmax - dmin) / nlevels
        self._levels = [LevelPoint(n * dv) for n in range(nlevels + 1)]

        if self._orientation == Qt.Horizontal:
            self.setMinimumHeight(3 * self._triangleHeight + 4)
            self.setMinimumWidth(self.labelSize * self._triangleWidth + 4)
        else:
            self.setMinimumWidth(2 * self._triangleHeight
                    + self.labelSize * self._triangleWidth + 4)
            self.setMinimumHeight(int((3 + nlevels) * 1.2
                    * self._triangleWidth + 4))
        self.update()

    def val2pixels(self, value):
        """ Return number of pixels from scalar value
        """
        if self._orientation == Qt.Vertical:
            v2p = self.bar.height()
        else:
            v2p = self.bar.width()
        v2p /=  self.scalarRange()
        if self._orientation == Qt.Vertical:
            return int(round(v2p * (self.max() - value)))
        else:
            return int(round(v2p * (value - self.min())))

    def pixels2value(self, pos):
        """ Return scalar value from position
        """
        minv, maxv = self.scalarMinMax()
        if self._orientation == Qt.Vertical:
            pixels = pos.y()
            value = ((self.bar.height() - pixels) / self.bar.height()
                    * self.scalarRange())
        else:
            pixels = pos.x()
            value = (pixels / self.bar.width() * self.scalarRange())
        return value

    def sizeHint(self):
        """ Widget default size
        """
        if self._orientation == Qt.Vertical:
            self._size.transpose()
            return self._size
        return self._size

    def scalarMinMax(self):
        """ Return scalar range tuple
        """
        return self.min(), self.max()

    def scalarRange(self):
        """ Return scalar bar range value
        """
        return self.max() - self.min()

    def min(self):
        """ Return scalar bar min value
        """
        return self._levels[0].value()

    def max(self):
        """ Return scalar bar max value
        """
        return self._levels[-1].value()

    def setMin(self, vmin):
        self._levels[0].setValue(vmin)

    def setMax(self, vmax):
        self._levels[-1].setValue(vmax)

#==============================================================================
if __name__ == "__main__":
    import sys
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    def test(obj):
        print('Current: %s' % obj)

    def change(obj):
        print('Levels changed:', obj)

    app = QtWidgets.QApplication(sys.argv)
    cpv = LevelsWidget(intervals=3, orientation=Qt.Vertical)
    cph = LevelsWidget(intervals=3, orientation=Qt.Horizontal)
    #cp.setLevels(11)
    cpv.levelsChanged.connect(change)
    cpv.currentLevel.connect(test)
    cph.levelsChanged.connect(change)
    cph.currentLevel.connect(test)
    #cpv.resize(150, 450)
    #cph.resize(450, 150)
    cpv.show()
    cph.show()
    app.exec_()

