""" A Qt Ipython widget to inspect running code.
    Tested with IPython 6.4.0, Python 3.6.5 and Qt5

    20180608 Roberto Vidmar

"""
import os
# sudo apt-get install python3-pyqt5.qtsvg
from qtconsole.rich_jupyter_widget import RichJupyterWidget
from qtconsole.inprocess import QtInProcessKernelManager

#==============================================================================
class ConsoleWidget(RichJupyterWidget):
    def __init__(self, parent=None, customBanner=None,
            fontSize=11, width=80, height=40, guiCompletion='plain',
            canExit=False, *args, **kargs):
        """ Create a new instance.

            Args:
                parent (widget): parent widget
                customBanner (str): this will be printed **BEFORE** the
                    (python) kernel banner
                fontSize (int): default 11
                width (int): console width in columns
                height (int): console height in columns
                guiCompletion (str): gui_completion attribute (plain or
                    droplist)
                canExit (bool): set to True to allow exit from console
                *args: will be bassed to RichJupyterWidget
                **kwargs: will be bassed to RichJupyterWidget
        """
        self.font_size = fontSize
        self.console_width = width
        self.console_height = height
        self.gui_completion = guiCompletion
        self._canExit = canExit

        super(ConsoleWidget, self).__init__(*args, **kargs)
        #self._trait_values['_display_banner'] = True

        self.kernel_manager = QtInProcessKernelManager()
        if customBanner is not None:
            # Replace Jupyter QtConsole X.Y.Z
            self.banner = customBanner

        self.kernel_manager.start_kernel()
        self.kernel_manager.kernel.gui = 'qt'
        self.kernel_client = kernel_client = self._kernel_manager.client()
        kernel_client.start_channels()
        if kargs:
            self.kernel_manager.kernel.shell.user_ns.update(**kargs)

        def stop():
            if self._canExit:
                kernel_client.stop_channels()
                self.kernel_manager.shutdown_kernel()
                self.close()
            else:
                self.printMsg("# Cannot exit from here...")

        self.exit_requested.connect(stop)

    def pushVar(self, adict):
        """ Given a dictionary containing name / value pairs,
            push those variables (keys) to the Jupyter console widget

            Args:
                adict (dict): variables to pass to the kernel
        """
        self.kernel_manager.kernel.shell.push(adict)

    def clear(self):
        """ Clears the terminal
        """
        self._control.clear()

    def printMsg(self, text):
        """ Prints some plain text to the console

            Args:
                text (str): text to print into the console
        """
        self._append_plain_text(text)

    def executeCmd(self, cmd):
        """ Execute a command in the frame of the console widget

            Args:
                cmd (str): command to execute in the kernel
        """
        self._execute(cmd, False)
        #self.kernel.shell.ev(cmd)

#==============================================================================
if __name__ == '__main__':
    import sys
    import signal
    from . import QtWidgets

    signal.signal(signal.SIGINT, signal.SIG_DFL)

    app = QtWidgets.QApplication(sys.argv)
    somedict = {'key1': 'val1', 'key2': 'val2'}
    widget = ConsoleWidget(customBanner='Available objects: %s\n\n'
            % list(somedict.keys()), canExit=True)
    widget.pushVar(somedict)
    widget.show()
    app.exec_()
    print("That's Al Folks!")
