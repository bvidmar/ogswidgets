# -*- coding: utf-8 -*-
""" A Progress Dialog module

    Author:
      - 20111202-20120110 Roberto Vidmar <rvidmar@inogs.it>
        Nicola Creati <ncreati@inogs.it>
      - 20180523 Roberto Vidmar PyQt5 and Python3

    License:
        MIT/X11 License (see :download:`license.txt <../../../license.txt>`)
"""
import sys
from . import QtCore, QtWidgets, Signal

for cls in ("QSize QThread".split()):
    globals()[cls] = getattr(QtCore, cls)

for cls in ("QVBoxLayout QDialog QDialogButtonBox QApplication QLabel"
        " QProgressBar".split()):
    globals()[cls] = getattr(QtWidgets, cls)

#==============================================================================
class ProgressDialog(QDialog):
    """ Processing gui with progress bar and message
    """

    # Custom signal that notifies abort of the process
    abortSignal = Signal(object)

    def __init__(self,
                 tf,
                 parent=None,
                 title='Processor Progress Dialog',
                 msg='The Message.',
                 value=0):
        """ Create a new ProgressDialog instance.

            :param tf: a :class:`ThreadedFunc` instance
            :type tf: :class:`ThreadedFunc`
            :param parent: parent widget
            :type parent: QtGui widget
            :param title: title for the progress dialog
            :type title: string, unicode
            :param msg: progress message
            :type msg: string, unicode
            :param value: initial value
            :type value: int
            :raises:
        """

        super(ProgressDialog, self).__init__(parent)

        self.setWindowTitle(title)

        # Set the minimu width
        #self.setMinimumWidth(320)

        # Main layout
        l = QVBoxLayout(self)

        # Standard progress bar
        self._msg = QLabel(msg)
        self._bar = QProgressBar()
        self._bar.setValue(value)

        # Dialog button with cancel to stop the thread
        self._buttonBox = QDialogButtonBox(self)
        self._buttonBox.setObjectName("buttonBox")
        self._buttonBox.setStandardButtons(QDialogButtonBox.Abort)

        # Final layout
        l.addWidget(self._msg)
        l.addWidget(self._bar)
        l.addWidget(self._buttonBox)

        self._buttonBox.clicked.connect(tf.abort)
        tf.progressSignal.connect(self.progress)
        tf.closeSignal.connect(self.onClose)

        # Show the widget
        self.setMinimumSize(QSize(300, 100))
        self.exec_()

    def showMessage(self, flag=True):
        """ Show/Hide message

            :param flag: Show/Hide message
            :type flag: bool
            :raises:
        """
        self._msg.setVisible(flag)

    def progress(self, value):
        """ Update progress bar

            :param value: progress value
            :type value: int [0, 100]
            :raises:
        """
        self._bar.setValue(value)

    def setMessage(self, msg):
        """ Set text message

            :param msg: text message
            :type msg: string, unicode
            :raises:
        """
        self._msg.setText(msg)

    def onClose(self):
        self.close()


#==============================================================================
class ThreadedFunc(QThread):
    """ A class to run a function inside a Thread.
    """
    progressSignal = Signal(object)
    endedSignal = Signal(object)
    closeSignal = Signal()

    def __init__(self, func, *args, **kargs):
        """ Create a new ThreadedFunc instance.

                \*args and \**kargs are fed to `func`

            :param func: the function to run
            :type func: function
            :param \*args: various
            :type \*args: various
            :param \*kargs: various
            :type \*kargs: various
            :raises:
        """
        super(ThreadedFunc, self).__init__()
        self._func = func
        self._quit = False
        self._args = args
        self._kargs = kargs
        self.start()

    def abort(self):
        """ Set the "we want abort" flag
        """
        self._quit = True

    def quit(self):
        """ A method to check if the "we want abort" flag is set from inside
            the threaded function.
        """
        return True if self._quit else False

    def run(self):
        """ Start the Thread
        """
        returnValue = self._func(self.progressSignal.emit, self.quit,
                                 *self._args, **self._kargs)
        self.endedSignal.emit(returnValue)
        self.closeSignal.emit()


#==============================================================================
if __name__ == "__main__":

    import signal
    import time
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    def f(progress, qfunc, *args, **kargs):
        for pc in range(101):
            if qfunc():
                return None
            if not pc % 2:
                print("args=%s percent=%d" % (args, pc))
            progress(pc)
            time.sleep(0.1)
        return "Something"

    app = QApplication(sys.argv)
    tf = ThreadedFunc(f, 'arg1', 'arg2')
    pd = ProgressDialog(tf)
    print("Progress dialog ended, execution can continue.....")
    for _ in range(10):
        sys.stdout.write('.')
        sys.stdout.flush()
        time.sleep(0.5)
    print("\nThat's all folks! - Ctrl-C to terminate.")
    app.exec_()
