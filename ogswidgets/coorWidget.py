""" A widget for (phi, lamda, h) or (easting, northing, h) coordinates

    The CoorWidget class implements a widget to display coordinates:
    (lat, lon, height) or (easting, northing height).
    The widget has optional combo boxes both for degrees representation
    (D.DD or D, M.MM or D, M, S.SS) and height (ellipsoidal or orthometric).
    The appearance of the widget (phi, lamda, h) or (easting, northing, h)
    depends on the Projection used.

    Author:
        20111221-20120214 Roberto Vidmar

    Updated:
        20201028 - R. Vidmar

    License:
        MIT/X11 License (see :download:`license.txt <../../../license.txt>`)
"""
from pyproj.crs.crs import CRS
from ogsutils.geodesy import DatumShift, Geoid

# Local imports
from .degreesWidget import DegreesWidget
from .heightWidget import HeightWidget
from .projectedWidget import ProjectedWidget
from . import QtCore, Qt, QtWidgets, Signal

globals()['QCoreApplication'] = getattr(QtCore, 'QCoreApplication')
for cls in ("QVBoxLayout QWidget QHBoxLayout"
        " QApplication QGroupBox QLabel QStackedWidget"
        " QMessageBox QComboBox".split()):
    globals()[cls] = getattr(QtWidgets, cls)


QCA = QCoreApplication

#==============================================================================
class CoorWidget(QWidget):
    """ A widget for (phi, lamda, h) or (easting, northing, h) coordinates.
    """
    WGS84 = 4979 # Geographic 3D CRS WGS 84
    changedSignal = Signal(object)

    # Initial visualization Mode
    ModeD = 0
    ModeDM = 1
    ModeDMS = 2
    COMBO_DMS_OFF = 0
    COMBO_DMS_ON = 1
    COMBO_HEIGHT_OFF = 0
    COMBO_HEIGHT_ON = 1

    # Height
    RepEll = 0
    RepOrtho = 1

    # Stacked Widget: Plane or Geographic
    PlaneIndex = 0
    GeographicIndex = 1

    # Stacked Widget: Point or File
    PointIndex = 0

    LAT_CHR = "\N{GREEK SMALL LETTER PHI}"
    LON_CHR = "\N{GREEK SMALL LETTER LAMDA}"
    HEIGHT_CHR = "H"
    EAST_CHR = "E"
    NORTH_CHR = "N"

    # Signals
    POINT_CHANGED = 'point'
    def __init__(self, xyz=(0, 0, 0), initMode=ModeD, repH=RepEll,
            ell2Ortho=None, name='', epsg=WGS84,
            comboU=COMBO_DMS_OFF, comboH=COMBO_HEIGHT_OFF):
        """ Create a new instance of the widget.

            Args:
                xyz (tuple):
                    - x = Easting or longitude,
                    - y = Northing or latitude,
                    - z = Ellipsoidal height
                initMode (int): Initial representation:
                    - ModeD = D.DD or M.MM
                    - ModeDM = D M.MM
                    - ModeDMS = D M S.SS
                repH (int): representation for height:
                    - RepEll = ellipsoidal
                    - RepOrtho = orthometric
                ell2Ortho (float): offset to add to z to obtain
                    orthometric height
                name (str): optional name of this object
                epsg (int or str): EPSG number for Coordinate Reference
                    System
                comboU (int): Add a combo box to select degrees
                    representation and File selection Default: COMBO_DMS_OFF
                    Values: (COMBO_DMS_OFF, COMBO_DMS_ON, COMBO_DMS_WITH_FILE)
                comboH (int): Add a combo box to select height representation
        """
        super().__init__()

        x, y, z = xyz
        self._ell2ortho = ell2Ortho

        # Set object's name
        self.setObjectName(name)

        # Optional Datum Shift instead of +towgs84
        self._datumShift = None
        # Optional Geoid Grid to add to +towgs84
        self._geoidGrid = None

        self._rep = initMode

        inputLayout = QHBoxLayout()
        self._makePointWidget(inputLayout, repH)
        self._makeCRS(inputLayout)
        self._makeCombos(inputLayout)

        # Main layout:
        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.coorGroup)
        self.setLayout(mainLayout)

        # SET CRS
        self.setCRS(epsg)

        self.setValue(x, y, z)
        self.setDMSCombo(comboU)
        self.setHCombo(comboH)

    def _makeCRS(self, inputLayout):
        # Make a group box frame with a title
        self.coorGroup = QGroupBox(
                QCA.translate("datumGroup", "Coordinate Reference System:"))
        self.coorGroup.setFlat(True)
        f = self.coorGroup.font()
        f.setBold(True)
        self.coorGroup.setFont(f)

        # Build widget layout:
        v = QVBoxLayout()
        v.addLayout(inputLayout)
        self.coorGroup.setLayout(v)

    def _makePointWidget(self, layout, repH):
        layout.addWidget(self._makeXYZWidget(repH))

    def _makeXYZWidget(self, repH):
        # Point Widget
        xyzWidget = QWidget()
        xyzLayout = QHBoxLayout()
        xyzLayout.setContentsMargins(0, 0, 0, 0)
        self._makeCoors(xyzLayout)
        self._makeHeight(xyzLayout, repH)
        xyzWidget.setLayout(xyzLayout)
        return xyzWidget

    def _makeCombos(self, layout):
        # Create "Plane" combo
        self.planeCombo = QComboBox()
        self.planeCombo.setObjectName("planeCombo")

        # Create "Geographic" combo
        self.geographicCombo = QComboBox()
        self.geographicCombo.setObjectName("geographicCombo")

        # Stacked Widget for plane/geographic representation
        self.formatStackedWidget = QStackedWidget()
        self.formatStackedWidget.addWidget(self.planeCombo)
        self.formatStackedWidget.addWidget(self.geographicCombo)

        # Optional combo box for Height representation
        self.heightCombo = QComboBox()
        self.heightCombo.setToolTip(
                QCA.translate(
                "hcombo", "Change height representation among\n"
                "ellipsoidal and orthometric height in meters"))
        self.heightCombo.addItems((QCA.translate("hcombo", "ell"),
                QCA.translate("hcombo", "ortho")))

        combosLayout = QVBoxLayout()
        combosLayout.setContentsMargins(0, 0, 0, 0)
        combosLayout.addWidget(
                self.formatStackedWidget,
                stretch=0, alignment=Qt.AlignCenter)
        combosLayout.addWidget(self.heightCombo,
                stretch=0, alignment=Qt.AlignVCenter)
        layout.addLayout(combosLayout)

        # Connect Signals
        self.heightCombo.currentIndexChanged.connect(self.setHRep)
        self.planeCombo.currentIndexChanged.connect(self._indexChanged)
        self.geographicCombo.currentIndexChanged.connect(self._indexChanged)

    def _makeCoors(self, layout):
        # Latitude, Longitude
        self.latWidget = DegreesWidget(self, latitude=True, rep=self._rep)
        self.lonWidget = DegreesWidget(self, latitude=False, rep=self._rep)
        # Easting, Northing
        easting = QCA.translate("easting", "Easting")
        northing = QCA.translate("northing", "Northing")
        self.eWidget = ProjectedWidget(self, name=easting)
        self.nWidget = ProjectedWidget(self, name=northing)

        # Now build the coordinates widgets
        xx = ((self.LAT_CHR, self.EAST_CHR), (self.latWidget, self.eWidget),
              (QCA.translate("latwidget", "Latitude"), easting))
        yy = ((self.LON_CHR, self.NORTH_CHR), (self.lonWidget, self.nWidget),
              (QCA.translate("lonwidget", "Longitude"), northing))

        self.coors = []
        for coor in (xx, yy):
            sWidget = QStackedWidget()
            #sWidget.setContentsMargins(0, 0, 0, 0)
            for label, dw, toolTip in zip(*coor):
                # Build a Label + Degrees widget
                w = QWidget()
                # The label
                cLabel = QLabel(label)
                cLabel.setToolTip(toolTip)
                # Add a layout for lat, long, height
                hlayout = QHBoxLayout()
                hlayout.setContentsMargins(0, 0, 0, 0)
                hlayout.addWidget(
                        cLabel, alignment=Qt.AlignLeft | Qt.AlignVCenter)
                # add the Degrees widget
                hlayout.addWidget(dw)
                w.setLayout(hlayout)
                # add it to the stackedWidget
                sWidget.addWidget(w)

            # save it into self.coors
            self.coors.append(sWidget)

        layout.addWidget(self.coors[0], alignment=Qt.AlignLeft)
        layout.addWidget(self.coors[1], alignment=Qt.AlignLeft)
        # Connect signals
        for w in (self.latWidget, self.lonWidget, self.eWidget, self.nWidget):
            w.textEditedSignal.connect(self.pointChanged)

    def _makeHeight(self, layout, rep):
        # Height
        hLabel = QLabel(self.HEIGHT_CHR)
        hLabel.setToolTip(QCA.translate("hLabelTooltip", "Height"))
        self.heightWidget = HeightWidget(
                self, ell2Ortho=self._ell2ortho, rep=rep)
        heightHLayout = QHBoxLayout()
        heightHLayout.setContentsMargins(0, 0, 0, 0)
        heightHLayout.addWidget(
                hLabel, alignment=Qt.AlignRight | Qt.AlignVCenter)
        heightHLayout.addWidget(self.heightWidget)
        layout.addLayout(heightHLayout)

        # Connect signals
        self.heightWidget.textEditedSignal.connect(self.pointChanged)

    def setHCombo(self, value):
        self.heightCombo.hide()
        if value and self._ell2ortho:
            self.heightCombo.show()

    def setDMSCombo(self, value):
        if not value:
            self.formatStackedWidget.hide()
            return

        self.formatStackedWidget.show()
        # Disconnect signals
        self.planeCombo.currentIndexChanged.disconnect(self._indexChanged)
        self.geographicCombo.currentIndexChanged.disconnect(self._indexChanged)

        # Clear combos
        self.planeCombo.clear()
        self.geographicCombo.clear()
        planeItems = (QCA.translate("plane", "M.MM"), )
        planeToolTip = QCA.translate("plane", "Easting, Northing in meters")
        geographicItems = (QCA.translate("degs", "D.DD"),
                           QCA.translate("degs", "D M.MM"),
                           QCA.translate("degs", "D M S.SS"))
        geographicToolTip = (QCA.translate(
                "degs", "Change angular representation among\n"
                "degrees.decimals,\ndegrees, minutes.decimals\nand degrees, "
                "minutes, seconds.decimals"))
        self.planeCombo.addItems(planeItems)
        self.planeCombo.setToolTip(planeToolTip)
        self.geographicCombo.addItems(geographicItems)
        self.geographicCombo.setToolTip(geographicToolTip)

        # Reset current index
        self.planeCombo.setCurrentIndex(self.ModeD)
        self.geographicCombo.setCurrentIndex(self._rep)

        # Reconnect signals
        self.planeCombo.currentIndexChanged.connect(self._indexChanged)
        self.geographicCombo.currentIndexChanged.connect(self._indexChanged)

    def description(self):
        """ Return widget description i.e. the pipeline describing the
            transformation to this CRS

            Returns:
                str: PRØJ pipeline
        """
        return self.crsLabel.text()

    def pointChanged(self):
        """ Emit a `changedSignal` Signal with 'point' argument.
        """
        self.changedSignal.emit(self.POINT_CHANGED)

    def datumShift(self):
        """ Return Datum Shift for this point

            :returns: Datum Shift for this point
            :rtype: float
            :raises:
        """
        return self._datumShift

    def geoidGrid(self):
        """ Return Geoid Grid for this point

            :returns: Geoid Grid for this point
            :rtype: float
            :raises:
        """
        return self._geoidGrid

    def setEll2Ortho(self, ell2Ortho):
        """ Set ellipsoidal to orthometric height (geoid) difference

            Args:
                ell2Ortho (float or None: ellipsoidal to orthometric
                    eight (geoid) difference
        """
        self._ell2ortho = ell2Ortho
        self.heightWidget.setEll2Ortho(ell2Ortho)
        if self._ell2ortho is None:
            self.heightCombo.hide()
        else:
            self.heightCombo.show()

    def setValue(self, *values):
        """ Set:
                - Latitude, Longitude, Height [ell2Ortho] **OR**
                - Easting, Northing, Height [ell2Ortho]

            Args:
                *values (float): Latitude, Longitude, Height [ell2Ortho]
        """
        if self.isLatLon():
            for w, j in zip((self.lonWidget, self.latWidget), range(3)):
                if hasattr(values[j], '__iter__'):
                    w.setValue(*values[j])
                else:
                    w.setValue(values[j])
        else:
            self.eWidget.setValue(values[0])
            self.nWidget.setValue(values[1])

        self.heightWidget.setEllH(values[2])

    def _indexChanged(self, index):
        """ Index changed in the combo boxes: set appropriate representation

            Args:
                index (int): new index in the ComboBox
        """
        if index < 0:
            return

        self.setDRep(index)

    def setDRep(self, rep):
        """ Set Degrees representation to rep if :attr:`isLatLon()` evaluates
            to True, else...

            Args:
                rep (ModeD .. ModeDMS): representation to use for latLon or
                    plane coordinates
        """
        if self.isLatLon():
            self.latWidget.setRep(rep)
            self.lonWidget.setRep(rep)
        self._rep = rep

    def setHRep(self, rep):
        """ Set Height representation to rep

            Args:
                rep (RepEll .. RepOrtho): representation to use for height
        """
        self.heightWidget.setRep(rep)

    def tvalue(self):
        """ Return widget value as tuples of text

            Returns:
                tuple: widget value as tuples of text
        """
        if self.isLatLon():
            return [self.latWidget.tvalue(), self.lonWidget.tvalue(),
                    (str(self.heightWidget.ellH()),
                     str(self.heightWidget.ell2Ortho()))
                    ]
        else:
            return [self.eWidget.tvalue(), self.nWidget.tvalue(),
                    (str(self.heightWidget.ellH()),
                    str(self.heightWidget.ell2Ortho()))
                    ]

    def fvalue(self):
        """ Return widget value as tuples of float

            Returns:
                tuple: widget value as tuples of float
        """
        if self.isLatLon():
            return [self.latWidget.fvalue(), self.lonWidget.fvalue(),
                    (self.heightWidget.ellH(),
                    self.heightWidget.ell2Ortho())
                    ]
        else:
            return [self.eWidget.fvalue(), self.nWidget.fvalue(),
                    (self.heightWidget.ellH(),
                    self.heightWidget.ell2Ortho())
                    ]

    def svalue(self):
        """ Return widget value as tuple of strings

            Returns:
                tuple: widget value as tuples of strings
        """
        if self.isLatLon():
            return [self.latWidget.svalue(), self.lonWidget.svalue(),
                    (str(self.heightWidget.ellH()),
                    str(self.heightWidget.ell2Ortho()))
                    ]
        else:
            return [self.eWidget.svalue(), self.nWidget.svalue(),
                    (str(self.heightWidget.ellH()),
                    str(self.heightWidget.ell2Ortho()))
                    ]

    def xyz(self):
        """ Return either
                * longitude, latitude, height
                * easting, northing, height

            according to :class:`isLatLon()`

            Returns:
                tuple: x, y, z

            .. note:: Height is always **ELLIPSOIDAL**.
        """
        if self.isLatLon():
            return [self.lonWidget.fvalue(0)[0],
                    self.latWidget.fvalue(0)[0],
                    self.heightWidget.ellH()
                    ]
        else:
            return [self.eWidget.fvalue(0)[0],
                    self.nWidget.fvalue(0)[0],
                    self.heightWidget.ellH()
                    ]

    def ell2Ortho(self):
        """ Return ellipsoidal to orthometric height difference

            Returns:
                float: ellipsoidal to orthometric height difference
        """
        return self.heightWidget.ell2Ortho()

    def isLatLon(self):
        """ Return True if CRS is geographic

            Returns:
                bool: True if CRS is geographic
        """
        return self._crs.is_geographic

    def crs(self):
        """ Return CRS object for this point

            Returns:
                CRS.CRS: CRS for this point
        """
        return self._crs

    def setCRS(self, epsg):
        """ Set CRS for this point and return CRS instance if ok

            Args:
                epsg (int or str): EPSG defining CRS

            Returns:
                :class:`pyproj.crs.CRS`: CRS instance
        """
        self.epsg = epsg
        try:
            self._crs = CRS.from_epsg(epsg)
        except RuntimeError as e:
            from IPython import embed; embed()
            msgBox = QMessageBox(
                    QMessageBox.Warning,
                    QCA.translate("projMbox", 'Invalid PROJ.4 string'),
                    QCA.translate(
                    "projMbox",
                    "'%s' is an invalid PROJ.4 string.\nThe reason is:\n%s."
                    ) % (projS, e), QMessageBox.Ok)
            msgBox.exec_()
        else:
            if self.isLatLon():
                index = 0
            else:
                index = 1
            self.coors[0].setCurrentIndex(index)
            self.coors[1].setCurrentIndex(index)
            self.setTitle("CRS: [EPSG %s] %s" % (self.epsg, self._crs.name))
            retval = self._crs

        if self.isLatLon():
            self.formatStackedWidget.setCurrentIndex(self.GeographicIndex)
        else:
            self.formatStackedWidget.setCurrentIndex(self.PlaneIndex)
        return retval

    def xxset(self, epsg):
        """ Set projection for this point and return it if ok else False

            Args:
                epsg (int or str): EPSG defining CRS

            Returns:
                :class:`pyproj.crs.CRS`: CRS instance
        """
        self.epsg = epsg
        retval = False
        self._datumShift = None
        self._geoidGrid = None
        cleanProjS = epsg

        # Search for extra keyword(s)
        #for extraKeyword in EXTRA_KEYWORDS:
        if False:
            b, key, e = projS.partition(extraKeyword)
            if key == DATUMSHIFT_KEYWORD:
                # extra keyword found, parse its value
                pn = e.split()[0]
                try:
                    self._datumShift = DatumShift(pn)
                except IOError as e:
                    msgBox = QMessageBox(
                            QMessageBox.Warning,
                            QCA.translate("datumMbox", 'IO Error'),
                            QCA.translate(
                            "datumMbox",
                            "Cannot load datum shift file '%s'.\n"
                            "The reason is:\n%s."
                            ) % (pn, e), QMessageBox.Ok)
                    msgBox.exec_()
                    return retval
                else:
                    if self._datumShift.datumShift() == (None, None):
                        self._datumShift = None
                        msgBox = QMessageBox(
                                QMessageBox.Warning,
                                QCA.translate("datumMbox",
                                              'Datum Shift Error'),
                                QCA.translate(
                                        "datumMbox",
                                        "'%s' is not a valid Datum Shift file."
                                ) % (pn), QMessageBox.Ok)
                        msgBox.exec_()
                        return retval
                    # Remove extra from the string
                    cleanProjS = cleanProjS.replace(extraKeyword + pn, '')
            elif key == GEOIDGRIDS_KEYWORD:
                # extra keyword found, parse its value
                pn = e.split()[0]
                try:
                    self._geoidGrid = Geoid(pn)
                except IOError as e:
                    msgBox = QMessageBox(
                            QMessageBox.Warning,
                            QCA.translate("geoidMbox", 'IO Error'),
                            QCA.translate(
                            "geoidMbox",
                            "Cannot load geoid grid file '%s'.\n"
                            "The reason is:\n%s."
                            ) % (pn, e), QMessageBox.Ok)
                    msgBox.exec_()
                    return retval
                else:
                    if self._geoidGrid.geoid() == None:
                        self._geoidGrid = None
                        msgBox = QMessageBox(
                                QMessageBox.Warning,
                                QCA.translate("geoidMbox", 'Geoid Grid Error'),
                                QCA.translate(
                                        "geoidMbox",
                                        "'%s' is not a valid Geoid Grid file.")
                                % (pn), QMessageBox.Ok)
                        msgBox.exec_()
                        return retval
                    # Remove extra from the string
                    cleanProjS = cleanProjS.replace(extraKeyword + pn, '')

        try:
            #self._proj = Proj(cleanProjS, preserve_units=True)
            self._proj = CRS.from_epsg(epsg)
        except RuntimeError as e:
            from IPython import embed; embed()
            msgBox = QMessageBox(
                    QMessageBox.Warning,
                    QCA.translate("projMbox", 'Invalid PROJ.4 string'),
                    QCA.translate(
                    "projMbox",
                    "'%s' is an invalid PROJ.4 string.\nThe reason is:\n%s."
                    ) % (projS, e), QMessageBox.Ok)
            msgBox.exec_()
        else:
            #if self._proj.is_latlong():
            if self.isLatLon():
                index = 0
            else:
                index = 1
            self.coors[0].setCurrentIndex(index)
            self.coors[1].setCurrentIndex(index)
            self.setTitle("CRS: [EPSG %s] %s" % (self.epsg, self._proj.name))
            retval = self._proj

        if self.isLatLon():
            self.formatStackedWidget.setCurrentIndex(self.GeographicIndex)
        else:
            self.formatStackedWidget.setCurrentIndex(self.PlaneIndex)
        return retval

    def setDescription(self, description):
        """ Set description for the CRS

            Args:
                description (str): description
        """
        self.crsLabel.setText(description)

    def setTitle(self, title):
        """ Set title for the widget

            Args:
                title (str): widget title
        """
        self.coorGroup.setTitle(title)

    def decimals(self):
        """ Return x, y, z decimals for representation as tuples

            Returns:
                tuple: decimals for representation of x, y, z
        """
        if self.isLatLon():
            rep = self.geographicCombo.currentIndex()
            if rep == DegreesWidget.DDD:
                xdecimals = (self.lonWidget.ddecimals, )
                ydecimals = (self.latWidget.ddecimals, )
            elif rep == DegreesWidget.DMM:
                xdecimals = (0, self.lonWidget.mdecimals)
                ydecimals = (0, self.latWidget.mdecimals)
            elif rep == DegreesWidget.DMS:
                xdecimals = (0, 0, self.lonWidget.sdecimals)
                ydecimals = (0, 0, self.latWidget.sdecimals)
        else:
            xdecimals = self.eWidget.decimals
            ydecimals = self.nWidget.decimals
        zdecimals = self.heightWidget.decimals
        return xdecimals, ydecimals, zdecimals

    def coorFormat(self):
        """ Return format of coordinates as shown in the comboBox

            Returns:
                XXXX:
        """

        if self.isLatLon():
            combo = self.geographicCombo
        else:
            combo = self.planeCombo
        index = combo.currentIndex()
        allItems = [combo.itemText(i) for i in range(combo.count())]
        print("coorFormat:")
        from IPython import embed; embed()
        return allItems[index]


#==============================================================================
if __name__ == '__main__':
    import sys
    from signal import signal, SIGINT, SIG_DFL
    import argparse

    signal(SIGINT, SIG_DFL)
    parser = argparse.ArgumentParser("CoorWidget",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-f", "--format", type=int, default=0,
            choices=(0, 1, 2),
            help="Set representation 0=D, 1=DM, 2=DMS: ")
    parser.add_argument("--combo-dms", type=int, default=0,
            choices=(0, 1),
            help="Disable/enable DMS combo 0=off, 1=on ")
    parser.add_argument("--combo-h", type=int, default=0,
            choices=(0, 1),
            help="Disable/enable HEIGHT combo 0=off, 1=on ")
    parser.add_argument("--epsg", default=4979,
            help="Set Coordinate Reference System from EPSG: ")
    parser.add_argument("-e", "--ellH", default=123,
            help="Set height above ellipsoid: ")
    parser.add_argument("-d", "--diff", type=float, default=None,
            help="Set ell-ortho difference: ")
    opts = parser.parse_args()

    app = QApplication(sys.argv)
    # LatLong
    w1 = CoorWidget(epsg=opts.epsg)
    w1.setDMSCombo(opts.combo_dms)
    w1.setHCombo(opts.combo_h)
    w1.setValue((13., 45.81882), (45.710551, ), 285.8)
    w1.setEll2Ortho(opts.diff)
    w1.show()
    if True:
        # Plane
        x = 4000000
        y = 50000000
        z = 285.8
        w2 = CoorWidget((x, y, z), epsg=32633)
        w2.setHCombo(CoorWidget.COMBO_HEIGHT_ON)
        w2.show()
    sys.exit(app.exec_())
