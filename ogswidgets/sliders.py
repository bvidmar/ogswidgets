""" Sliders module.

    Author:
        - 2009-2011 Nicola Creati
        - 2012-2012 Roberto Vidmar - Modified for use with PySide

    License:
        MIT/X11 License (see :download:`license.txt <../../../license.txt>`)

"""
from . import Signal, QtCore, Qt, QtGui, QtWidgets

#==============================================================================
def float2str(afloat, decimals):
    """ Return afloat converted to string with `decimals` decimals
    """
    return "%%.%df" % decimals % afloat

#==============================================================================
class QRangeSlider(QtWidgets.QSlider):
    """ A slider for a range of values.

        This class provides a dual-slider for a range of values.
        There is a defined maximum and minimum, as is a normal slider,
        but there are 2 slider values.

        Signals:
            valueChanged
    """
    valueChanged = Signal(tuple)

    def __init__(self, *args, lowColor=Qt.red, highColor=Qt.green,
            indipendent=False):
        super(QRangeSlider, self).__init__(*args)

        self._low = self.minimum()
        self._high = self.maximum()
        self._lowColor = lowColor
        self._highColor = highColor
        self._indipendent = indipendent
        self.pressed_control = QtWidgets.QStyle.SC_None
        self.hover_control = QtWidgets.QStyle.SC_None
        self.click_offset = 0

        # 0 for the low, 1 for the high, -1 for both
        self.active_slider = 0

    def low(self):
        return self._low

    def setLow(self, low):
        self._low = low
        #self.update()

    def high(self):
        return self._high

    def setHigh(self, high):
        self._high = high
        #self.update()

    def paintEvent(self, event):
        # based on http://qt.gitorious.org/qt/qt/blobs/master/src/
        #   gui/widgets/qslider.cpp
        #embed()
        painter = QtGui.QPainter(self)
        style = QtWidgets.QApplication.style()
        for i, value in enumerate([self._low, self._high]):
            opt = QtWidgets.QStyleOptionSlider()
            self.initStyleOption(opt)
            #opt.palette.setColor(QtGui.QPalette.Base, QtGui.QColor(255, 0, 0))

            # Only draw the groove for the first slider so it doesn't get drawn
            # on top of the existing ones every time
            if i == 0:
                opt.subControls = (QtWidgets.QStyle.SC_SliderGroove
                        | QtWidgets.QStyle.SC_SliderHandle)
            else:
                opt.subControls = QtWidgets.QStyle.SC_SliderHandle

            if self.tickPosition() != self.NoTicks:
                opt.subControls |= QtWidgets.QStyle.SC_SliderTickmarks

            if self.pressed_control:
                opt.activeSubControls = self.pressed_control
                opt.state |= QtWidgets.QStyle.State_Sunken
            else:
                opt.activeSubControls = self.hover_control

            opt.sliderPosition = int(value)
            opt.sliderValue = int(value)
            style.drawComplexControl(QtWidgets.QStyle.CC_Slider, opt, painter,
                                     self)
            rectHandle = style.subControlRect(QtWidgets.QStyle.CC_Slider, opt,
                                              QtWidgets.QStyle.SC_SliderHandle,
                                              self)
            if self._indipendent:
                if i == 0:
                    painter.fillRect(rectHandle, QtGui.QBrush(self._lowColor))
                    painter.drawText(
                        QtCore.QRectF(rectHandle), Qt.AlignCenter, "L")
                else:
                    painter.fillRect(rectHandle, QtGui.QBrush(self._highColor))
                    painter.drawText(
                        QtCore.QRectF(rectHandle), Qt.AlignCenter, "H")

    def mousePressEvent(self, event):
        event.accept()

        style = QtWidgets.QApplication.style()
        button = event.button()

        # In a normal slider control, when the user clicks on a point in the
        # slider's total range, but not on the slider part of the control the
        # control would jump the slider value to where the user clicked.
        # For this control, clicks which are not direct hits will slide both
        # slider parts

        if button:
            opt = QtWidgets.QStyleOptionSlider()
            self.initStyleOption(opt)

            self.active_slider = -1

            for i, value in enumerate([self._low, self._high]):
                opt.sliderPosition = value
                hit = style.hitTestComplexControl(style.CC_Slider, opt,
                                                  event.pos(), self)
                if hit == style.SC_SliderHandle:
                    self.active_slider = i
                    self.pressed_control = hit

                    self.triggerAction(self.SliderMove)
                    self.setRepeatAction(self.SliderNoAction)
                    self.setSliderDown(True)
                    break

            if self.active_slider < 0:
                self.pressed_control = QtWidgets.QStyle.SC_SliderHandle
                self.click_offset = self.__pixelPosToRangeValue(
                    self.__pick(event.pos()))
                self.triggerAction(self.SliderMove)
                self.setRepeatAction(self.SliderNoAction)
        else:
            event.ignore()

    def mouseMoveEvent(self, event):
        if self.pressed_control != QtWidgets.QStyle.SC_SliderHandle:
            event.ignore()
            return

        event.accept()
        new_pos = self.__pixelPosToRangeValue(self.__pick(event.pos()))
        opt = QtWidgets.QStyleOptionSlider()
        self.initStyleOption(opt)

        if self.active_slider < 0:
            offset = new_pos - self.click_offset
            self._high += offset
            self._low += offset
            if self._low < self.minimum():
                diff = self.minimum() - self._low
                self._low += diff
                self._high += diff
            if self._high > self.maximum():
                diff = self.maximum() - self._high
                self._low += diff
                self._high += diff
        elif self.active_slider == 0:
            if not self._indipendent:
                if new_pos >= self._high:
                    new_pos = self._high - 1
            self._low = new_pos
        else:
            if not self._indipendent:
                if new_pos <= self._low:
                    new_pos = self._low + 1
            self._high = new_pos

        self.click_offset = new_pos

        self.update()

        self.sliderMoved.emit(new_pos)

    def __pick(self, pt):
        if self.orientation() == Qt.Horizontal:
            return pt.x()
        else:
            return pt.y()

    def mouseReleaseEvent(self, evt):
        evt.accept()
        self.valueChanged.emit((self._low, self._high))

    def __pixelPosToRangeValue(self, pos):
        opt = QtWidgets.QStyleOptionSlider()
        self.initStyleOption(opt)
        style = QtWidgets.QApplication.style()

        gr = style.subControlRect(style.CC_Slider, opt, style.SC_SliderGroove,
                                  self)
        sr = style.subControlRect(style.CC_Slider, opt, style.SC_SliderHandle,
                                  self)

        if self.orientation() == Qt.Horizontal:
            slider_length = sr.width()
            slider_min = gr.x()
            slider_max = gr.right() - slider_length + 1
        else:
            slider_length = sr.height()
            slider_min = gr.y()
            slider_max = gr.bottom() - slider_length + 1

        return style.sliderValueFromPosition(
            self.minimum(), self.maximum(), pos - slider_min,
            slider_max - slider_min, opt.upsideDown)

    def value(self):
        """ Return slider values
        """
        return self.low(), self.high()


#==============================================================================
class LabelSlider(QtWidgets.QWidget):
    """ A slider widget with a label showing current value for a float range.
        The LabelSlider can show two sliders for selecting two values.
        The sliders *CAN* be indipendent i.e. both can move in the whole range
        of values. In this case sliders show as moving H and L letters on
        a coloured background.
        Value(s) must be set using setValue method

        Signals:
            valueChanged
            sliderMoved
    """

    valueChanged = Signal(object)
    sliderMoved = Signal(object)

    def __init__(self,
                 parent=None,
                 srange=(0, 1),
                 nlabels=1,
                 value=None,
                 decimals=0,
                 ticks=20,
                 indipendent=False,
                 name=''):
        """ Create a new LabelSlider instance.

            Args:
                parent (:class:`QtWidgets.QWidget`): parent widget
                srange (tuple): min, max values
                nlabels (int): number of labels
                value (float or tuple): set value of slider
                decimals (int): number of decimals for the label and value
                    returned
                ticks (int): number of ticks
                indipendent (bool): if True sliders can move indipendently
                    over the whole range
                name (str): object name
        """
        super(LabelSlider, self).__init__(parent)
        self._nlabels = nlabels
        self._indipendent = indipendent
        self._decimals = decimals
        self.setObjectName(name)
        self._resolution = 10 ** -decimals
        self._leftLabel = QtWidgets.QLabel(self)
        self._rightLabel = None
        self.ticks = ticks
        self._leftLabel.setText(float2str(srange[0], self._decimals))

        l = QtWidgets.QHBoxLayout(self)
        l.addWidget(self._leftLabel)

        if self._nlabels == 2:
            self._slider = QRangeSlider(
                Qt.Horizontal, self, indipendent=self._indipendent)
            self._rightLabel = QtWidgets.QLabel(self)
            self._rightLabel.setText(float2str(srange[1], self._decimals))
            l.addWidget(self._slider)
            l.addWidget(self._rightLabel)
            self._slider.setLow(srange[0] / self._resolution)
            self._slider.setHigh(srange[1] / self._resolution)
        else:
            self._slider = QtWidgets.QSlider(Qt.Horizontal, self)
            self._slider.setValue(int(srange[0] / self._resolution))
            l.addWidget(self._slider)


        self._slider.valueChanged.connect(self._valueChanged)
        self._slider.sliderMoved.connect(self._onSliderMove)
        self._slider.setTickPosition(QtWidgets.QSlider.TicksBothSides)

        self.setRange(srange)
        if value is not None:
            self.setValue(value)
        #self._slider.setMinimum(ranges[0] / self._resolution)
        #self._slider.setMaximum(ranges[1] / self._resolution)
        #step = int((self._slider.maximum() - self._slider.minimum()) / 20.0)
        #if step == 0:
            #step = 1
        #self._slider.setSingleStep(step)

    def _onSliderMove(self, value):
        if self._rightLabel is not None:
            low = self._slider.low() * self._resolution
            high = self._slider.high() * self._resolution
            self._rightLabel.setText(float2str(high, self._decimals))
            self._leftLabel.setText(float2str(low, self._decimals))
            self.sliderMoved.emit((low, high))
        else:
            self._leftLabel.setText(
                float2str(value * self._resolution, self._decimals))
            self.sliderMoved.emit(value * self._resolution)

    def _valueChanged(self, value):
        if isinstance(value, tuple):
            self.valueChanged.emit((value[0] * self._resolution,
                                    value[1] * self._resolution))
        else:
            self.valueChanged.emit(value * self._resolution)

    @property
    def name(self):
        return self.objectName()

    def setRange(self, theRange):
        """ Override (minimum, maximum) range values
        """
        self._slider.setMinimum(int(theRange[0] / self._resolution))
        self._slider.setMaximum(int(theRange[1] / self._resolution))
        step = int((self._slider.maximum() - self._slider.minimum())
                / self.ticks)
        if step == 0:
            step = 1
        self._slider.setSingleStep(step)

    def setValue(self, value):
        if self._nlabels == 2:
            if not self._indipendent and (value[0] > value[1]):
                raise RuntimeError(
                    "%s > %s! This is forbidden!" % (value[0], value[1]))

            ivalue = [v / self._resolution for v in value]
            if ivalue[0] < self._slider.minimum():
                ivalue[0] = self._slider.minimum()
            if ivalue[1] > self._slider.maximum():
                ivalue[1] = self._slider.maximum()
            self._slider.setLow(ivalue[0])
            self._slider.setHigh(ivalue[1])
            self._leftLabel.setText(float2str(value[0], self._decimals))
            self._rightLabel.setText(float2str(value[1], self._decimals))
        else:
            ivalue = value / self._resolution
            if ivalue < self._slider.minimum():
                ivalue = self._slider.minimum()
            if ivalue > self._slider.maximum():
                ivalue = self._slider.maximum()
            self._slider.setValue(int(ivalue))
            self._leftLabel.setText(float2str(value, self._decimals))

    def value(self):
        """ Return slider value(s)
        """
        if self._nlabels == 2:
            value = self._slider.value()
            return value[0] * self._resolution, value[1] * self._resolution
        else:
            return self._slider.value()


#==============================================================================
if __name__ == "__main__":
    import sys
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    def echo(value):
        print(value)

    app = QtWidgets.QApplication(sys.argv)

    lslider = LabelSlider(None, srange=(-360, 360), nlabels=1,
                indipendent=True, name="LabelSlider")
    lslider.setValue(-10)
    l2slider = LabelSlider(None, srange=(-360, 360), nlabels=2,
                indipendent=True, value=(-180, 180), name="LabelSlider")
    #l2slider.setValue((-180, 180))
    rslider = QRangeSlider(Qt.Horizontal)
    rslider.setMinimum(0)
    rslider.setMaximum(10000)
    rslider.setLow(1000)
    rslider.setHigh(5000)
    rslider.setValue(3500)
    rslider.sliderMoved.connect(echo)
    l2slider.valueChanged.connect(echo)
    rslider.show()
    lslider.show()
    l2slider.show()
    app.exec_()
