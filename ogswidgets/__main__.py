import os

widgets = ("appExceptionTrace console coorDialog coorWidget"
        " degreesWidget fieldsWizard fileBrowser filePicker heightWidget"
        " hsiIndicator crsDialog projectedWidget qMemoryWidget sliders"
        " progressDialog").split()
for widget in widgets:
    input("Testing %s: press <Enter> " % widget)
    os.system("python3 -m ogswidgets.%s" % widget)
