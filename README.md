# ogswidgets #
Ogswidgets a Python package with the widgets developed
@ OGS.

### The ogswidgets classes ###

* NiceColors

## Modules ##
* abstractFeditor
* appExceptionTrace
* console
* coorDialog
* coorWidget
* crsDialog
* crsMVC
* degreesWidget
* delegates
* fieldsWizard
* fileBrowser
* fileCoorWidget
* filePicker
* heightWidget
* hsiIndicator
* image
* levelsWidget
* lineEditors
* progressDialog
* projectedWidget
* qMemoryWidget
* sliders

### Where is the documentation? ###

[Here!](https://bvidmar.bitbucket.io/ogswidgets/)

### Who do I talk to? ###

* [Roberto Vidmar](mailto:rvidmar@inogs.it)

* [Nicola Creati](mailto:ncreati@inogs.it)
