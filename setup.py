"""
OGSWIDGETS a collection of PyQt5 / PySide2 widgets developed at OGS
===================================================================

- OGSWIDGETS
    Python package containing a collection of PyQt5 - PySide2 widgets
    developed at OGS

Authors:
    - 2014-2020
    - Nicola Creati, ncreati@inogs.it
    - Roberto Vidmar, rvidmar@inogs.it
    - R. Vidmar, 20210128

Istituto Nazionale di Oceanografia e di Geofisica Sperimentale - OGS
https://www.inogs.it

"""

import os
import pip
from setuptools import setup, find_packages
import distutils.spawn
import subprocess

PKGNAME = 'ogswidgets'
SUBVERSION = "a0"
REQFILE = "requirements.txt"
EXECUTABLES = []
CFGFILES = []
SCRIPTS = ['bin/%s' % sc for sc in EXECUTABLES]
# Retrieve version information
exec(open(os.path.join(PKGNAME, "__version__.py")).read())
version = "%s.%s" % (__version__, SUBVERSION)

# Check pip version
if int(pip.__version__.split('.')[0]) < 20:
    # pip upgrade needed!
    print("\n\nWARNING!\n    pip version is too old (%s < 20.0.0)\n"
            % pip.__version__)
    raise SystemExit("\n\n Please run 'pip3 install -U pip' and then"
            " run again this command.\n")

#------------------------------------------------------------------------------
# Check for VIRTUAL_ENV
if 'VIRTUAL_ENV' in os.environ:
    bin_path = os.path.join(os.environ.get('VIRTUAL_ENV'),
                       'lib',
                       'python%d.%d' % sys.version_info[:2],
                       'site-packages', PKGNAME, 'bin')
else:
    bin_path = None

# Avoid name clash for scripts
ok = True
conflicting = []
for ex in EXECUTABLES:
    executable = distutils.spawn.find_executable(ex, bin_path)
    if executable:
        ok = False
        for line in open(executable):
            if ("This script belongs to Python package %s" % PKGNAME in line
                    or (PKGNAME in line
                        and 'EASY-INSTALL-DEV-SCRIPT'in line)):
                # This executable belong to this package
                ok = True
                break
        if not ok:
            conflicting.append(executable)
if not ok:
    raise SystemExit("\nWARNING!\n"
            "Installation will overwrite the following files:\n"
            " --> %s\nPlease resolve conflict before retrying.\n"
            "***Installation aborted***" % conflicting)

#==============================================================================
description = __doc__.split('\n')[1:-1][0]
classifiers = """
Development Status :: 1 - Planning
Intended Audience :: Developers
Intended Audience :: Science/Research
License :: OSI Approved :: BSD License
Operating System :: POSIX
Programming Language :: C
Programming Language :: C++
Programming Language :: Fortran
Programming Language :: Python
Topic :: Scientific/Engineering
Topic :: Software Development :: Libraries
"""

with open(REQFILE) as fp:
    requirements = fp.read()

setup(name=PKGNAME,
        version=version,
        install_requires=requirements,
        description=description,
        long_description=open("README.md", "r").read(),
        long_description_content_type='text/markdown',
        classifiers=classifiers.split('\n')[1:-1],
        keywords=[PKGNAME, 'PyQt5', 'PySide2', 'Widgets'],
        platforms=['POSIX'],
        license='MIT',
        scripts=SCRIPTS,
        include_package_data=True,
        url='https://bitbucket.org/bvidmar/ogswidgets',
        download_url='https://bitbucket.org/bvidmar/ogsutils/',
        author='Roberto Vidmar',
        author_email='rvidmar@inogs.it',
        packages=find_packages(),
        )
